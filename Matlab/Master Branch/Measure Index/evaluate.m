function [cpsnr,cssim,essim,fsim,ser] =  evaluate(intped,GT)
cpsnr = psnr(uint8(intped),GT);
cssim = ssim(uint8(intped),GT);
essim = ESSIM(uint8(intped),GT);
fsim = FSIM(uint8(intped),GT);
ser = SCIELAB_ERROR(uint8(intped),GT);
end