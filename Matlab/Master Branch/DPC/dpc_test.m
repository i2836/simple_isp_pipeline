%% This is the single test file of the module DPC(Dynamic Pixel Correction),use the test case in the directory to test the DPC process.
%% The position of DPC module in the ISP pipeline is always after the SP(SP Noise Removal Process)
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_dpc_param = struct('mode',2,'Jeehoon_Th',20,'Jeehoon_Num',5000); %Three methods: 0 Jeehoon_Th��1 chans�� 2 Chao-Yi, Jeehoon_Th and Jeehoon_Num are param in mode 0
dpc_eb = 1;
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after LSC
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
sp_out = fread(fileinptr,[top_paramset.width, top_paramset.height],'uint8');
sp_out = sp_out';
fclose(fileinptr);

%% Main Process
dpc_out = module_dpc_process(sp_out,module_dpc_param,top_paramset,dpc_eb);

%% Disp Result in png format
figure(1),imshow(uint8(sp_out));
figure(2),imshow(uint8(dpc_out));
imwrite(uint8(dpc_out),'./Test Case/Case Three/DPC_test_out_mode2.png');

%% Store the Result in the Raw Foramt
path = './Test Case/Case Three/';
file = 'dpc_test_out_mode2.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,dpc_out','uint8');
fclose(fileoutptr);