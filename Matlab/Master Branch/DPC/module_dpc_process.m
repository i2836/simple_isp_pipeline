%% This is the main process of DPC module(Defected Pixel Correction),which is used to Correct the defected pixels in the camera process.
%% dpc_eb = 1,enables the DPC process, vice versa.
%% The DPC process in the ISP pipeline is always after the processes of SP Noise Removal
%% here enables 3 kinds of Defected Pixels Correction process,case mode:0/1/2
    %% mode 0: Jeehoon An's method, refers to passage: "ADAPTIVE DETECTION AND CONCEALMENT ALGORITHM OF DEFECTIVE PIXEL"
    %% mode 1: CHAN's method, refers to passage: "Dead pixel real-time detection method for image￣
    %% mode 2: Chao's method, refers to passage: "Real-Time Photo Sensor Dead Pixel Detection for Embedded Devices"

function [dpc_out] = module_dpc_process(lsc_out,module_dpc_param,top_paramset,dpc_eb)
if(dpc_eb)
    %% param set process
    if(top_paramset.Auto)
        module_dpc_param.mode = 1; %% Auto set the Chen's DPC methods,without other paramters
    else
        mode = module_dpc_param.mode;
        Jeehoon_Th = module_dpc_param.Jeehoon_Th;
        Jeehoon_Num = module_dpc_param.Jeehoon_Num;
    end
    
    %% initial the output array and padding for the need
    [Img_Height,Img_Width] = size(lsc_out); 
    lsc_pad_out = padding_array(lsc_out,2,2);
    dpc_out = lsc_out;  

    Dis = zeros(Img_Height,Img_Width);
    
    %% when mode = 0:enables the Jeehoon_Th's method, 1: enables the chen's methods ,2:Chao-Yi's methods
    if(mode == 0) %Jeehoon_Th's method
        D1 = abs(lsc_pad_out(1:Img_Height,3:Img_Width+2) - lsc_pad_out(5:Img_Height+4,3:Img_Width+2)) + ...
             abs(lsc_pad_out(2:Img_Height+1,3:Img_Width+2) - lsc_pad_out(4:Img_Height+3,3:Img_Width+2));  %%Vertical Dir Edge
        D2 = abs(lsc_pad_out(1:Img_Height,5:Img_Width+4) - lsc_pad_out(5:Img_Height+4,1:Img_Width)) + ...
             abs(lsc_pad_out(2:Img_Height+1,4:Img_Width+3) - lsc_pad_out(4:Img_Height+3,2:Img_Width+1));  %%45 Degree Dir Edge
        D3 = abs(lsc_pad_out(3:Img_Height+2,1:Img_Width) - lsc_pad_out(3:Img_Height+2,5:Img_Width+4)) + ... 
             abs(lsc_pad_out(3:Img_Height+2,2:Img_Width+1) - lsc_pad_out(3:Img_Height+2,4:Img_Width+3));  %%Horzontial Dir Edge
        D4 = abs(lsc_pad_out(1:Img_Height,1:Img_Width) - lsc_pad_out(5:Img_Height+4,5:Img_Width+4)) + ...
             abs(lsc_pad_out(2:Img_Height+1,2:Img_Width+1) - lsc_pad_out(4:Img_Height+3,4:Img_Width+3));  %%135 Degree Dir Edge
         
        Dmin = min(min(min(D1,D2),D3),D4);
          
       %% calculate the Edge Difference map
        for  i = 1:Img_Height
            for j = 1:Img_Width
                if(( Dmin(i,j) == D1(i,j)) && (D1(i,j)<Jeehoon_Th))
                    Dis(i,j) = abs(lsc_pad_out(i+2,j+2) - (lsc_pad_out(i,j+2) + lsc_pad_out(i+4,j+2) + 1)/2);
                elseif(( Dmin(i,j) == D2(i,j)) && (D2(i,j)<Jeehoon_Th))
                    Dis(i,j) = abs(lsc_pad_out(i+2,j+2) - (lsc_pad_out(i,j+4) + lsc_pad_out(i+1,j+3) + lsc_pad_out(i+3,j+1) + lsc_pad_out(i+4,j)+ 2)/4);
                elseif(( Dmin(i,j) == D3(i,j)) && (D3(i,j)<Jeehoon_Th))
                    Dis(i,j) = abs(lsc_pad_out(i+2,j+2) - (lsc_pad_out(i+2,j) + lsc_pad_out(i+2,j+4) + 1)/2);
                elseif(( Dmin(i,j) == D4(i,j)) && (D4(i,j)<Jeehoon_Th))
                    Dis(i,j) = abs(lsc_pad_out(i+2,j+2) - (lsc_pad_out(i,j) + lsc_pad_out(i+1,j+1) + lsc_pad_out(i+3,j+3) + lsc_pad_out(i+4,j+4)+ 2)/4);
                else
                    Dis(i,j) = abs(lsc_pad_out(i+2,j+2) - ((lsc_pad_out(i+1,j+1) + lsc_pad_out(i+1,j+2) + lsc_pad_out(i+1,j+3)+... 
                       lsc_pad_out(i+2,j+1) + lsc_pad_out(i+2,j+3) + lsc_pad_out(i+3,j+1) + lsc_pad_out(i+3,j+2) + lsc_pad_out(i+3,j+3))+4)/8);
                end
            end
        end
        
       %% sort the distance map
        [~,Dis_sort_Index] = sort(reshape(Dis',[1,Img_Height*Img_Width]),'descend'); 
        Defected_pos = (Dis_sort_Index(1:Jeehoon_Num));
        
       %% ensure the hot/cold defect pixel pos
        Defected_pos_y = min(floor(Defected_pos/Img_Width)+1,Img_Height);
        Defected_pos_x = min(max(Defected_pos - (Defected_pos_y - 1) *Img_Width,1),Img_Width);

       %% correct the Cold defect point   
       for i = 1:length(Defected_pos)
        switch Dmin(Defected_pos_y(i), Defected_pos_x(i))  %%judge the edge type
            case D1(Defected_pos_y(i), Defected_pos_x(i))  %%vertical edge
                dpc_out(Defected_pos_y(i), Defected_pos_x(i)) = (lsc_pad_out(Defected_pos_y(i), Defected_pos_x(i) + 2) + lsc_pad_out( Defected_pos_y(i) + 4, Defected_pos_x(i) + 2) + 1)/2;
            case D2(Defected_pos_y(i), Defected_pos_x(i))  %%45 degree edge
                dpc_out(Defected_pos_y(i), Defected_pos_x(i)) = (lsc_pad_out(Defected_pos_y(i), Defected_pos_x(i) + 4) + lsc_pad_out( Defected_pos_y(i) + 1, Defected_pos_x(i) + 3) +... 
                    lsc_pad_out(Defected_pos_y(i) + 3, Defected_pos_x(i) + 1) + lsc_pad_out( Defected_pos_y(i) + 4, Defected_pos_x(i)) + 2)/4;
            case D3(Defected_pos_y(i), Defected_pos_x(i))  %%horizontial edge
                dpc_out(Defected_pos_y(i), Defected_pos_x(i)) = (lsc_pad_out( Defected_pos_y(i) + 2, Defected_pos_x(i)) + lsc_pad_out( Defected_pos_y(i) + 2, Defected_pos_x(i) + 4) + 1)/2;
            case D4(Defected_pos_y(i), Defected_pos_x(i))  %%135 degree edge
                dpc_out(Defected_pos_y(i), Defected_pos_x(i)) = (lsc_pad_out( Defected_pos_y(i), Defected_pos_x(i)) + lsc_pad_out( Defected_pos_y(i) + 1, Defected_pos_x(i) + 1) +... 
                    lsc_pad_out(Defected_pos_y(i) + 3, Defected_pos_x(i) + 3) + lsc_pad_out( Defected_pos_y(i) + 4, Defected_pos_x(i) + 4) + 2)/4;
            otherwise
                dpc_out(Defected_pos_y(i), Defected_pos_x(i)) = (lsc_pad_out( Defected_pos_y(i) + 1, Defected_pos_x(i) + 1) + lsc_pad_out( Defected_pos_y(i) + 1, Defected_pos_x(i) + 2) + lsc_pad_out( Defected_pos_y(i) + 1, Defected_pos_x(i) + 3) +... 
                    lsc_pad_out(Defected_pos_y(i) + 2, Defected_pos_x(i) + 1) + lsc_pad_out( Defected_pos_y(i) + 2, Defected_pos_x(i) + 3) + lsc_pad_out( Defected_pos_y(i) + 3, Defected_pos_x(i) + 1) + lsc_pad_out( Defected_pos_y(i) + 3, Defected_pos_x(i) + 2) + ...
                    lsc_pad_out(Defected_pos_y(i) + 3, Defected_pos_x(i) + 3) + 4)/8;
        end 
       end  
       
       dpc_out = Matrix_Clip(dpc_out,top_paramset.BitDepth);
    elseif(mode == 1)  %% chen's method    
        for i = 1:Img_Height
            for j = 1:Img_Width
              %% fetch the local information window and calculate the mid result！！Dif and Avg
                temp_block_1 = lsc_pad_out(i:i+4,j:j+4);
                temp_block = bilinear(temp_block_1);
                sort_out = sort(reshape(temp_block(2:4,2:4),[1,9]));
                GH = sort_out(8); GL = sort_out(2);
                Dif = GH - GL; Avg = (sum(sum(temp_block(2:4,2:4))) - temp_block(3,3) - GH - GL)/6;
                
              %% judge if the cur point is defected and correct it
                if((temp_block(3,3)<(Avg - Dif))||(temp_block(3,3)>(Avg + Dif))) %% cur point is Defected Point
                    dpc_out(i,j) = dpc_correction_process(temp_block(2:4,2:4));
                else
                    dpc_out(i,j) = temp_block(3,3);
                end
            end
        end
        dpc_out = Matrix_Clip(dpc_out,top_paramset.BitDepth);
    else   %Chao-Yi's methods
        for i = 1:Img_Height
            for j = 1:Img_Width
               %% fetch the local information window and calculate the mid result！！est、Dif and Avg
                temp_block_1 = lsc_pad_out(i:i+4,j:j+4);
                temp_block = bilinear(temp_block_1);
                sort_out = sort(reshape(temp_block(2:4,2:4),[1,9])); GH = sort_out(9); 
                
               %% judge if the cur point is defected and correct it
                if((temp_block(3,3)<(GH/8)) || (temp_block(3,3)> (7*GH/8)))
                     est = (temp_block(3,2)+temp_block(3,4)+1)/2;
                     Dif = abs(est-temp_block(3,3));
                     Avg = (temp_block(3,2) + temp_block(3,4) + temp_block(2,3) + temp_block(4,3) +4* temp_block(3,3) + 4)/8;
                     
                   if((Dif>Avg)||(Dif > (bitshift(1,top_paramset.BitDepth) - 1 - Avg)))                  
                        dpc_out(i,j) = dpc_correction_process(temp_block(2:4,2:4));
                   else
                       dpc_out(i,j) = temp_block(3,3);
                   end                  
                else
                    dpc_out(i,j) = temp_block(3,3);
                end
            end
        end
        dpc_out = Matrix_Clip(dpc_out,top_paramset.BitDepth);
    end
else
   dpc_out  = lsc_out; 
end
end

function [out]  = dpc_correction_process(temp_block)
D1 = abs(2*temp_block(2,2) - temp_block(1,2) - temp_block(3,2))/2;
D2 = abs(2*temp_block(2,2) - temp_block(1,3) - temp_block(3,1))/2;
D3 = abs(2*temp_block(2,2) - temp_block(2,1) - temp_block(2,3))/2;
D4 = abs(2*temp_block(2,2) - temp_block(1,1) - temp_block(3,3))/2;
Dmin = min(min(min(D1,D2),D3),D4);

if(Dmin == D1)
   out = (temp_block(1,2) + temp_block(3,2) + 1)/2;
elseif(Dmin == D2)
   out = (temp_block(1,3) + temp_block(3,1) + 1)/2;
elseif(Dmin == D3)
   out = (temp_block(2,1) + temp_block(2,3) + 1)/2;
else
   out = (temp_block(1,1) + temp_block(3,3) + 1)/2;
end
end

function [full_map_out] = bilinear(full_map_in)
[he_in,wd_in] = size(full_map_in);
full_map_out = full_map_in;
full_map_out(1:2:he_in,2:2:wd_in-1) = (full_map_in(1:2:he_in,1:2:wd_in-2) +  full_map_in(1:2:he_in,3:2:wd_in))/2;
full_map_out(2:2:he_in-1,1:2:wd_in) = (full_map_in(1:2:he_in - 2,1:2:wd_in) +  full_map_in(3:2:he_in,1:2:wd_in))/2;
full_map_out(2:2:he_in-1,2:2:wd_in-1) = (full_map_in(1:2:he_in -2,1:2:wd_in-2) + full_map_in(1:2:he_in -2,3:2:wd_in) + full_map_in(3:2:he_in ,1:2:wd_in - 2) +  full_map_in(3:2:he_in,3:2:wd_in))/4;
end