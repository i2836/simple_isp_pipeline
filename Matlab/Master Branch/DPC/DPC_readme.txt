This is the explantion of DPC (Defect Pixel Correction) module, which is always occuring after the SP Noise Removal processes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                      %
%                                                                                                                                                                                                            %
%             1.The DPC process is aimed at Correcting the  Defected pixels in sensors , there exist one test case                                    %         
%                                                                                                                                                                                                            %
%  File Explanation:                                                                                                                                                                                %
%                                                                                                                                                                                                            %
%             1.module_dpc_process.m: main process of dpc , input the SP processed out and output the DPC processed out             %
%             2.dpc_test.m: test process of single dpc module, can be runned seperately beyond isp_top.m                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation��                                                                                                                                                                                                  %
%                                                                                                                                                                                                                                    %
%             1. dpc_eb : 1 ！！ Open the DPC process ,0 ！！Close the dpc process and directly output the input data                                                 %       
%                                                                                                                                                                                                                                    %                                                                                                           %
%             2. module_dpc_param.mode:  enables the difference methods of DPC detection process and correction process                                      %                                             
%                                                              mode 、Jeehoon_Th's method、Chen's method 、Chao's method                                                               % 
%             3. module_dpc_param.Jeehoon_Th:  Predefined Threhold in  Jeehoon_Th's method                                                                                     %
%             4. module_dpc_param.Jeehoon_Num:  Predefined Defected Pixel Number in  Jeehoon_Th's method                                                           %                                                                                                                                         
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%