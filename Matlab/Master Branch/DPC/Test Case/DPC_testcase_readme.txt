There are three cases to test the DPC（Defected Pixel Correction）process:
	case one: general processed out image from the SP module in the isp pipeline， always not included the manually added defected pixels
                case two: manually added 50 defected pixels in the images
                case three: manually added 0.1% defected pixels in the images
The Input Picture Resolution is 1920p*1080p—— rggb CFA pattern downsampled Raw format input, which is  always after SP Processed out.
	