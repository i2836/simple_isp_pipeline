This is the explantion of Demosaic (Raw Domain to full RGB Color Domain Interpolation) module, which is always occuring after the RawDns  processes（ABF）.
For Genenal，here are four demosaic examples into open-source project ，which can be switched the paramter —— "mode"
For more information about the presented four demosaicking methods, please refers to the description in the file ——"module_demosaic_process.m”   
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                     %
%                                                                                                                                                                                                                        %
%             1.The Demosaic process is aimed at recover the Full RGB components caused by the CFA in the Raw Domain image , there exist %
%                one test case                                                                                                                                                                                    %         
%                                                                                                                                                                                                                        %
%  File Explanation:                                                                                                                                                                                            %
%                                                                                                                                                                                                                        %
%             1. module_demosaic_process.m: main process of demosaic, input the rawdns processed out and output the demosaiced             %
%                                                                 processed out                                                                                                                                 %
%             2. demosaic_test.m: test process of single demosaic module, can be runned seperately beyond isp_top.m                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                       %
%                                                                                                                                                                                                                                          %
%             1. demosaic_eb : 1 —— Open the demosaic process , 0 ——Close the demosaic process and directly output the input data                           %        
%                                                                                                                                                                                                                                          %                                                                                                           %
%             2. module_demosaic_param.mode:  control the demosaic methods adopts in the open-source project                                                            %
%                                                                       mode = 0/1/2/3, seperately related to the Bilinear/HAIntp/GBTFIntp/ UVCIntp                                       %                                                                                % 
%             3. module_demosaic_param.Tao:  the threhold which is adopted to judge the block type in the UVCIntp process                                            %                                                                                                            %                                                                                                                                                                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%