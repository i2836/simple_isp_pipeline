%% This is the single test file of the module Demosaic (Raw Domain to RGB full Color Interpolation process),use the test case in the directory to test the Demosaic process.
%% The Position of Demosaic module in the ISP pipeline,is always after the RawDns Process��adopts the ABF here��.
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% For the methods of Demosaic algorithm adopted here, you have the four choose ���� switch the parameter "mode" to changes the adopted demosaic methods
       %% mode 0: simplest Bilinear Interpolation��linear
       %% mode 1: the HA��Hillton-Adam Interpolation����linear+Edge judge,refers to paper����"Adaptive Color Plane Interpolation in Single Sensor Color Electronic Camera"
       %% mode 2: the GBTF(Gradient Based Threhold Free Color Interpoltion)��linear+postprocess��refers to the paper ����"GRADIENT BASED THRESHOLD FREE COLOR FILTER ARRAY INTERPOLATION"
       %% mode 3: the VCD methods��edge judge + linear intp��refers to the paper����"Color Demosaicing Using Variance of Color Differences"
 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_demosaic_param = struct('mode',3,'Tao',2);%%Tao is the paramter of the mode3��UVC 
demosaic_eb = 1;     %% enables the demosaic process
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after RawDns module
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
rawdns_out = fread(fileinptr,[top_paramset.width, top_paramset.height],'uint8');
rawdns_out = rawdns_out';
fclose(fileinptr);

%% Main Process of Demosaic: switch the parameter "mode�� to change the adopted demosaic method
demosaic_out = module_demosaic_process(rawdns_out,module_demosaic_param,top_paramset,demosaic_eb);

%% Disp Result in png format
figure(1),imshow(uint8(rawdns_out));
figure(2),imshow(uint8(demosaic_out));
imwrite(uint8(demosaic_out),'./Test Case/Case/Demosaic_test_out_mode3.png');