%% This is the Process module of Demosaic(Raw Domain to Full RGB color Interpolation),for more detailed information,please look at the Demosaic_readme.txt and testcase_readme.txt
%% The position of the Demosaic module is always after by the RawDns process in the ISP pipeline 
%% when demosaic_eb = 1, enables the Demosaic process ,vice versa.
%% For the methods of Demosaic algorithm adopted here, you have the four choose ���� switch the parameter "mode" to changes the adopted demosaic methods
       %% mode 0: simplest Bilinear Interpolation��linear
       %% mode 1: the HA��Hillton-Adam Interpolation����linear+Edge judge,refers to paper����"Adaptive Color Plane Interpolation in Single Sensor Color Electronic Camera"
       %% mode 2: the GBTF(Gradient Based Threhold Free Color Interpoltion)��linear+postprocess��refers to the paper ����"GRADIENT BASED THRESHOLD FREE COLOR FILTER ARRAY INTERPOLATION"
       %% mode 3: the VCD methods��edge judge + linear intp��refers to the paper����"Color Demosaicing Using Variance of Color Differences"

function [demosaic_out] = module_demosaic_process(rawdns_out,module_demosaic_param,top_paramset,demosaic_eb)
if(demosaic_eb == 1)
    %% Initilize the param set and under-processed data
    if(top_paramset.Auto == 1)
        mode = 0;
        Tao = 2;
    else
        mode = module_demosaic_param.mode;
        Tao = module_demosaic_param.Tao;
    end
    
    switch mode
        case 0
            demosaic_out = Bilinear(rawdns_out,top_paramset.Bayer,top_paramset.BitDepth);
        case 1
            demosaic_out = HAintp(rawdns_out,top_paramset.Bayer,top_paramset.BitDepth);
        case 2
            demosaic_out = GBTFintp(rawdns_out,top_paramset.Bayer,top_paramset.BitDepth);
        case 3
            demosaic_out = UVCIintp(rawdns_out,top_paramset.Bayer,top_paramset.BitDepth,Tao);
        otherwise
            demosaic_out = HAintp(rawdns_out,top_paramset.Bayer,top_paramset.BitDepth);
    end
    demosaic_out = Matrix_Clip(demosaic_out,top_paramset.BitDepth);   
else
    demosaic_out = rawdns_out; %%when the demosaic process is not enabled , output the input raw files, also the raw files
end
end




%% mode 0: Bilinear Interpoltion,3*3 window ,needs to padding 2 rows/cols
function [Bi_out] = Bilinear(mosaiced_image_in,BayerPattern,BitDepth)
mosaiced_image_in = double(mosaiced_image_in);

[he_in,wd_in] = size(mosaiced_image_in);
mosaiced_in = double(padding_array(mosaiced_image_in,2,2));

Bi_out = zeros(he_in,wd_in,3);

% Bilinear Intp
for i = 1:he_in
    for j = 1:wd_in
        if(strcmp(BayerPattern,'rggb'))
            if(mod(i,2) && mod(j,2)) %Original sampled R
                Bi_out(i,j,1) = mosaiced_in(i+2,j+2);
                Bi_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3) + mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/4 ;
                Bi_out(i,j,3) = (mosaiced_in(i+1,j+1) + mosaiced_in(i+1,j+3) + mosaiced_in(i+3,j+1) + mosaiced_in(i+3,j+3))/4;
            elseif(mod(i + 1,2) && mod(j,2)) %bottom-left at Bayer Pattern,Original smapled Gb
                Bi_out(i,j,1) = (mosaiced_in(i + 1,j + 2) + mosaiced_in(i + 3,j + 2))/2;
                Bi_out(i,j,2) = mosaiced_in(i + 2,j + 2);
                Bi_out(i,j,3) = (mosaiced_in(i + 2,j + 1) + mosaiced_in(i + 2,j + 3))/2;
            elseif(mod(i,2) && mod(j + 1,2)) %top-right at Bayer Pattern,original Gr
                Bi_out(i,j,1) = (mosaiced_in(i + 2,j + 1) + mosaiced_in(i + 2,j + 3))/2;
                Bi_out(i,j,2) = mosaiced_in(i + 2,j + 2);
                Bi_out(i,j,3) = (mosaiced_in(i + 1,j + 2)+mosaiced_in(i + 3,j + 2))/2;
            else  %Origian sampled B
                Bi_out(i,j,1) = (mosaiced_in(i+1,j+1) + mosaiced_in(i+1,j+3) + mosaiced_in(i+3,j+1) + mosaiced_in(i+3,j+3))/4;
                Bi_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3) + mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/4 ;
                Bi_out(i,j,3) = mosaiced_in(i+2,j+2);
            end
        elseif(strcmp(BayerPattern,'grbg'))
            if(mod(i,2) && mod(j,2)) %top-left at Bayer Pattern, Original sampled Gr
                Bi_out(i,j,1) = (mosaiced_in(i + 2,j + 1) + mosaiced_in(i + 2,j + 3))/2;
                Bi_out(i,j,2) = mosaiced_in(i + 2,j + 2);
                Bi_out(i,j,3) = (mosaiced_in(i + 1,j + 2)+mosaiced_in(i + 3,j + 2))/2;
            elseif(mod(i + 1,2) && mod(j,2)) %bottom-left at Bayer Pattern,Original smapled B
                Bi_out(i,j,1) = (mosaiced_in(i+1,j+1) + mosaiced_in(i+1,j+3) + mosaiced_in(i+3,j+1) + mosaiced_in(i+3,j+3))/4;
                Bi_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3) + mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/4 ;
                Bi_out(i,j,3) = mosaiced_in(i+2,j+2);
            elseif(mod(i,2) && mod(j + 1,2)) %top-right at Bayer Pattern,original R
                Bi_out(i,j,1) = mosaiced_in(i+2,j+2);
                Bi_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3) + mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/4;
                Bi_out(i,j,3) = (mosaiced_in(i+1,j+1) + mosaiced_in(i+1,j+3) + mosaiced_in(i+3,j+1) + mosaiced_in(i+3,j+3))/4;
            else %Origian sampled Gb
                Bi_out(i,j,1) = (mosaiced_in(i + 1,j + 2)+mosaiced_in(i + 3,j + 2))/2;
                Bi_out(i,j,2) = mosaiced_in(i+2,j+2);
                Bi_out(i,j,3) = (mosaiced_in(i + 2,j + 1) + mosaiced_in(i + 2,j + 3))/2;
            end
        elseif(strcmp(BayerPattern,'gbrg'))
            if(mod(i,2) && mod(j,2)) %top-left at Bayer Pattern, Original sampled Gb
                Bi_out(i,j,1) = (mosaiced_in(i + 1,j + 2)+mosaiced_in(i + 3,j + 2))/2;
                Bi_out(i,j,2) = mosaiced_in(i+2,j+2);
                Bi_out(i,j,3) = (mosaiced_in(i + 2,j + 1) + mosaiced_in(i + 2,j + 3))/2;
            elseif(mod(i + 1,2) && mod(j,2)) %bottom-left at Bayer Pattern,Original smapled R
                Bi_out(i,j,1) = mosaiced_in(i+2,j+2);
                Bi_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3) + mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/4 ;
                Bi_out(i,j,3) = (mosaiced_in(i+1,j+1) + mosaiced_in(i+1,j+3) + mosaiced_in(i+3,j+1) + mosaiced_in(i+3,j+3))/4;
            elseif(mod(i,2) && mod(j + 1,2)) %top-right at Bayer Pattern,original B
                Bi_out(i,j,1) = (mosaiced_in(i+1,j+1) + mosaiced_in(i+1,j+3) + mosaiced_in(i+3,j+1) + mosaiced_in(i+3,j+3))/4;
                Bi_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3) + mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/4 ;
                Bi_out(i,j,3) = mosaiced_in(i+2,j+2);           
            else %Origian sampled Gr
                Bi_out(i,j,1) = (mosaiced_in(i + 2,j + 1) + mosaiced_in(i + 2,j + 3))/2;
                Bi_out(i,j,2) = mosaiced_in(i + 2,j + 2);
                Bi_out(i,j,3) = (mosaiced_in(i + 1,j + 2)+mosaiced_in(i + 3,j + 2))/2;
            end
        else
            if(mod(i,2) && mod(j,2))  %top-left at Bayer Pattern, Original sampled B
                Bi_out(i,j,1) = (mosaiced_in(i+1,j+1) + mosaiced_in(i+1,j+3) + mosaiced_in(i+3,j+1) + mosaiced_in(i+3,j+3))/4;
                Bi_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3) + mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/4 ;
                Bi_out(i,j,3) = mosaiced_in(i+2,j+2);  
            elseif(mod(i + 1,2) && mod(j,2)) %bottom-left at Bayer Pattern,Original smapled Gr
                Bi_out(i,j,1) = (mosaiced_in(i + 2,j + 1) + mosaiced_in(i + 2,j + 3))/2;
                Bi_out(i,j,2) = mosaiced_in(i + 2,j + 2);
                Bi_out(i,j,3) = (mosaiced_in(i + 1,j + 2)+mosaiced_in(i + 3,j + 2))/2;
            elseif(mod(i,2) && mod(j + 1,2)) %top-right at Bayer Pattern,original Gb
                Bi_out(i,j,1) = (mosaiced_in(i + 1,j + 2)+mosaiced_in(i + 3,j + 2))/2;
                Bi_out(i,j,2) = mosaiced_in(i+2,j+2);
                Bi_out(i,j,3) = (mosaiced_in(i + 2,j + 1) + mosaiced_in(i + 2,j + 3))/2;
            else %Origian sampled R
                Bi_out(i,j,1) = mosaiced_in(i+2,j+2);
                Bi_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3) + mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/4;
                Bi_out(i,j,3) = (mosaiced_in(i+1,j+1) + mosaiced_in(i+1,j+3) + mosaiced_in(i+3,j+1) + mosaiced_in(i+3,j+3))/4;
            end            
        end
    end           
end

% Clip to [0,255]
for k = 1:3
   temp =  Bi_out(:,:,k);
   temp( temp > (bitshift(1,BitDepth) - 1)) = bitshift(1,BitDepth) - 1;
   temp(temp < 0) = 0;
   Bi_out(:,:,k) = temp;
end
end

%% mode 1: HA Intp Process
function [HA_out] =  HAintp(mosaiced_image_in,BayerPattern,BitDepth)
[he_in,wd_in] = size(mosaiced_image_in);
mosaiced_in = padding_array(mosaiced_image_in,2,2);
mosaiced_in = double(mosaiced_in);

if(strcmp(BayerPattern,'rggb'))
    % G interpolation
    for i = 1:he_in
        for j = 1:wd_in
            if((mod(i,2) && mod(j,2)) || (mod(i + 1,2) && mod(j + 1,2))) %top-left/bottom-right of Bayer Pattern,downsampled R and B
                 Grad_H = abs(mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3)) + abs(2 * mosaiced_in(i+2,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4));
                 Grad_V = abs(mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2)) + abs(2 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2));
                 if(Grad_H > Grad_V)
                     HA_out(i,j,2) = (mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/2 + (2 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2))/4;
                 elseif(Grad_H < Grad_V)
                     HA_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3))/2 + (2 * mosaiced_in(i+2,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4))/4;
                 else
                     HA_out(i,j,2) = (mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2) + mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3))/4 + (4 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4))/8; 
                 end                                 
            else   
                 HA_out(i,j,2) = mosaiced_in(i+2,j+2);
            end            
        end
    end
    
    temp = HA_out(:,:,2);
    temp(temp>255) = 255;
    temp(temp<0) = 0;
    HA_out(:,:,2) = temp;
    
    G_Pad = padding_array(HA_out(:,:,2),2,2);
     % RB interpolation
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %Original Unsampled R,needs to calculate B
                HA_out(i,j,1) = mosaiced_in(i+2,j+2);
                Res_B = (G_Pad(i+1,j+1) + G_Pad(i+1,j+3) + G_Pad(i+3,j+1) + G_Pad(i+3,j+3)- mosaiced_in(i+1,j+1) - mosaiced_in(i+1,j+3) -mosaiced_in(i+3,j+1) - mosaiced_in(i+3,j+3))/4; 
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            elseif(mod(i,2) && mod(j+1,2)) %Origianl Sampled  Gr,needs to calculate the R and B
                Res_R = (G_Pad(i+2,j+1)+G_Pad(i+2,j+3) - mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3))/2;
                Res_B = (G_Pad(i+1,j+2)+G_Pad(i+3,j+2) - mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2))/2;
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            elseif(mod(i+1,2) && mod(j,2)) %Original Sampled Gb,needs to calculate the R and B
                Res_B = (G_Pad(i+2,j+1)+G_Pad(i+2,j+3) - mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3))/2;
                Res_R = (G_Pad(i+1,j+2)+G_Pad(i+3,j+2) - mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2))/2;
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            else %Original Sampled B ��needs to calculate R                
                Res_R = (G_Pad(i+1,j+1) + G_Pad(i+1,j+3) + G_Pad(i+3,j+1) + G_Pad(i+3,j+3)- mosaiced_in(i+1,j+1) - mosaiced_in(i+1,j+3) -mosaiced_in(i+3,j+1) - mosaiced_in(i+3,j+3))/4; 
                HA_out(i,j,3) = mosaiced_in(i+2,j+2);
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
            end 
        end
    end
    
elseif(strcmp(BayerPattern,'grbg'))
    % G interpolation
    for i = 1:he_in
        for j = 1:wd_in
            if((mod(i,2) && mod(j + 1,2)) || (mod(i + 1,2) && mod(j,2))) %top-left/bottom-right of Bayer Pattern,downsampled R and B
                 Grad_H = abs(mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3)) + abs(2 * mosaiced_in(i+2,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4));
                 Grad_V = abs(mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2)) + abs(2 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2));
                 if(Grad_H > Grad_V)
                     HA_out(i,j,2) = (mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/2 + (2 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2))/4;
                 elseif(Grad_H < Grad_V)
                     HA_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3))/2 + (2 * mosaiced_in(i+2,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4))/4;
                 else
                     HA_out(i,j,2) = (mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2) + mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3))/4 + (4 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4))/8; 
                 end                                 
            else   
                 HA_out(i,j,2) = mosaiced_in(i+2,j+2);
            end            
        end
    end
    
    temp = HA_out(:,:,2);
    temp(temp>255) = 255;
    temp(temp<0) = 0;
    HA_out(:,:,2) = temp;
    
    G_Pad = padding_array(HA_out(:,:,2),2,2);
    % RB interpolation
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %Original Unsampled Gr,needs to calculate B and R
                Res_R = (G_Pad(i+2,j+1)+G_Pad(i+2,j+3) - mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3))/2;
                Res_B = (G_Pad(i+1,j+2)+G_Pad(i+3,j+2) - mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2))/2;
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            elseif(mod(i,2) && mod(j+1,2)) %Origianl Sampled  R,needs to calculate the B
                HA_out(i,j,1) = mosaiced_in(i+2,j+2);
                Res_B = (G_Pad(i+1,j+1) + G_Pad(i+1,j+3) + G_Pad(i+3,j+1) + G_Pad(i+3,j+3)- mosaiced_in(i+1,j+1) - mosaiced_in(i+1,j+3) -mosaiced_in(i+3,j+1) - mosaiced_in(i+3,j+3))/4; 
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B; 
            elseif(mod(i+1,2) && mod(j,2)) %Original Sampled B,needs to calculate the R                 
                Res_R = (G_Pad(i+1,j+1) + G_Pad(i+1,j+3) + G_Pad(i+3,j+1) + G_Pad(i+3,j+3)- mosaiced_in(i+1,j+1) - mosaiced_in(i+1,j+3) -mosaiced_in(i+3,j+1) - mosaiced_in(i+3,j+3))/4; 
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = mosaiced_in(i+2,j+2);
            else %Original Sampled Gb ��needs to calculate R and B
                Res_B = (G_Pad(i+2,j+1)+G_Pad(i+2,j+3) - mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3))/2;
                Res_R = (G_Pad(i+1,j+2)+G_Pad(i+3,j+2) - mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2))/2;
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            end 
        end
    end
    
elseif(strcmp(BayerPattern,'gbrg'))
   % G interpolation
   for i = 1:he_in
        for j = 1:wd_in
            if((mod(i,2) && mod(j + 1,2)) || (mod(i + 1,2) && mod(j,2))) %top-left/bottom-right of Bayer Pattern,downsampled R and B
                 Grad_H = abs(mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3)) + abs(2 * mosaiced_in(i+2,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4));
                 Grad_V = abs(mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2)) + abs(2 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2));
                 if(Grad_H > Grad_V)
                     HA_out(i,j,2) = (mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/2 + (2 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2))/4;
                 elseif(Grad_H < Grad_V)
                     HA_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3))/2 + (2 * mosaiced_in(i+2,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4))/4;
                 else
                     HA_out(i,j,2) = (mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2) + mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3))/4 + (4 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4))/8; 
                 end                                 
            else   
                 HA_out(i,j,2) = mosaiced_in(i+2,j+2);
            end            
        end
   end
   
    temp = HA_out(:,:,2);
    temp(temp>255) = 255;
    temp(temp<0) = 0;
    HA_out(:,:,2) = temp;
    
   G_Pad = padding_array(HA_out(:,:,2),2,2);
   % RB interpolation
   for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %Original Unsampled Gb,needs to calculate B and R
                Res_B = (G_Pad(i+2,j+1)+G_Pad(i+2,j+3) - mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3))/2;
                Res_R = (G_Pad(i+1,j+2)+G_Pad(i+3,j+2) - mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2))/2;
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            elseif(mod(i,2) && mod(j+1,2)) %Origianl Sampled  B,needs to calculate the R
                Res_R = (G_Pad(i+1,j+1) + G_Pad(i+1,j+3) + G_Pad(i+3,j+1) + G_Pad(i+3,j+3)- mosaiced_in(i+1,j+1) - mosaiced_in(i+1,j+3) -mosaiced_in(i+3,j+1) - mosaiced_in(i+3,j+3))/4; 
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = mosaiced_in(i+2,j+2);
            elseif(mod(i+1,2) && mod(j,2)) %Original Sampled R,needs to calculate the B                 
                HA_out(i,j,1) = mosaiced_in(i+2,j+2);
                Res_B = (G_Pad(i+1,j+1) + G_Pad(i+1,j+3) + G_Pad(i+3,j+1) + G_Pad(i+3,j+3)- mosaiced_in(i+1,j+1) - mosaiced_in(i+1,j+3) -mosaiced_in(i+3,j+1) - mosaiced_in(i+3,j+3))/4; 
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            else %Original Sampled Gr ��needs to calculate R and B
                Res_R = (G_Pad(i+2,j+1)+G_Pad(i+2,j+3) - mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3))/2;
                Res_B = (G_Pad(i+1,j+2)+G_Pad(i+3,j+2) - mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2))/2;
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            end 
        end
    end
else
   % G interpolation
   for i = 1:he_in
        for j = 1:wd_in
            if((mod(i,2) && mod(j,2)) || (mod(i + 1,2) && mod(j + 1,2))) %top-left/bottom-right of Bayer Pattern,downsampled R and B
                 Grad_H = abs(mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3)) + abs(2 * mosaiced_in(i+2,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4));
                 Grad_V = abs(mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2)) + abs(2 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2));
                 if(Grad_H > Grad_V)
                     HA_out(i,j,2) = (mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2))/2 + (2 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2))/4;
                 elseif(Grad_H < Grad_V)
                     HA_out(i,j,2) = (mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3))/2 + (2 * mosaiced_in(i+2,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4))/4;
                 else
                     HA_out(i,j,2) = (mosaiced_in(i+1,j+2) + mosaiced_in(i+3,j+2) + mosaiced_in(i+2,j+1) + mosaiced_in(i+2,j+3))/4 + (4 * mosaiced_in(i+2,j+2) - mosaiced_in(i,j+2) - mosaiced_in(i+4,j+2) - mosaiced_in(i+2,j) - mosaiced_in(i+2,j+4))/8; 
                 end                                 
            else   
                 HA_out(i,j,2) = mosaiced_in(i+2,j+2);
            end            
        end
   end
   
    temp = HA_out(:,:,2);
    temp(temp>255) = 255;
    temp(temp<0) = 0;
    HA_out(:,:,2) = temp;
    
   G_Pad = padding_array(HA_out(:,:,2),2,2);
    % RB interpolation 
   for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %Original Unsampled B,needs to calculate  R
                Res_R = (G_Pad(i+1,j+1) + G_Pad(i+1,j+3) + G_Pad(i+3,j+1) + G_Pad(i+3,j+3)- mosaiced_in(i+1,j+1) - mosaiced_in(i+1,j+3) -mosaiced_in(i+3,j+1) - mosaiced_in(i+3,j+3))/4; 
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = mosaiced_in(i+2,j+2);
            elseif(mod(i,2) && mod(j+1,2)) %Origianl Sampled  Gb,needs to calculate the R and B
                Res_B = (G_Pad(i+2,j+1)+G_Pad(i+2,j+3) - mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3))/2;
                Res_R = (G_Pad(i+1,j+2)+G_Pad(i+3,j+2) - mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2))/2;
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            elseif(mod(i+1,2) && mod(j,2)) %Original Sampled Gr,needs to calculate the B and R              
                Res_R = (G_Pad(i+2,j+1)+G_Pad(i+2,j+3) - mosaiced_in(i+2,j+1) - mosaiced_in(i+2,j+3))/2;
                Res_B = (G_Pad(i+1,j+2)+G_Pad(i+3,j+2) - mosaiced_in(i+1,j+2) - mosaiced_in(i+3,j+2))/2;
                HA_out(i,j,1) = G_Pad(i+2,j+2) - Res_R;
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            else %Original Sampled R ��needs to calculate B
                HA_out(i,j,1) = mosaiced_in(i+2,j+2);
                Res_B = (G_Pad(i+1,j+1) + G_Pad(i+1,j+3) + G_Pad(i+3,j+1) + G_Pad(i+3,j+3)- mosaiced_in(i+1,j+1) - mosaiced_in(i+1,j+3) -mosaiced_in(i+3,j+1) - mosaiced_in(i+3,j+3))/4; 
                HA_out(i,j,3) = G_Pad(i+2,j+2) - Res_B;
            end 
        end
   end   
end

for k = 1:3
    temp = HA_out(:,:,k);
    temp(temp>(bitshift(1,BitDepth) - 1 )) = bitshift(1,BitDepth) - 1;
    temp(temp<0) = 0;
    HA_out(:,:,k) = temp;
end

end

%% mode 2: GBTF Intp process
function [GBTF_out] = GBTFintp(image_mosaiced_in,BayerPattern,BitDepth)
[he_in,wd_in] = size(image_mosaiced_in);
mosaiced_in = double(padding_array(image_mosaiced_in,2,2));

% Variables define
temp_H = zeros(he_in,wd_in);
temp_V = zeros(he_in,wd_in);

map_H = zeros(he_in,wd_in);
map_V = zeros(he_in,wd_in);

DH = zeros(he_in+8,wd_in+8);
DV = zeros(he_in+8,wd_in+8);

Wn = zeros(he_in,wd_in);
Ws = zeros(he_in,wd_in);
We = zeros(he_in,wd_in);
Ww = zeros(he_in,wd_in);

smoothed_Color_Differnence = zeros(he_in,wd_in);

R_out = zeros(he_in,wd_in);G_out = zeros(he_in,wd_in);B_out = zeros(he_in,wd_in);

Conv_H = [-1,2,2,2,-1]/4;
Conv_V = Conv_H';

% get the HA temp result in Vertical/Horzontial Directions
for i =1:he_in
    for j = 1:wd_in
        temp_H(i,j) = sum(Conv_H.* mosaiced_in(i+2,j:j+4));
        temp_V(i,j) = sum(Conv_V.* mosaiced_in(i:i+4,j+2));
    end
end


% get the unsmoothed color difference map
% temp_H =imfilter(mosaiced_in,Conv_H);
% temp_V =imfilter(mosaiced_in,Conv_V);

if(strcmp(BayerPattern,'rggb') || strcmp(BayerPattern,'bggr'))
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i+j,2) == 0) % RB position
                map_H(i,j) = temp_H(i,j) - mosaiced_in(i+2,j+2);
                map_V(i,j) = temp_V(i,j) - mosaiced_in(i+2,j+2);
            else % Gr/Gb position
                map_H(i,j) = mosaiced_in(i+2,j+2) - temp_H(i,j);
                map_V(i,j) = mosaiced_in(i+2,j+2) - temp_V(i,j);    
            end
        end
    end
else %if(strcmp(BayerPattern,'grbg/gbrg')
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i+j,2) == 1) % RB position
                map_H(i,j) = temp_H(i,j) - mosaiced_in(i+2,j+2);
                map_V(i,j) = temp_V(i,j) - mosaiced_in(i+2,j+2);
            else % Gr/Gb position
                map_H(i,j) = mosaiced_in(i+2,j+2) - temp_H(i,j);
                map_V(i,j) = mosaiced_in(i+2,j+2) - temp_V(i,j);    
            end
        end
    end    
end
    
%  smooth the unsmoothed color difference map
map_H_padded = padding_array(map_H,5,5);
map_V_padded = padding_array(map_V,5,5);

F_ns = [ 1 1 1 1 1] /5;
F_we = F_ns';

for i = 1:he_in+8
    for j = 1:wd_in+8
        DH(i,j) = abs(map_H_padded(i+1,j) - map_H_padded(i+1,j+2));
        DV(i,j) = abs(map_V_padded(i,j+1) - map_V_padded(i+2,j+1));
    end
end

for i =1:he_in
    for j =1:wd_in
        Vector_N = map_V_padded(i+1:i+5,j+5);Vector_S = map_V_padded(i+5:i+9,j+5);
        Vector_W = map_H_padded(i+5,j+5:j+9);Vector_E = map_H_padded(i+5,j+1:j+5);
        
        Wn(i,j) = 1./(1 + (sum(sum(DV(i:i+4,j+2:j+6),2)))^2);
        Ws(i,j) = 1./(1 + (sum(sum(DV(i+4:i+8,j+2:j+6),2)))^2);
        Ww(i,j) = 1./(1 + (sum(sum(DH(i+2:i+6,j:j+4),2)))^2);
        We(i,j) = 1./(1 + (sum(sum(DH(i+2:i+6,j+4:j+8),2)))^2);
        
        sum_weight =  Wn(i,j) + Ws(i,j) + Ww(i,j) + We(i,j);
        sum_value = Wn(i,j).*F_ns*Vector_N + Ws(i,j).*F_ns*Vector_S + We(i,j).*Vector_E*F_we + Ww(i,j).*Vector_W*F_we;
        smoothed_Color_Differnence(i,j) = sum_value ./sum_weight;
    end
end

% full G interpolation
if(strcmp(BayerPattern,'rggb')||strcmp(BayerPattern,'bggr'))
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i+j,2) == 0) % RB postion,needs to add smoothed Color Difference
                G_out(i,j) = mosaiced_in(i+2,j+2) + smoothed_Color_Differnence(i,j);
            else % G postion, don't needs to add smoothed Color Difference
                G_out(i,j) = mosaiced_in(i+2,j+2);
            end
        end
    end
else
   for i = 1:he_in
        for j = 1:wd_in
            if(mod(i+j,2) == 1)
                G_out(i,j) = mosaiced_in(i+2,j+2) + smoothed_Color_Differnence(i,j);
            else
                G_out(i,j) = mosaiced_in(i+2,j+2);
            end    
        end
   end
end

temp = G_out;
temp(temp>(bitshift(1,BitDepth) -1)) = bitshift(1,BitDepth) -1;
temp(temp<0) = 0;
G_out = temp;

% RB interpolation after G interpolated,needs G interpolated data
RBconv_filter = [0 0 -1 0 -1 0 0;
                 0 0  0 0  0 0 0;
                 -1 0 10 0 10 0 -1;
                 0 0  0 0  0 0 0;
                 -1 0 10 0 10 0 -1;
                 0 0  0 0  0 0 0;
                 0 0 -1 0 -1 0 0;
                 ];
RBconv_filter = RBconv_filter/32;

img_in_padded = double(padding_array(image_mosaiced_in,4,4));
smoothed_Color_Differnence_padded = padding_array(smoothed_Color_Differnence,4,4);
G_out_padded = padding_array(G_out,2,2);
if(strcmp(BayerPattern,'rggb'))
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %R position
                R_out(i,j) = img_in_padded(i+4,j+4);
                B_out(i,j) = G_out_padded(i+2,j+2) - sum(sum(smoothed_Color_Differnence_padded (i+1:i+7,j+1:j+7).* RBconv_filter,2));
            elseif(mod(i+1,2) && mod(j+1,2)) % B position
                B_out(i,j) = img_in_padded(i+4,j+4);
                R_out(i,j) = G_out_padded(i+2,j+2) - sum(sum(smoothed_Color_Differnence_padded (i+1:i+7,j+1:j+7).* RBconv_filter,2));
            elseif(mod(i,2) && mod(j+1,2)) %Gr position
                R_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+2,j+1) - img_in_padded(i+4,j+3))+(G_out_padded(i+2,j+3) - img_in_padded(i+4,j+5)))/2;
                B_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+1,j+2) - img_in_padded(i+3,j+4))+(G_out_padded(i+3,j+2) - img_in_padded(i+5,j+4)))/2;
            else %Gb position
                R_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+1,j+2) - img_in_padded(i+3,j+4))+(G_out_padded(i+3,j+2) - img_in_padded(i+5,j+4)))/2;
                B_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+2,j+1) - img_in_padded(i+4,j+3))+(G_out_padded(i+2,j+3) - img_in_padded(i+4,j+5)))/2;               
            end
        end
    end
elseif(strcmp(BayerPattern,'grbg'))
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %Gr position
                R_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+2,j+1) - img_in_padded(i+4,j+3))+(G_out_padded(i+2,j+3) - img_in_padded(i+4,j+5)))/2;
                B_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+1,j+2) - img_in_padded(i+3,j+4))+(G_out_padded(i+3,j+2) - img_in_padded(i+5,j+4)))/2;
            elseif(mod(i+1,2) && mod(j+1,2)) % Gb position
                R_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+1,j+2) - img_in_padded(i+3,j+4))+(G_out_padded(i+3,j+2) - img_in_padded(i+5,j+4)))/2;
                B_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+2,j+1) - img_in_padded(i+4,j+3))+(G_out_padded(i+2,j+3) - img_in_padded(i+4,j+5)))/2;               
            elseif(mod(i,2) && mod(j+1,2)) %r position
                R_out(i,j) = img_in_padded(i+4,j+4);
                B_out(i,j) = G_out_padded(i+2,j+2) - sum(sum(smoothed_Color_Differnence_padded(i+1:i+7,j+1:j+7).* RBconv_filter,2));
            else %b position
                B_out(i,j) = img_in_padded(i+4,j+4);
                R_out(i,j) = G_out_padded(i+2,j+2) - sum(sum(smoothed_Color_Differnence_padded(i+1:i+7,j+1:j+7).* RBconv_filter,2));         
            end
        end
    end
elseif(strcmp(BayerPattern,'gbrg'))
   for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %Gb position
                R_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+1,j+2) - img_in_padded(i+3,j+4))+(G_out_padded(i+3,j+2) - img_in_padded(i+5,j+4)))/2;
                B_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+2,j+1) - img_in_padded(i+4,j+3))+(G_out_padded(i+2,j+3) - img_in_padded(i+4,j+5)))/2;              
            elseif(mod(i+1,2) && mod(j+1,2)) % Gr position
                R_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+2,j+1) - img_in_padded(i+4,j+3))+(G_out_padded(i+2,j+3) - img_in_padded(i+4,j+5)))/2;
                B_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+1,j+2) - img_in_padded(i+3,j+4))+(G_out_padded(i+3,j+2) - img_in_padded(i+5,j+4)))/2;
            elseif(mod(i,2) && mod(j+1,2)) %b position
                B_out(i,j) = img_in_padded(i+4,j+4);
                R_out(i,j) = G_out_padded(i+2,j+2) - sum(sum(smoothed_Color_Differnence_padded(i+1:i+7,j+1:j+7).* RBconv_filter,2));   
            else %r position
                R_out(i,j) = img_in_padded(i+4,j+4);
                B_out(i,j) = G_out_padded(i+2,j+2) - sum(sum(smoothed_Color_Differnence_padded(i+1:i+7,j+1:j+7).* RBconv_filter,2));
            end
        end
    end 
else %bggr 
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %b position
                B_out(i,j) = img_in_padded(i+4,j+4);
                R_out(i,j) = G_out_padded(i+2,j+2) - sum(sum(smoothed_Color_Differnence_padded(i+1:i+7,j+1:j+7).* RBconv_filter,2));   
            elseif(mod(i+1,2) && mod(j+1,2)) % r position
                R_out(i,j) = img_in_padded(i+4,j+4);
                B_out(i,j) = G_out_padded(i+2,j+2) - sum(sum(smoothed_Color_Differnence_padded(i+1:i+7,j+1:j+7).* RBconv_filter,2));
            elseif(mod(i,2) && mod(j+1,2)) %gb position
                R_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+1,j+2) - img_in_padded(i+3,j+4))+(G_out_padded(i+3,j+2) - img_in_padded(i+5,j+4)))/2;
                B_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+2,j+1) - img_in_padded(i+4,j+3))+(G_out_padded(i+2,j+3) - img_in_padded(i+4,j+5)))/2;              
            else %gr position
                R_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+2,j+1) - img_in_padded(i+4,j+3))+(G_out_padded(i+2,j+3) - img_in_padded(i+4,j+5)))/2;
                B_out(i,j) = G_out_padded(i+2,j+2) - ((G_out_padded(i+1,j+2) - img_in_padded(i+3,j+4))+(G_out_padded(i+3,j+2) - img_in_padded(i+5,j+4)))/2;
            end
        end
    end 
end

temp = R_out;
temp(temp>(bitshift(1,BitDepth) -1)) = bitshift(1,BitDepth) -1;
temp(temp<0) = 0;
R_out = temp;

temp = B_out;
temp(temp>(bitshift(1,BitDepth) -1)) = bitshift(1,BitDepth) -1;
temp(temp<0) = 0;
B_out = temp;

GBTF_out = cat(3,R_out,G_out,B_out);
end

%% mode 3:VCD Intp Process
function [UVC_out] =  UVCIintp(mosaiced_image_in,BayerPattern,BitDepth,Tao)
[he_in,wd_in] = size(mosaiced_image_in);
mosaiced_in = double(padding_array(mosaiced_image_in,6,6));
UVC_out = zeros(he_in,wd_in,3);

% G interpolation
if(strcmp(BayerPattern,'rggb') || strcmp(BayerPattern,'bggr'))
    for i =1:he_in
        for j =1:wd_in
            if(mod(i+j,2) == 0)
                UVC_out(i,j,2) = UVC_Gintp(mosaiced_in(i:i+12,j:j+12),Tao);
            else
                UVC_out(i,j,2) = mosaiced_in(i+6,j+6);
            end
        end
    end    
else
    for i =1:he_in
        for j =1:wd_in
            if(mod(i+j,2) == 1)
                UVC_out(i,j,2) = UVC_Gintp(mosaiced_in(i:i+12,j:j+12),Tao);
            else
                UVC_out(i,j,2) = mosaiced_in(i+6,j+6);
            end
        end
    end
end  
    
temp = UVC_out(:,:,2);
temp(temp>(bitshift(1,BitDepth)  - 1)) = bitshift(1,BitDepth)  - 1;
temp(temp<0) = 0;
UVC_out(:,:,2) = temp;

%RB interpolation
G_padded = padding_array(UVC_out(:,:,2),2,2);

if(strcmp(BayerPattern,'rggb'))
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %R position
                Color_difference_sum = (G_padded(i+1,j+1) +G_padded(i+1,j+3) +G_padded(i+3,j+1) +G_padded(i+3,j+3)) - (mosaiced_in(i+5,j+5) + mosaiced_in(i+5,j+7) + mosaiced_in(i+7,j+5) + mosaiced_in(i+7,j+7)); 
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6);
                UVC_out(i,j,3) = G_padded(i+2,j+2) -  Color_difference_sum/4;
            elseif(mod(i + 1,2) && mod(j + 1,2)) %B position
                Color_difference_sum = (G_padded(i+1,j+1) + G_padded(i+1,j+3) + G_padded(i+3,j+1) +G_padded(i+3,j+3)) - (mosaiced_in(i+5,j+5) + mosaiced_in(i+5,j+7) + mosaiced_in(i+7,j+5) + mosaiced_in(i+7,j+7)); 
                UVC_out(i,j,1) =  G_padded(i+2,j+2) -  Color_difference_sum/4;               
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6);
            elseif(mod(i,2) && mod(j+1,2)) % Gr position
                Color_difference_H = (G_padded(i+2,j+1)+G_padded(i+2,j+3)) - (mosaiced_in(i+6,j+5)+mosaiced_in(i+6,j+7));
                Color_difference_V = (G_padded(i+1,j+2)+G_padded(i+3,j+2)) - (mosaiced_in(i+5,j+6)+mosaiced_in(i+7,j+6));
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6) - Color_difference_H/2;
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6) - Color_difference_V/2;
            else  %Gb positioni
                Color_difference_H = (G_padded(i+2,j+1)+G_padded(i+2,j+3)) - (mosaiced_in(i+6,j+5)+mosaiced_in(i+6,j+7));
                Color_difference_V = (G_padded(i+1,j+2)+G_padded(i+3,j+2)) - (mosaiced_in(i+5,j+6)+mosaiced_in(i+7,j+6));
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6) - Color_difference_V/2;
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6) - Color_difference_H/2;               
            end            
        end
    end
elseif(strcmp(BayerPattern,'grbg'))
     for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %Gr position
                Color_difference_H = (G_padded(i+2,j+1)+G_padded(i+2,j+3)) - (mosaiced_in(i+6,j+5)+mosaiced_in(i+6,j+7));
                Color_difference_V = (G_padded(i+1,j+2)+G_padded(i+3,j+2)) - (mosaiced_in(i+5,j+6)+mosaiced_in(i+7,j+6));
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6) - Color_difference_H/2;
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6) - Color_difference_V/2;
            elseif(mod(i + 1,2) && mod(j + 1,2)) %Gb position
                Color_difference_H = (G_padded(i+2,j+1)+G_padded(i+2,j+3)) - (mosaiced_in(i+6,j+5)+mosaiced_in(i+6,j+7));
                Color_difference_V = (G_padded(i+1,j+2)+G_padded(i+3,j+2)) - (mosaiced_in(i+5,j+6)+mosaiced_in(i+7,j+6));
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6) - Color_difference_V/2;
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6) - Color_difference_H/2;  
            elseif(mod(i,2) && mod(j+1,2)) % R position
                Color_difference_sum = (G_padded(i+1,j+1) + G_padded(i+1,j+3) + G_padded(i+3,j+1) + G_padded(i+3,j+3)) - (mosaiced_in(i+5,j+5) + mosaiced_in(i+5,j+7) + mosaiced_in(i+7,j+5) + mosaiced_in(i+7,j+7)); 
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6);
                UVC_out(i,j,3) = G_padded(i+2,j+2) -  Color_difference_sum/4;
            else  %B positioni
                Color_difference_sum = (G_padded(i+1,j+1) + G_padded(i+1,j+3) + G_padded(i+3,j+1) + G_padded(i+3,j+3)) - (mosaiced_in(i+5,j+5) + mosaiced_in(i+5,j+7) + mosaiced_in(i+7,j+5) + mosaiced_in(i+7,j+7)); 
                UVC_out(i,j,1) =  G_padded(i+2,j+2) -  Color_difference_sum/4;               
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6);           
            end            
        end
    end
elseif(strcmp(BayerPattern,'gbrg'))
     for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %Gb position
                Color_difference_H = (G_padded(i+2,j+1)+G_padded(i+2,j+3)) - (mosaiced_in(i+6,j+5)+mosaiced_in(i+6,j+7));
                Color_difference_V = (G_padded(i+1,j+2)+G_padded(i+3,j+2)) - (mosaiced_in(i+5,j+6)+mosaiced_in(i+7,j+6));
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6) - Color_difference_V/2;
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6) - Color_difference_H/2;
            elseif(mod(i + 1,2) && mod(j + 1,2)) %Gr position
                Color_difference_H = (G_padded(i+2,j+1)+G_padded(i+2,j+3)) - (mosaiced_in(i+6,j+5)+mosaiced_in(i+6,j+7));
                Color_difference_V = (G_padded(i+1,j+2)+G_padded(i+3,j+2)) - (mosaiced_in(i+5,j+6)+mosaiced_in(i+7,j+6));
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6) - Color_difference_H/2;
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6) - Color_difference_V/2;
            elseif(mod(i,2) && mod(j+1,2)) % B position
                Color_difference_sum = (G_padded(i+1,j+1) +G_padded(i+1,j+3) +G_padded(i+3,j+1) +G_padded(i+3,j+3)) - (mosaiced_in(i+5,j+5) + mosaiced_in(i+5,j+7) + mosaiced_in(i+7,j+5) + mosaiced_in(i+7,j+7)); 
                UVC_out(i,j,1) =  G_padded(i+2,j+2) -  Color_difference_sum/4;               
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6);
            else  %R positioni
                Color_difference_sum = (G_padded(i+1,j+1) + G_padded(i+1,j+3) + G_padded(i+3,j+1) +G_padded(i+3,j+3)) - (mosaiced_in(i+5,j+5) + mosaiced_in(i+5,j+7) + mosaiced_in(i+7,j+5) + mosaiced_in(i+7,j+7)); 
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6);
                UVC_out(i,j,3) = G_padded(i+2,j+2) -  Color_difference_sum/4;       
            end            
        end
    end
else
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i,2) && mod(j,2)) %B position
                Color_difference_sum = (G_padded(i+1,j+1) + G_padded(i+1,j+3) + G_padded(i+3,j+1) + G_padded(i+3,j+3)) - (mosaiced_in(i+5,j+5) + mosaiced_in(i+5,j+7) + mosaiced_in(i+7,j+5) + mosaiced_in(i+7,j+7)); 
                UVC_out(i,j,1) =  G_padded(i+2,j+2) -  Color_difference_sum/4;               
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6);
            elseif(mod(i + 1,2) && mod(j + 1,2)) %R position
                Color_difference_sum = (G_padded(i+1,j+1) + G_padded(i+1,j+3) + G_padded(i+3,j+1) + G_padded(i+3,j+3)) - (mosaiced_in(i+5,j+5) + mosaiced_in(i+5,j+7) + mosaiced_in(i+7,j+5) + mosaiced_in(i+7,j+7)); 
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6);
                UVC_out(i,j,3) = G_padded(i+2,j+2) -  Color_difference_sum/4; 
            elseif(mod(i,2) && mod(j+1,2)) % Gb position
                Color_difference_H = (G_padded(i+2,j+1)+G_padded(i+2,j+3)) - (mosaiced_in(i+6,j+5)+mosaiced_in(i+6,j+7));
                Color_difference_V = (G_padded(i+1,j+2)+G_padded(i+3,j+2)) - (mosaiced_in(i+5,j+6)+mosaiced_in(i+7,j+6));
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6) - Color_difference_V/2;
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6) - Color_difference_H/2;
            else  %Gr positioni
                Color_difference_H = (G_padded(i+2,j+1)+G_padded(i+2,j+3)) - (mosaiced_in(i+6,j+5)+mosaiced_in(i+6,j+7));
                Color_difference_V = (G_padded(i+1,j+2)+G_padded(i+3,j+2)) - (mosaiced_in(i+5,j+6)+mosaiced_in(i+7,j+6));
                UVC_out(i,j,1) = mosaiced_in(i+6,j+6) - Color_difference_H/2;
                UVC_out(i,j,3) = mosaiced_in(i+6,j+6) - Color_difference_V/2;
            end            
        end
    end
end

% clip
for k =1:3
    temp = UVC_out(:,:,k);
    temp(temp>(bitshift(1,BitDepth) - 1)) = bitshift(1,BitDepth) - 1;
    temp(temp<0) = 0;
    UVC_out(:,:,k) = temp;
end

R_pad = padding_array(UVC_out(:,:,1),2,2);
B_pad = padding_array(UVC_out(:,:,3),2,2);
% refine process,here only refines G
if(strcmp(BayerPattern,'rggb'))
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i+j,2) == 0) % RB position
                if(mod(i,2) == 1) % R position
                    UVC_out(i,j,2) = UVC_Clip(UVC_G_refine(mosaiced_in(i+4:i+8,j+4:j+8),R_pad(i+1:i+3,j+1:j+3)),BitDepth);
                else % B position
                    UVC_out(i,j,2) = UVC_Clip(UVC_G_refine(mosaiced_in(i+4:i+8,j+4:j+8),B_pad(i+1:i+3,j+1:j+3)),BitDepth);
                end
            end
        end
    end            
elseif(strcmp(BayerPattern,'bggr'))
   for i = 1:he_in
        for j = 1:wd_in
            if(mod(i+j,2) == 0)  % RB position
                if(mod(i,2) == 1) % B position
                    UVC_out(i,j,2) = UVC_Clip(UVC_G_refine(mosaiced_in(i+4:i+8,j+4:j+8),B_pad(i+1:i+3,j+1:j+3)),BitDepth);
                else  % R position
                    UVC_out(i,j,2) = UVC_Clip(UVC_G_refine(mosaiced_in(i+4:i+8,j+4:j+8),R_pad(i+1:i+3,j+1:j+3)),BitDepth);
                end
            end
        end
    end  
elseif(strcmp(BayerPattern,'gbrg'))
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i+j,2) == 1)  % RB position
                if(mod(i,2) == 1) % B position
                    UVC_out(i,j,2) = UVC_Clip(UVC_G_refine(mosaiced_in(i+4:i+8,j+4:j+8),B_pad(i+1:i+3,j+1:j+3)),BitDepth);
                else  % R position
                    UVC_out(i,j,2) = UVC_Clip(UVC_G_refine(mosaiced_in(i+4:i+8,j+4:j+8),R_pad(i+1:i+3,j+1:j+3)),BitDepth);
                end
            end
        end
    end  
else  %grbg
    for i = 1:he_in
        for j = 1:wd_in
            if(mod(i+j,2) == 1)  % RB position
                if(mod(i,2) == 1) % R position
                    UVC_out(i,j,2) = UVC_Clip(UVC_G_refine(mosaiced_in(i+4:i+8,j+4:j+8),R_pad(i+1:i+3,j+1:j+3)),BitDepth);
                else  % B position
                    UVC_out(i,j,2) = UVC_Clip(UVC_G_refine(mosaiced_in(i+4:i+8,j+4:j+8),B_pad(i+1:i+3,j+1:j+3)),BitDepth);
                end
            end
        end
    end 
end

end

function [out] = UVC_Clip(pix_in,bit)
if(pix_in > (bitshift(1,bit) - 1))
    out = bitshift(1,bit) - 1;
elseif(pix_in<0.0)
    out = 0.0;
else
    out = pix_in;
end
end

function [G_out] = UVC_Gintp(block_in,Tao)
LH = 0;LV=0;
for k = 5:9
   LH = LH + abs(block_in(k,5) - block_in(k,7))+ abs(block_in(k,9) - block_in(k,7)) + abs(block_in(k,6) - block_in(k,7)) + abs(block_in(k,8) - block_in(k,7));
   LV = LV + abs(block_in(5,k) - block_in(7,k))+ abs(block_in(9,k) - block_in(7,k)) + abs(block_in(6,k) - block_in(7,k)) + abs(block_in(8,k) - block_in(7,k));
end

e = max((1+LH)/(1+LV),(1+LV)/(1+LH));

if((e > Tao) && (LH<LV))
    G_out = (block_in(7,6) + block_in(7,8))/2 + (2 * block_in(7,7) - block_in(7,5) - block_in(7,9))/4;
elseif(e > Tao) && (LV<LH)
    G_out = (block_in(6,7) + block_in(8,7))/2 + (2 * block_in(7,7) - block_in(5,7) - block_in(9,7))/4;
elseif( e > Tao) && (LV == LH)
    temp_G = (block_in(7,6) + block_in(6,7) + block_in(7,8) + block_in(8,7))/4;
    temp_C = (4* block_in(7,7) - block_in(5,7) - block_in(9,7) -block_in(7,5) - block_in(7,9))/8;
    G_out = temp_G + temp_C;
else
    G_out = UVC_flag_Gintp(block_in);
end
end

function [G_flat_out] = UVC_flag_Gintp(block_in)
Grad_H =0; Grad_V =0;  Grad_D =0;
DHH = zeros(1,9); DVV = zeros(1,9); DGH = zeros(1,9);DGV = zeros(1,9);
for i = 1:2:9
    DHH(i) = block_in(7,i+2) - ((block_in(7,i+1) + block_in(7,i+3))/2+(2*block_in(7,i+2) - block_in(7,i) - block_in(7,i+4))/4);
    DVV(i) = block_in(i+2,7) - ((block_in(i+1,7) + block_in(i+3,7))/2+(2*block_in(i+2,7) - block_in(i,7) - block_in(i+4,7))/4);
    DGH(i) = block_in(7,i+2) - (block_in(6,i+2)+block_in(8,i+2)+block_in(7,i+1)+block_in(7,i+3))/4 - (4*block_in(7,i+2) - block_in(5,i+2) - block_in(9,i+2) - block_in(7,i) - block_in(7,i+4))/8;
    DGV(i) = block_in(i+2,7) - (block_in(i+2,6)+block_in(i+2,8)+block_in(i+1,7)+block_in(i+3,7))/4 - (4*block_in(i+2,7) - block_in(i+2,5) - block_in(i+2,9) - block_in(i,7) - block_in(i+4,7))/8;
end

for i = 2:2:8
     DHH(i) = (DHH(i-1)+DHH(i+1))/2;
     DVV(i) = (DVV(i-1)+DVV(i+1))/2;
     DGH(i) = (DGH(i-1)+DGH(i+1))/2;
     DGV(i) = (DGV(i-1)+DGV(i+1))/2;
end

ave_grad_H = sum(DHH(:));ave_grad_V = sum(DVV(:));ave_grad_DH = sum(DGH(:));ave_grad_DV = sum(DGV(:));
ave_grad_H = ave_grad_H/9; ave_grad_V= ave_grad_V/9; ave_grad_DH = ave_grad_DH/9;ave_grad_DV = ave_grad_DV/9;

for i = 1:9
    Grad_H = Grad_H + (DHH(i)-ave_grad_H)^2; 
    Grad_V = Grad_V + (DVV(i)-ave_grad_V)^2; 
    Grad_D = Grad_D + (DGH(i)-ave_grad_DH)^2 +(DGV(i) - ave_grad_DV)^2;
end

Grad_H = Grad_H/9; Grad_V = Grad_V/9; Grad_D = Grad_D/18;
Dir = min(Grad_H,min(Grad_V,Grad_D));

if(Dir == Grad_H) %Interpolate at H direction in HA 
    G_flat_out =  (block_in(7,6) + block_in(7,8))/2 + (2*block_in(7,7)-block_in(7,5)-block_in(7,9))/4;
elseif(Dir == Grad_V) %Interpolate at V direction in HA  
    G_flat_out =  (block_in(6,7) + block_in(8,7))/2 + (2*block_in(7,7)-block_in(5,7)-block_in(9,7))/4;
else %Interpolate at Diag direction in HA  
    G_flat_out =  (block_in(7,6) + block_in(7,8) + block_in(6,7) + block_in(8,7))/4 + (4*block_in(7,7)-block_in(7,5)-block_in(7,9) - block_in(5,7)-block_in(9,7))/8;
end
end

function [G_refined] = UVC_G_refine(block_in,pad_in)
weight_n = abs(block_in(1,3) - block_in(3,3)) + abs(block_in(2,3) - block_in(4,3)); weight_n = 1./(1+weight_n);
weight_s = abs(block_in(5,3) - block_in(3,3)) + abs(block_in(4,3) - block_in(2,3)); weight_s = 1./(1+weight_s);
weight_w = abs(block_in(3,1) - block_in(3,3)) + abs(block_in(3,2) - block_in(3,4)); weight_w = 1./(1+weight_w);
weight_e = abs(block_in(3,5) - block_in(3,3)) + abs(block_in(3,4) - block_in(3,2)); weight_e = 1./(1+weight_e); 

Color_Diff_N = block_in(2,3) - pad_in(1,2);
Color_Diff_S = block_in(4,3) - pad_in(3,2);
Color_Diff_W = block_in(3,2) - pad_in(2,1);
Color_Diff_E = block_in(3,4) - pad_in(2,3);

CDF = weight_n.*Color_Diff_N + weight_s.*Color_Diff_S + weight_w.*Color_Diff_W + weight_e.*Color_Diff_E;
CDF = CDF./(weight_n + weight_s + weight_w + weight_e);
G_refined = block_in(3,3) + CDF;
end