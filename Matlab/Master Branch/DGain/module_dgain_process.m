%% This is the Process of DGain,for more detailed information
%% The position of the Dgain Module is always after the BLC process in the ISP pipeline
%% when dgain_eb = 1, enables the Gain Correction Process,vice versa.

function [dgain_out] = module_dgain_process(blc_out,module_dgain_param,top_paramset,dgain_eb)

if(dgain_eb)
    
    [Img_Height,Img_Width] = size(blc_out);  
    
    if(~top_paramset.Auto)
        rDain = module_dgain_param.rDain;
        gDain = module_dgain_param.gDain;
        bDain = module_dgain_param.bDain;
    else
        rDain = 1;
        gDain = 1;
        bDain = 1;
    end
    
    if(strcmp(top_paramset.Bayer, 'rggb'))
        dgain_out(1:2:Img_Height - 1,1:2:Img_Width - 1) = blc_out(1:2:Img_Height - 1,1:2:Img_Width - 1).* rDain;
        dgain_out(2:2:Img_Height ,2:2:Img_Width) = blc_out(2:2:Img_Height,2:2:Img_Width).* bDain;
        dgain_out(1:2:Img_Height - 1 ,2:2:Img_Width) = blc_out(1:2:Img_Height - 1 ,2:2:Img_Width).* gDain;
        dgain_out(2:2:Img_Height,1:2:Img_Width - 1) = blc_out(2:2:Img_Height,1:2:Img_Width - 1).* gDain;
    elseif(strcmp(top_paramset.Bayer, 'grbg'))
        dgain_out(1:2:Img_Height - 1,1:2:Img_Width - 1) = blc_out(1:2:Img_Height - 1,1:2:Img_Width - 1).* gDain;
        dgain_out(2:2:Img_Height ,2:2:Img_Width) = blc_out(2:2:Img_Height,2:2:Img_Width).* gDain;
        dgain_out(1:2:Img_Height - 1 ,2:2:Img_Width) = blc_out(1:2:Img_Height - 1 ,2:2:Img_Width).* rDain;
        dgain_out(2:2:Img_Height,1:2:Img_Width - 1) = blc_out(2:2:Img_Height,1:2:Img_Width - 1).* bDain;
    elseif(strcmp(top_paramset.Bayer, 'gbrg'))
        dgain_out(1:2:Img_Height - 1,1:2:Img_Width - 1) = blc_out(1:2:Img_Height - 1,1:2:Img_Width - 1).* gDain;
        dgain_out(2:2:Img_Height ,2:2:Img_Width) = blc_out(2:2:Img_Height,2:2:Img_Width).* gDain;
        dgain_out(1:2:Img_Height - 1 ,2:2:Img_Width) = blc_out(1:2:Img_Height - 1 ,2:2:Img_Width).* bDain;
        dgain_out(2:2:Img_Height,1:2:Img_Width - 1) = blc_out(2:2:Img_Height,1:2:Img_Width - 1).* rDain;
    else %pattern is bggr
        dgain_out(1:2:Img_Height - 1,1:2:Img_Width - 1) = blc_out(1:2:Img_Height - 1,1:2:Img_Width - 1).* bDain;
        dgain_out(2:2:Img_Height ,2:2:Img_Width) = blc_out(2:2:Img_Height,2:2:Img_Width).* rDain;
        dgain_out(1:2:Img_Height - 1 ,2:2:Img_Width) = blc_out(1:2:Img_Height - 1 ,2:2:Img_Width).* gDain;
        dgain_out(2:2:Img_Height,1:2:Img_Width - 1) = blc_out(2:2:Img_Height,1:2:Img_Width - 1).* gDain;
    end   
    dgain_out = Matrix_Clip(dgain_out,top_paramset.BitDepth);
    
else
    dgain_out = blc_out;
end

end