%% This is the single test file of the module Dgain,use the test case in the directory to test the Dgain process.
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_dgain_param = struct('rDain',1,'bDain',1,'gDain',1);
dgain_eb = 1;
addpath(genpath('../../'));    

%% Read The Test Image In,always the data being processed after BLC
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
blc_out = fread(fileinptr,[top_paramset.width,top_paramset.height],'uint8');
blc_out = blc_out';
fclose(fileinptr);

%% Main Dgain Process
dgain_out = module_dgain_process(blc_out,module_dgain_param,top_paramset,dgain_eb);

%% Disp Result
figure(1),imshow(uint8(blc_out));
figure(2),imshow(uint8(dgain_out));
imwrite(uint8(dgain_out),'./Test Case/Case/dgain_test_out.png');


%% Store the Result in the Raw Foramt
path = './Test Case/Case/';
file = 'Dgain_test_out.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,dgain_out','uint8');
fclose(fileoutptr);

