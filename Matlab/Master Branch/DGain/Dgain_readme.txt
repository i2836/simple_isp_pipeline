This is the explantion of Dgain (Digtial Gain Correction) module, which is always occuring after the BLC processes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation：                                                                                                                                                                       %
%                                                                                                                                                                                                            %
%             1.The Dynamic process is aimed at compensating the Sensor Gain of Input light , there exist one case                               %         
%                                                                                                                                                                                                            %
%  File Explanation:                                                                                                                                                                                %
%                                                                                                                                                                                                            %
%             1.module_dgain_process.m: main process of dgain , input the BLC processed out and output the Dgain processed out    %
%             2.dgain_test.m: test process of single dgain module, can be runned seperately beyond isp_top.m                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
       
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  param Explanation：                                                                                                                                                                                                 %
%                                                                                                                                                                                                                                    %
%             1. dgain_eb : 1 —  Open the (Digital Gain Correction) process  to compensate the Photoelectric conversion correspondence   in sensor  %            
%                              0 — Close the  (Digital Gain Correction) process and output the input data                                                                                %            
%             2. module_dgain_param.r/g/bgain:  Separate Gain Value for R/G/B Positions of Raw Format input                                                            %
%                                                                                                                                                                                                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%