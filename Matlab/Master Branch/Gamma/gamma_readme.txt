This is the explantion of Gamma (Gamma Correction) module, which is always occuring after the SA processes.
The Gamma Correction adopted here are two modes: 
            mode 0：refers to the paper —— "An Image Enhancement Method Based on Gamma Correction" (AEMGC)
            mode 1:  refers to the paper —— “ Efficient Contrast Enhancement Using Adaptive Gamma Correction With Weighting Distribution”(AGCWD)
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                                        %
%                                                                                                                                                                                                                                           %
%             1.The Gamma process is aimed at compensating the display response function of the digtial screen, there exist only one test case                  %                                                                                                                                                                                   
%                                                                                                                                                                                                                                           %                                                                                                                                                                                                                                
%  File Explanation:                                                                                                                                                                                                                %
%                                                                                                                                                                                                                                           %
%             1. module_gamma_process.m: main process of sa, input LTM processed out and output the  sa corrected  processed out                               %
%                                                                                                                                                                                                                                           % 
%             2. gamma_test.m: test process of single gamma module, can be runned seperately beyond isp_top.m                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                             %
%                                                                                                                                                                                                                                                %
%             1. gamma_eb : 1 —— Open the gamma process , 0 ——Close the gamma process and directly output the input data                                          %        
%                                                                                                                                                                                                                                                %                                                                                                          
%             2. module_gamma_param.mode:  switch the mode of adopted gamma methods, 0/1 seperately related to  (AEMGC/ AGCWD)                           %    
%             3. module_gamma_param.k:  set the linear coefficient of  methods: AEMGC                                                                                                             %
%             4. module_gamma_param.m: set the linear coefficient of methods: AGCWD                                                                                                           %                                                                                                                                                                                                                                                                  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%