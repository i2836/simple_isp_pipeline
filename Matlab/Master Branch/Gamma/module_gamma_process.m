%% This is the Process of Gamma (Gamma Correction),for more detailed information,please look at the Gamma_readme.txt and testcase_readme.txt
%% The position of the Gamma module is always after by the SA process in the ISP pipeline 
%% when gamma_eb = 1, enables the gamma process ,vice versa.
      %%  There adopt two methods to the gamma process which can be switched by the parameter:mode
         %%  mode:  0 related to the passage:"An Image Enhancement Method Based on Gamma Correction" (AEMGC)
         %%  mode   1 related to the passage:  ˇ° Efficient Contrast Enhancement Using Adaptive Gamma Correction With Weighting Distributionˇ±(AGCWD) 
         %%  k:  set the linear coefficient of  methods: AEMGC ,ranges from [1-5],related to the r = [0.2, 1];
         %%  s:  set the linear coefficient of the method: AGCWD,ranges from [1-5],related to the r = [0.2, 1];
                 
function [gamma_out] = module_gamma_process(sa_out,module_gamma_param,top_paramset,gamma_eb)
if(gamma_eb == 1) %% enables the sa process
    if(top_paramset.Auto == 1)
        mode = 0;
        k = 0.4;
        s = 0.6;
    else
        mode = module_gamma_param.mode;
        k = module_gamma_param.k;
        s = module_gamma_param.s;
    end
    
    switch mode
        case 0
            gamma_out = Matrix_Clip(Gamma_AEMGC(double(sa_out),k,top_paramset.BitDepth),top_paramset.BitDepth);
        case 1
            gamma_out = Matrix_Clip(Gamma_AGCWD(double(sa_out),s,top_paramset.BitDepth),top_paramset.BitDepth);
        otherwise
            gamma_out = Matrix_Clip(Gamma_AEMGC(double(sa_out),k,top_paramset.BitDepth),top_paramset.BitDepth);
    end
else
    gamma_out = sa_out;
end
end

function [out] = Gamma_AEMGC(sa_out,k,BitDepth)
[he_in,wd_in,~] = size(sa_out);
img_grey = double(rgb2gray(uint8(sa_out)));
img_in_hsv = rgb2hsv(sa_out);
V = img_in_hsv(:,:,3);

range = bitshift(1,BitDepth);
Gamma_Lut = zeros(1,range);

for i = 1:range
    Gamma_Lut(i) = 1./(1+k.*cos(pi.*i/256));
end

for i = 1:he_in
    for j = 1:wd_in
        V_out(i,j) = (range - 1).* nthroot((V(i,j)./(range - 1)).^ceil(100*(Gamma_Lut(ceil(img_grey(i,j))+1))+1),100);
    end
end

out = hsv2rgb(cat(3,img_in_hsv(:,:,1),img_in_hsv(:,:,2),V_out));
end

function [out] = Gamma_AGCWD(sa_out,s,BitDepth)
[he_in,wd_in,~] = size(sa_out);
img_in_hsv = rgb2hsv(sa_out);
Vmax = max(max(img_in_hsv(:,:,3)));

V = img_in_hsv(:,:,3);
pdf = zeros(1,bitshift(1,BitDepth));
cdf = zeros(1,bitshift(1,BitDepth));

%% statistic and normalize the pdf 
for i = 1:he_in
    for j = 1:wd_in
        pdf(min(ceil(V(i,j)),bitshift(1,BitDepth))) = pdf(min(ceil(V(i,j)),bitshift(1,BitDepth))) + 1;
    end   
end

pdf = pdf./(he_in*wd_in);
pdf_max = max(pdf);pdf_min = min(pdf);

for i = 1:bitshift(1,BitDepth)
    pdf(i) = pdf_max.*nthroot(((pdf(i) - pdf_min)./max((pdf_max - pdf_min),1)).^s,5);
end
pdf = pdf./(sum(pdf));

%% statistic the cdf and set the relaeted Lut of cdf value
for i = 1:bitshift(1,BitDepth)
    if( i == 1)
        cdf(i) = pdf(i);
    else
        cdf(i) = cdf(i - 1) + pdf(i);
    end
end

for i = 1:bitshift(1,BitDepth)
    cdf(i)  = round(10000* cdf(i))/10000; 
end

Original_Lut = (1 - cdf)*100;

%%Gamma Correction according to the cdf
for i = 1:he_in
    for j = 1:wd_in
       V(i,j) = Vmax.*nthroot((V(i,j)./Vmax).^(ceil(Original_Lut(ceil(V(i,j)))+1)),100);
    end   
end

V = max(min(bitshift(1,BitDepth)-1,V),0);
out = hsv2rgb(cat(3,img_in_hsv(:,:,1),img_in_hsv(:,:,2),V));
end
