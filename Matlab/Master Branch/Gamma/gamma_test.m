%% This is the single test file of the module Gamma Correction ,use the test case in the directory to test the Gamma process.
%% The Position of Gamma module in the ISP pipeline,is always after the SA Process in the RGB domain
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% There adopt two methods to the gamma process which can be switched by the parameter:mode
         %%  mode:  0 related to the passage:"An Image Enhancement Method Based on Gamma Correction" (AEMGC)
         %%  mode   1 related to the passage:  ˇ° Efficient Contrast Enhancement Using Adaptive Gamma Correction With Weighting Distributionˇ±(AGCWD) 
         %%  k:  set the linear coefficient of  methods: AEMGC ,ranges from [1-5],related to the r = [0.2, 1];
         %%  s:  set the linear coefficient of the method: AGCWD
                 
         
%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                              
module_gamma_param = struct('mode',1,'k',0.8,'s',1);%% two gamma methods:AEGMC/AGCWD, k and s are seperately the coeff of two methods
gamma_eb = 1;     %% enables the Saturation Adjustment process
addpath(genpath('../../'));
  
%% Read The Test Image In,always the data being processed after SA processed module
sa_out = imread('./Test Case/Case Two/sa_test_out_low_contrast.png');

%% Main Process of Gamma
gamma_out = module_gamma_process(sa_out,module_gamma_param,top_paramset,gamma_eb);

%% Disp Result in png format
figure(1),imshow(uint8(sa_out));
figure(2),imshow(uint8(gamma_out));
imwrite(uint8(gamma_out),'./Test Case/Case Two/gamma_test_out_mode1.png');