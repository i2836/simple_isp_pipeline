%% This is the Process of BLC,for more detailed information,please look at the BLC_readme.txt and testcase_readme.txt
%% The position of BLC is always behinds the FPN in the still ISP pipelines
%% when blc_eb = 1, enables the Black-Level-Correction ,vice versa.

function [blc_out] = module_blc_process(fpn_out,module_blc_param,top_paramset,blc_eb)
if(blc_eb)
    if(top_paramset.Auto == 1)
        module_blc_param.blc_value = 0;
    end
    blc_out = Matrix_Clip((fpn_out - module_blc_param.blc_value),top_paramset.BitDepth);
else
    blc_out = fpn_out;    
end
end