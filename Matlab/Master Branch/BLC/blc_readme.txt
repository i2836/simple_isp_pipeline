This is the explantion of BLC (Black-Level-Correction) module, which is always occuring after the FPN processes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation：                                                                                                                                                                       %
%                                                                                                                                                                                                            %
%             1.The BLC process is aimed at eliminating the Black Current in the dark environments, there exist one case                        %         
%                                                                                                                                                                                                            %
%  File Explanation:                                                                                                                                                                                %
%                                                                                                                                                                                                            %
%             1.module_blc_process.m: main process of BLC , input the FPN processed images and output the BLC processed out         %
%             2.blc_test.m: test process of single blc module, can be runned seperately beyond isp_top.m                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
       
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  param Explanation：                                                                                                                                                                                                 %
%                                                                                                                                                                                                                                    %
%             1. blc_eb : 1 —  Open the Black-Level-Correction process  to eliminate the dark current caused by the sensors                               	       %            
%                              0 — Close the Black-Level-Correction process and output the input data                                                                                    %            
%             2. module_blc_param.blc_value:  Value of Dark Current，can be statistic by the specific rows  in the reference imgs or from the auto       %
%                                                                 statistic process embbed in the camera paramter set                                                                                 %
%                                                                                                                                                                                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

