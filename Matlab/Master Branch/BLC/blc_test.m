%% This is the single test file of the module BLC,use the test case in the directory to test the blc process.
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_blc_param = struct('blc_value',10);
blc_eb = 1;
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after FPN
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
fpn_out = fread(fileinptr,[top_paramset.width, top_paramset.height],'uint8');
fpn_out = fpn_out';
fclose(fileinptr);

%% Main BLC Process
blc_out = module_blc_process(fpn_out,module_blc_param,top_paramset,blc_eb);

%% Disp Result in png format
figure(1),imshow(uint8(fpn_out));
figure(2),imshow(uint8(blc_out));
imwrite(uint8(blc_out),'./Test Case/Case/BLC_test_out.png');

%% Store the Result in the Raw Foramt
path = './Test Case/Case/';
file = 'BLC_test_out.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,blc_out','uint8');
fclose(fileoutptr);