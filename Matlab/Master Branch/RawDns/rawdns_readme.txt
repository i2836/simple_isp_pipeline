This is the explantion of RawDns  (Raw Domain Denoise) module, which is always occuring after the GB (Green Balance) processes.
The Method adopts here  is the ABF filter that from the reference paper: "Adaptive Bilateral Filter for Sharpness Enhancement and Noise Removal"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                     %
%                                                                                                                                                                                                                        %
%             1.The RawDns process is aimed at eliminating the Noise in the  Raw Domain image , there exist one test case                                %         
%                                                                                                                                                                                                                        %
%  File Explanation:                                                                                                                                                                                            %
%                                                                                                                                                                                                                        %
%             1.module_rawdns_process.m: main process of rawdns, input the GB processed out and output the rawdns processed out            %
%             2.rawdns_test.m: test process of single rawdns module, can be runned seperately beyond isp_top.m                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                       %
%                                                                                                                                                                                                                                          %
%             1. rawdns_eb : 1 —— Open the rawdns process , 0 ——Close the rawdn process and directly output the input data                                       %        
%                                                                                                                                                                                                                                          %                                                                                                           %
%             2. module_rawdns_param.LOG_bsize:  the Size of LoG search block                                                                                                                    % 
%             3. module_rawdns_param.Filter_bsize:  the Size of ABF cacualnate block                                                                                                            %
%             4. module_rawdns_param.Lmax:  Threhold of the LoG result                                                                                                                               %
%             5. module_rawdns_param.Sigma_LOG:  Sigma Value adopted in the LOG process                                                                                             %                                                                                                                                                                        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%