%% This is the Process of RawDns(Raw Domain Denoise),for more detailed information,please look at the RawDns_readme.txt and testcase_readme.txt
%% The position of the RawDns module is always after by the GB process in the ISP pipeline 
%% when rawdns_eb = 1, enables the RawDns process ,vice versa.
%% For the method of RawDns algorithm adopted here,please refers to the passage ���� "Adaptive Bilateral Filter for Sharpness Enhancement and Noise Removal"
%% Compared to the tradtional RawDns module which could caused the smooth in the edge area, this passage enables the modified BF filter to remove the noise while sharpens the edge at the same time, the paramter sigma_r and moment factor is locally set according to the LOG result. 

function [rawdns_out] = module_rawdns_process(gb_out,module_rawdns_param,top_paramset,rawdns_eb)
if(rawdns_eb == 1)
    %% Initilize the param set and under-processed data
    if(top_paramset.Auto == 1)
        LOG_bsize = 9;
        Filter_bsize = 7;
        Lmax = 60;
        Sigma_LOG = 1.5;
    else
        LOG_bsize = module_rawdns_param.LOG_bsize;
        Filter_bsize = module_rawdns_param.Filter_bsize;
        Lmax = module_rawdns_param.Lmax;
        Sigma_LOG = module_rawdns_param.Sigma_LOG;
    end
    
    %% Padding the input img_data according to the width of preset LOG kernel width
    [Img_Height,Img_Width] = size(gb_out); 
    pad_width = (LOG_bsize+1)/2;
    gb_pad_out = padding_array(gb_out,pad_width,pad_width);  

%    %% prerpare the space for the full-intp picture data of the R/Gr/Gb/B bayer components
%     R_full_Intp = zeros(Img_Height+2*pad_width -2,Img_Width+2*pad_width -2);
%     Gr_full_Intp = zeroszeros(Img_Height+2*pad_width -2,Img_Width+2*pad_width -2);
%     Gb_full_Intp = zeroszeros(Img_Height+2*pad_width -2,Img_Width+2*pad_width -2);
%     B_full_Intp = zeros(Img_Height+2*pad_width -2,Img_Width+2*pad_width -2);


    temp_full_Intp = zeros(Img_Height+2*pad_width -2,Img_Width+2*pad_width -2,4);
    %% sepeate the H*W Bayer pattern raw Img Data into 4 sub data maps: each only has (H/2)*(W/2) datas  
    temp_full_Intp(1:2:Img_Height+2*pad_width -3,1:2:Img_Width+2*pad_width -3,1) = gb_pad_out(2:2:Img_Height+2*pad_width -2,2:2:Img_Width+2*pad_width -2);%%top-left Bayer type
    temp_full_Intp(1:2:Img_Height+2*pad_width -3,1:2:Img_Width+2*pad_width -3,2) = gb_pad_out(2:2:Img_Height+2*pad_width -2,3:2:Img_Width+2*pad_width - 1); %%top-right Bayer type 
    temp_full_Intp(1:2:Img_Height+2*pad_width -3,1:2:Img_Width+2*pad_width -3,3) = gb_pad_out(3:2:Img_Height+2*pad_width -1,2:2:Img_Width+2*pad_width -2); %%down-left Bayer type 
    temp_full_Intp(1:2:Img_Height+2*pad_width -3,1:2:Img_Width+2*pad_width -3,4) = gb_pad_out(3:2:Img_Height+2*pad_width -1,3:2:Img_Width+2*pad_width - 1); %%down-right Bayer type 
    
    %%Bilinear the 4 sub temp_full_Intp maps
    for i = 1:4
       temp_full_Intp(:,:,i) = Bilinear(temp_full_Intp(:,:,i));
    end
    
%    temp_full_Intp = zeros(1:Img_Height+2*pad_width -2,1:Img_Width+2*pad_width -2,4);
%    %% Use the HA Intp to get the full-resolution 4 sub maps ,i/j related to the top-left offset between the related subblock and gb_pad_out 
%     for i = 0:1
%         for j = 0:1
%             temp_full_Intp(:,:,i*2+j+1) = HA_Intp(gb_pad_out,i,j);
%         end
%     end
    
    %% main Adaptive Bifilter Process for 4 kinds of Bayer Patterns
    for i = 1:Img_Height
        for j = 1:Img_Width
            if(mod(i,2) && mod(j,2)) %Top-left position
                 rawdns_out(i,j) = ABF_Process(temp_full_Intp(i:i+LOG_bsize-1,j:j+LOG_bsize-1,1),Filter_bsize,Lmax,Sigma_LOG);
            elseif(mod(i+1,2) && mod(j+1,2))  %Down-Right position
                 rawdns_out(i,j) = ABF_Process(temp_full_Intp(i:i+LOG_bsize-1,j:j+LOG_bsize-1,4),Filter_bsize,Lmax,Sigma_LOG);
            elseif(mod(i+1,2) && mod(j,2)) %Down-Left position
                 rawdns_out(i,j) = ABF_Process(temp_full_Intp(i:i+LOG_bsize-1,j:j+LOG_bsize-1,3),Filter_bsize,Lmax,Sigma_LOG);
            else   %Top-Right position
                 rawdns_out(i,j) = ABF_Process(temp_full_Intp(i:i+LOG_bsize-1,j:j+LOG_bsize-1,2),Filter_bsize,Lmax,Sigma_LOG);
            end 
        end
    end
    rawdns_out = Matrix_Clip(rawdns_out,top_paramset.BitDepth);       
else
    rawdns_out = gb_out;
end
end

%% Simplest Bilinear Intp
function [full_map_out] = Bilinear(full_map_in)
[he_in,wd_in] = size(full_map_in);
full_map_out = full_map_in;
full_map_out(1:2:he_in-1,2:2:wd_in-2) = (full_map_in(1:2:he_in-1,1:2:wd_in-3) +  full_map_in(1:2:he_in-1,3:2:wd_in-1))/2;
full_map_out(2:2:he_in-2,1:2:wd_in-1) = (full_map_in(1:2:he_in-3,1:2:wd_in-1) +  full_map_in(3:2:he_in-1,1:2:wd_in-1))/2;
full_map_out(2:2:he_in-2,2:2:wd_in-2) = (full_map_in(1:2:he_in-3,1:2:wd_in-3) +  full_map_in(1:2:he_in-3,3:2:wd_in-1) + full_map_in(3:2:he_in-1,1:2:wd_in-3) + full_map_in(3:2:he_in-1,3:2:wd_in-1))/4;
full_map_out(he_in,:) = full_map_out(he_in - 1,:);
full_map_out(:,wd_in) = full_map_out(:,wd_in-1); 
end

%% The ABF Filtering process
function [out] = ABF_Process(block_in,Filter_bsize,Lmax,Sigma_LOG)
[he_in,wd_in] = size(block_in);
half_width = (he_in - 1)/2;

Div = he_in * wd_in * (- pi * (Sigma_LOG.^4));
C = 0;
%%cal the LOG result
for i =-half_width :half_width 
   for j = -half_width :half_width 
       C = C + (1 - (i.^2+j.^2)/(2*(Sigma_LOG.^2))) .* exp(-(i.^2+j.^2)/(2*(Sigma_LOG.^2)));
   end
end
C = C./Div;
fprintf('C = %f\n',C);

for i =-half_width :half_width 
    for j = -half_width :half_width 
           LoG_Operator(i+half_width+1,j+half_width+1) = (1 - (i.^2+j.^2)/(2*(Sigma_LOG.^2))).* exp(-(i.^2+j.^2)/(2*(Sigma_LOG.^2))) + C;
    end
end

LoG_Result = round(sum(sum(LoG_Operator.*block_in)));
LoG_Result = min(max(LoG_Result,-Lmax),Lmax);

%%Lut related to the LOG result
Optimal_offset(1:61) = (-60:0)/2; Optimal_offset(62:121)=(1:60)/3;
sigma_r(1:30) = 5; sigma_r(31:40) = (5:1:14);sigma_r(41:50) = 15;
sigma_r(51:58) = 13;sigma_r(59:61) = 7;sigma_r(62:71) = 11;
sigma_r(72:81) = 13;sigma_r(92:121) = 5; sigma_r(82:91) = (14:-1:5);
 
%%Pre-Define the kernel: Domain kernel and modified range kernel
G_Kernel = fspecial('gaussian',Filter_bsize,1);
Range_Kernel_Width = (Filter_bsize - 1)/2;

for i = -Range_Kernel_Width:Range_Kernel_Width
    for j = -Range_Kernel_Width:Range_Kernel_Width
          R_kernel(i+Range_Kernel_Width+1,j+Range_Kernel_Width+1) = exp(-((block_in(1+half_width+i,1+half_width+j) - block_in(1+half_width,1+half_width) - Optimal_offset(LoG_Result + Lmax + 1))^2)/(2.*(sigma_r(LoG_Result + Lmax + 1)^2)));
%         R_kernel(i+Range_Kernel_Width+1,j+Range_Kernel_Width+1) = exp(-((block_in(1+half_width+i,1+half_width+j) - block_in(1+half_width,1+half_width))^2)/(2.*(5^2)));
    end
end

% ABF_Kernel = G_Kernel;
ABF_Kernel = (G_Kernel.* R_kernel)./(sum(sum(G_Kernel.* R_kernel)));
out = sum(sum(block_in(1+half_width - Range_Kernel_Width : 1+half_width + Range_Kernel_Width,1+half_width - Range_Kernel_Width : 1+half_width + Range_Kernel_Width).* ABF_Kernel));
end