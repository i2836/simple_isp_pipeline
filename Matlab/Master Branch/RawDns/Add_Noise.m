top_paramset.width = 1920; top_paramset.height = 1080;
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
clean_out = fread(fileinptr,[top_paramset.width, top_paramset.height],'uint8');
clean_out = clean_out';
fclose(fileinptr);

noise_out = Matrix_Clip(clean_out + 10 * randn(top_paramset.height,top_paramset.width),8);

path = './Test Case/Case Two/';
file = 'added_10%_noise.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,noise_out','uint8');
fclose(fileoutptr);
