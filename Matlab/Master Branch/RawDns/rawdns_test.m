%% This is the single test file of the module RawDns (Raw domain Denoise),use the test case in the directory to test the RawDns process.
%% The Position of RawDns module in the ISP pipeline,is always after the GB Process
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% For the method of RawDns algorithm adopted here,please refers to the passage ���� "Adaptive Bilateral Filter for Sharpness Enhancement and Noise Removal"
%% Compared to the tradtional RawDns module which could caused the smooth in the edge area, this passage enables the modified BF filter to remove the noise while sharpens the edge at the same time, the paramter sigma_r and moment factor is locally set according to the LOG result. 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_rawdns_param = struct('LOG_bsize',9,'Filter_bsize',7,'Lmax',60,'Sigma_LOG',1.5); %% LOG_size and Filter_size are seperately the width of LOG kernel and ABF Filter, Lmax is the Max abs thre of LOG result, Sigma_LOG is the sigma value of LOG operators
rawdns_eb = 1;     %% enables the RawDns process
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after GB
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
gb_out = fread(fileinptr,[top_paramset.width, top_paramset.height],'uint8');
gb_out = gb_out';
fclose(fileinptr);

%% Main Process of RawDns: the ABF filter
rawdns_out = module_rawdns_process(gb_out,module_rawdns_param,top_paramset,rawdns_eb);

%% Disp Result in png format
figure(1),imshow(uint8(gb_out));
figure(2),imshow(uint8(rawdns_out));
imwrite(uint8(rawdns_out),'./Test Case/Case Two/RAWDNS_test_out.png');

%% Store the Result in the Raw Foramt
path = './Test Case/Case Two/';
file = 'rawdns_test_out.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,rawdns_out','uint8');
fclose(fileoutptr);