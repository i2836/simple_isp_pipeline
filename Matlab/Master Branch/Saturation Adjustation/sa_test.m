%% This is the single test file of the module SA (Saturation Adjustment),use the test case in the directory to test the SA process.
%% The Position of SA module in the ISP pipeline,is always after the LTM Process in the RGB domain
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% There just adopts only one method to the SA process����just the linear stretching coeff of s components inthe HSV color space and finally inverse
         %% parameter coeff:  set the linear coeff of adjusted S, default is 1.5
       
       
%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                              
module_sa_param = struct('coeff',4); %% set the linear coeff of adjusted S components
sa_eb = 1;     %% enables the Saturation Adjustment process
addpath(genpath('../../'));
  
%% Read The Test Image In,always the data being processed after LTM processed module
ltm_out = imread('./Test Case/Case one/ltm_test_out.png');
%% Main Process of SA
sa_out = module_sa_process(ltm_out,module_sa_param,top_paramset,sa_eb);

%% Disp Result in png format
figure(1),imshow(uint8(ltm_out));
figure(2),imshow(uint8(sa_out));
imwrite(uint8(sa_out),'./Test Case/Case One/sa_test_out.png');