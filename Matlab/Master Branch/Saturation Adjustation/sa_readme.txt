This is the explantion of SA (Saturation Adjustment) module, which is always occuring after the LTM processes.
The SA adopts here is the simplest saturation adjustment in the HSV Color Space where only adjusts the S and remains the H and V components.
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                      %
%                                                                                                                                                                                                                         %
%             1.The SA process is aimed at adjusting the saturation in the HSV color space linearly, there exist one test case                                %                                                                                                                                                                                   %
%                                                                                                                                                                                                                         %                                                                                                                                                                                                                                 %
%  File Explanation:                                                                                                                                                                                             %
%                                                                                                                                                                                                                         %
%             1. module_sa_process.m: main process of sa, input LTM processed out and output the  sa corrected  processed out                      %
%                                                                                                                                                                                                                         %
%             2. ltm_sa.m: test process of single sa module, can be runned seperately beyond isp_top.m                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                             %
%                                                                                                                                                                                                                                                %
%             1. sa_eb : 1 —— Open the sa process , 0 ——Close the sa process and directly output the input data                                                                     %        
%                                                                                                                                                                                                                                                %                                                                                                          
%             2. module_sa_param.k:  set the linear coefficient of adjusted S components                                                                                                             %                                                                                                      %                                                                                                                                                                   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%