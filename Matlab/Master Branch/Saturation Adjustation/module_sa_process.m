%% This is the Process of SA (Saturation Adjustment),for more detailed information,please look at the SA_readme.txt and testcase_readme.txt
%% The position of the SA module is always after by the LTM process in the ISP pipeline 
%% when sa_eb = 1, enables the sa process ,vice versa.
        %% parameter coeff:  set the linear coeff of adjusted S, default is 1.5 
function [sa_out] = module_sa_process(ltm_out,module_sa_param,top_paramset,sa_eb)
if(sa_eb == 1) %% enables the sa process
    if(top_paramset.Auto == 1)
        coeff = 1.5;
    else
        coeff = module_sa_param.coeff;
    end
    sa_out = Matrix_Clip(Satuation_Adjustment(double(ltm_out),coeff),top_paramset.BitDepth);   
else
    sa_out = ltm_out;
end
end

function [out] = Satuation_Adjustment(img_in,coeff)
img_in_hsv = rgb2hsv(img_in);
img_in_hsv(:,:,2) =  max(0,min(img_in_hsv(:,:,2)*coeff,1));
out  = hsv2rgb(img_in_hsv);

% img_in_hsv_1 = Manual_RGB2HSV(img_in);
% img_in_hsv_1(:,:,2) =  max(0,min(img_in_hsv_1(:,:,2)*coeff,1));
% out = Manual_HSV2RGB(img_in_hsv_1);
end


function [out] = Manual_RGB2HSV(img_in_1)
[he_in,wd_in,~] = size(img_in_1);
img_in  = img_in_1./255;

for i = 1:he_in
    for j = 1:wd_in
        MAX = max(img_in(i,j,:)); MIN = min(img_in(i,j,:));
        if(MAX == MIN)
            h = 0;
        elseif((MAX == img_in(i,j,1)) && (img_in(i,j,2)>=img_in(i,j,3)))
            h = 60*(img_in(i,j,2) - img_in(i,j,3))./(MAX-MIN);
        elseif((MAX == img_in(i,j,1)) && (img_in(i,j,2)<img_in(i,j,3)))
            h = 60*(img_in(i,j,2) - img_in(i,j,3))./(MAX-MIN) + 360;
        elseif(MAX == img_in(i,j,2)) 
            h = 60*(img_in(i,j,3) - img_in(i,j,1))./(MAX-MIN) + 120;
        else
            h = 60*(img_in(i,j,1) - img_in(i,j,2))./(MAX-MIN) + 240;
        end
       
        if(MAX == 0)
            s = 0;
        else
            s = 1 - MIN./MAX;
        end
        v = MAX;
        out(i,j,:)= [h./360,s,v.*255];
    end
end
end


function [out] = Manual_HSV2RGB(img_in)
[he_in,wd_in,~] = size(img_in);
temp = zeros(1,3);
for i = 1:he_in
    for j = 1:wd_in
        H = img_in(i,j,1).*360;
        S = img_in(i,j,2);
        V = img_in(i,j,3)./255;
        C = V.*S; 
        X = C.*(1-abs((mod(H/60,2) -1)));
        m = V -C ; 
        
        if((H>=0) &&(H<60))
            temp = [C,X,0];
        elseif((H>=60) &&(H<120))
            temp = [X,C,0];
        elseif((H>=120) &&(H<180))
            temp = [0,C,X];
        elseif((H>=180) &&(H<240))
            temp = [0,X,C];   
        elseif((H>=240) &&(H<300))
            temp = [X,0,C];    
        else
            temp = [C,0,X];   
        end
        out(i,j,:) = [(temp(1)+m).*255,(temp(2)+m).*255,(temp(3)+m).*255];
    end
end
end