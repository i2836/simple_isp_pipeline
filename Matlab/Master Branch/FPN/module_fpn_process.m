%% This is the Process of 2 frames merges FPN,for more detailed information,please look at the FPN_readme.txt and testcase_readme.txt
%% when fpn_eb = 1, merges the 2 successive frames by the move-estimation result threhold, which is judges by the MAD of neighborhood blocks during block-matching process.
%% when fpn_eb = 1, just average merge the two frames to send out.


%% Version 1:with the move vector estimation, the  move vector calculation is through find the mininum vector voting sums of Relative position migration coordinate index during the neiborhood pathches 

function [fpn_out] = module_fpn_process(images_in,module_fpn_param,top_paramset,fpn_eb)
if(fpn_eb)     
    if(top_paramset.Auto == 1)
        module_fpn_param.Sbize = 7;
        module_fpn_param.Cbsize = 3;
        module_fpn_param.mov_thre = 50;
    end
    
    [Img_Height,Img_Width] = size(images_in(:,:,1));
    pad_col_num = (module_fpn_param.Sbize + 1)/2;
    pad_row_num = (module_fpn_param.Sbize + 1)/2;
    half_cb = (module_fpn_param.Cbsize - 1)/2;
    half_sb = (module_fpn_param.Sbize - 1)/2;
    Min_MAD = module_fpn_param.mov_thre;
    cmp_block = zeros(module_fpn_param.Cbsize,module_fpn_param.Cbsize);
    ref_block = zeros(module_fpn_param.Cbsize,module_fpn_param.Cbsize);
    search_block = zeros(module_fpn_param.Sbize,module_fpn_param.Sbize,top_paramset.frames-1);
    images_accum = zeros(Img_Height,Img_Width);
    


    Offset = zeros(module_fpn_param.Sbize - module_fpn_param.Cbsize + 1,module_fpn_param.Sbize - module_fpn_param.Cbsize + 1,top_paramset.frames-1);
    x_offset = 0;
    y_offset = 0;
    for i = 1:top_paramset.frames 
        fpn_img(:,:,i) = padding_array(double(images_in(:,:,i)),pad_col_num,pad_row_num);
    end
    
    for i = pad_row_num:pad_row_num+Img_Height-1
        for j = pad_col_num:pad_col_num+Img_Width-1
            ref_block = fpn_img(i:i + 2 * half_cb ,j:j + 2 * half_cb,top_paramset.frames); 
            acm_value = 0; acm_weight = 0;
            for k = 1:top_paramset.frames-1
                search_block(:,:,k) = fpn_img(i - pad_row_num + 1:i + pad_row_num - 1,j - pad_col_num + 1:j + pad_col_num - 1,k);
                              
                for m = half_cb+1:(2*pad_row_num - 1 - half_cb)
                    for n = half_cb+1:(2*pad_col_num - 1 - half_cb)
                        MAD = 0;
                        cmp_block = search_block(m - half_cb:m + half_cb,n - half_cb:n + half_cb,k);
                        MAD =sum(sum(abs(cmp_block - ref_block)));
                        
                        if((MAD < module_fpn_param.mov_thre))
                            if(MAD<Min_MAD)
                               Min_MAD = MAD;
                               x_offset = m + half_cb - pad_row_num - 2; % the x_offset that previous top_paramset.frames-1 hold compared to the current frame
                               y_offset = n + half_cb - pad_col_num - 2; % the y_offset that previous top_paramset.frames-1 hold compared to the current frame
                            end
                        end
                                                   
                    end
                end
%               fprintf('lastest Offest_x is %d,offset_y = %d\n',x_offset,y_offset);
                Offset(x_offset + (module_fpn_param.Sbize + 1)/2,y_offset + (module_fpn_param.Sbize + 1)/2,k) = Offset(x_offset + half_sb,y_offset + half_sb,k) + 1; % offset stores the [x_offset,y_offset,i] that previous frame move vector compared tol the cur frame               
            end
          
        end
    end

    %accum the previous k-1 frame as the move vector presents, final add the final frame to accumlate the sumvalue ,finally, average 
    for i = 1:top_paramset.frames-1
        [Row_Index,Col_Index] = find(Offset(:,:,i) == max(max(Offset(:,:,i))));
        Row_Index = Row_Index - (module_fpn_param.Sbize+1)/2; Col_Index = Col_Index - (module_fpn_param.Sbize+1)/2;
        images_accum = images_accum + fpn_img(1+pad_row_num+Row_Index:Img_Height+pad_row_num+Row_Index,1+pad_col_num+Col_Index:Img_Width+pad_col_num+Col_Index,i);  
%         fprintf('lastest Offest_x is %d,offset_y = %d\n',Col_Index,Row_Index);
    end
    fpn_out = (images_accum + images_in(:,:,top_paramset.frames))/top_paramset.frames;    
else
    for i = 1:top_paramset.frames
        images_accum = images_accum + images_in(:,:,i);
    end
    fpn_out = images_accum/top_paramset.frames;
end
end


%% Version 2:without the move estimation, the patch-matching strtegy is
%% similar to the Eurlioun Distance which the weight calculation is differ from the NLM-NSS idea

% function [fpn_out] = module_fpn_process(images_in,module_fpn_param,top_paramset,fpn_eb)
% [Img_Height,Img_Width,dim] = size(images_in);
% pad_col_num = (module_fpn_param.Sbize + 1)/2;
% pad_row_num = (module_fpn_param.Sbize + 1)/2;
% half_cb = (module_fpn_param.Cbsize - 1)/2;
% half_sb = (module_fpn_param.Sbize - 1)/2;
% 
% cmp_block = zeros(module_fpn_param.Cbsize,module_fpn_param.Cbsize);
% ref_block = zeros(module_fpn_param.Cbsize,module_fpn_param.Cbsize);
% search_block = zeros(module_fpn_param.Sbize,module_fpn_param.Sbize,top_paramset.frames-1);
% images_accum = zeros(Img_Height,Img_Width,dim);
% 
% if(fpn_eb)
%     for i = 1:top_paramset.frames 
%         fpn_img(:,:,i) = padding_array(double(images_in(:,:,i)),pad_col_num,pad_row_num);
%     end
%     
%     for i = pad_row_num:pad_row_num+Img_Height-1
%         for j = pad_col_num:pad_col_num+Img_Width-1
%             ref_block = fpn_img(i:i + 2 * half_cb ,j:j + 2 * half_cb,top_paramset.frames); 
%             acm_value = 0; acm_weight = 0;
%             for k = 1:top_paramset.frames-1
%                 search_block(:,:,k) = fpn_img(i - pad_row_num + 1:i + pad_row_num - 1,j - pad_col_num + 1:j + pad_col_num - 1,k);
%                               
%                 for m = half_cb+1:(2*pad_row_num - 1 - half_cb)
%                     for n = half_cb+1:(2*pad_col_num - 1 - half_cb)
%                         MAD = 0;
%                         cmp_block = search_block(m - half_cb:m + half_cb,n - half_cb:n + half_cb,k);
%                         MAD =sum(sum(abs(cmp_block - ref_block)));
%                         
%                         if((MAD < module_fpn_param.mov_thre))
%                             weight = 1./(MAD+1);
%                             acm_weight = acm_weight + weight;
%                             acm_value = acm_value + weight.*cmp_block(2,2);
%                        end                          
%                     end
%                 end
% 
%             end
%             
%             
%          if(acm_weight == 0)
%              fpn_out(i - pad_row_num + 1,j - pad_col_num + 1) =  fpn_img(i+1,j+1,top_paramset.frames);
%          else
%              fpn_out(i - pad_row_num + 1,j - pad_col_num + 1) =  Single_Value_Clip(acm_value./acm_weight,top_paramset.BitDepth);
%          end
%             
%         end
%     end 
% else
%     for i = 1:top_paramset.frames
%         images_accum = images_accum + images_in(:,:,i);
%     end
%     fpn_out = images_accum/top_paramset.frames;
% end