        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %  Module Explanation：                                                                                                                                                                       %
        %                                                                                                                                                                                                            %
        %             1.The FPN process is aimed at eliminating the Fixed Pattern Noise in the successive frames,there exists two cases              %
        %                according to the different Move estimation results of successive 2 frames                                                                        %         
        %                                                                                                                                                                                                            %
        %  File Explanation:                                                                                                                                                                                 %
        %                                                                                                                                                                                                            %
        %             1.module_fpn_process.m: main process of FPN, input the camera raw data and output the FPN processed data                %
        %             2.fpn_test.m: test process of single FPN module, can be runned seperately beyond isp_top.m                                            %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
       
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  param Explanation：                                                                                                                                                                                                 %
%           1. fpn_eb : 1 —  Open the Move Estimation between the 2 frames and average the offset_vector correcrted frames                                    %                                       
%                               0 — Close the Move Estimation and just average merge                                                                                                             %           
%           2. module_fpn_param.mov_thre: threhold of block-Matching , larges more blurred and more artifacts, lower  more noisy                          %
%                                                             Default is 50, recommond ranges from [30,80].                                                                                             %
%           3. module_fpn_param.Cbsize: compare block size in the Move Estimation,larger the more smooth and  lower noise                                    %
%                                                        but more calculation expands, only odd number enables. Default is 3, recommond ranges from [3,7]              %         
%           4. module_fpn_param.Sbize:  Search block size in the Move Estimation, larger the more smooth and lower noise,                                        %
%                                                        but more calculation expands, only odd number enables. Default is 7, recommond ranges from [7,11]            %
%                                                                                                                                                                                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
