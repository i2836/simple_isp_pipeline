%% This is the single test file of the module fpn,use the test case in the directory to test the FPN process.
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% For general, Consider the conditions that the numbers of merged frame is only 2.
%% For the Param set of the struct: module_fpn_param,please look at the param_readme.txt

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_fpn_param = struct('mov_thre',50,'Cbsize',3 ,'Sbize',7);
fpn_eb = 1;
addpath(genpath('../../'));

cache = zeros(top_paramset.width,top_paramset.height);
%% Read The Test Image In,always the multi-frame raw data from the caremas 
for i = 1:top_paramset.frames
     [file,path] = uigetfile('*.raw');
     fileptr(i) = fopen([path,file],'r');
     cache  = fread(fileptr(i),[top_paramset.width,top_paramset.height],'uint8');
     images_in(:,:,i) = cache';   
     fclose(fileptr(i));
end


%% Main FPN Process
fpn_out = module_fpn_process(images_in,module_fpn_param,top_paramset,fpn_eb);

%% Disp and Result
figure(1),imshow(uint8(fpn_out));
figure(2),imshow(uint8(images_in(:,:,1)));
figure(3),imshow(uint8(images_in(:,:,2)));

imwrite(uint8(fpn_out),'./Test Case/Case Two/fpn_out.png');

%% Store the Result in the Raw Foramt
path = './Test Case/Case Two/';
file = 'fpn_test_out.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,fpn_out','uint8');
fclose(fileoutptr);