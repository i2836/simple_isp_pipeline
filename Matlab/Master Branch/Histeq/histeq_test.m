%% This is the single test file of the module Histeq (Histgram Equalization process),use the test case in the directory to test the Histeq process.
%% The Position of Histeq module in the ISP pipeline,is always after the CCM Process
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% The parameter mode is used to switch the histeq method adopted in this module,where 
       %% mode 0: the simplest global histeq process
       %% mode 1: the simplest local  histeq process
       %% mdoe 2: the modified global histeq process, refers to paper:"Fast Image/Video Contrast Enhancement Based on Weighted Thresholded Histogram Equalization "

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                              
module_histeq_param = struct('mode',0,'Lbsize',15,'u',0.5,'v',0.5);
histeq_eb = 1;     %% enables the Histgram Equalization process
addpath(genpath('../../'));
  
%% Read The Test Image In,always the data being processed after CCM filtering module
ccm_out = imread('./Test Case/Case one/ccm_test_out_eye3.png');

%% Main Process of Histeq
histeq_out = module_histeq_process(ccm_out,module_histeq_param,top_paramset,histeq_eb);

%% Disp Result in png format
figure(1),imshow(uint8(ccm_out));
figure(2),imshow(uint8(histeq_out));
imwrite(uint8(histeq_out),'./Test Case/Case one/histeq_test_out_mode2.png');