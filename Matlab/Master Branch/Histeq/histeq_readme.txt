This is the explantion of Histeq (Histgoram Equaliztion) module, which is always occuring after the CCM processes.
The Histeq adopts here has the  three modes : 
          mode 0 ： the simplest Global Histeq based on the global PDF and CDF mapping function. 
          mode 1 ： the simplset Local  Histeq based on the local PDF and CDF mapping function. 
          mode 2 :    the modified Global Histeq based on the refer paper "Fast Image/Video Contrast Enhancement Based on Weighted  Thresholded Histogram Equalization "
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                      %
%                                                                                                                                                                                                                         %
%             1.The Histeq process is aimed at sterching the Hist distribution in the RGB domain , there exist  one test case                                 %
%                                                                                                                                                                                                                         %                                                                                                                                                                                                                                 %
%  File Explanation:                                                                                                                                                                                             %
%                                                                                                                                                                                                                         %
%             1. module_histeq_process.m: main process of histeq, input CCM processed out and output the  Histeq corrected  processed out  %
%                                                                                                                                                                                                                         %
%             2. histeq_test.m: test process of single histeq module, can be runned seperately beyond isp_top.m                                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                             %
%                                                                                                                                                                                                                                                %
%             1. histeq_eb : 1 —— Open the histeq process , 0 ——Close the histeq process and directly output the input data                                                  %        
%                                                                                                                                                                                                                                                %                                                                                                          
%             2. module_histeq_param.mode:  set the histeq methods adopted in the current module, 0/1/2seperately to the Global/Local/Modified Global    %                                                                                                                                                                    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%