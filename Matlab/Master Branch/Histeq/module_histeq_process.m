%% This is the Process of Histeq (Histgoram Equaliztion),for more detailed information,please look at the Histeq_readme.txt and testcase_readme.txt
%% The position of the Histeq module is always after by the CCM process in the ISP pipeline 
%% when histeq_eb = 1, enables the histeq process ,vice versa.
        %% Mode = 0: Global Histeq 
        %% Mode = 1: Local Histeq
        %% Mode = 2: Modified Global Histeq
        %% u and v are seperately the paramter of method:Modified Global Histeq
function [histeq_out] = module_histeq_process(ccm_out,module_histeq_param,top_paramset,histeq_eb)
if(histeq_eb == 1) %% enables the histeq process
    if(top_paramset.Auto == 1)
        mode = 0;
        Lbsize = 7;
        u = 0.5;
        v = 0.5;
    else
        mode = module_histeq_param.mode;
        Lbsize = module_histeq_param.Lbsize;
        u = module_histeq_param.u;
        v =module_histeq_param.v;
    end
    
    switch mode
        case 0
            histeq_out = Matrix_Clip(Global_Histeq(double(ccm_out),top_paramset.BitDepth),top_paramset.BitDepth);
        case 1
            histeq_out = Matrix_Clip(Local_Histeq(double(ccm_out),top_paramset.BitDepth,Lbsize),top_paramset.BitDepth);
        case 2
            histeq_out = Matrix_Clip(Modified_Global_Histeq(double(ccm_out),top_paramset.BitDepth,u,v),top_paramset.BitDepth);
        otherwise
            histeq_out = Matrix_Clip(Global_Histeq(double(ccm_out),top_paramset.BitDepth),top_paramset.BitDepth);
    end
else
    histeq_out = ccm_out;
end
end

%% the process of the global histeq equaliztion
function [histeq_out] = Global_Histeq(img_in,BitDepth)
[he_in,wd_in,dim] = size(img_in);
range = bitshift(1,BitDepth);
ndf = zeros(dim,range);
% pdf = zeros(dim,range);
total_num = he_in*wd_in;
acc_sum = zeros(dim);
cdf = zeros(dim,range);
Lut = zeros(dim,range);
R_max = max(max(img_in(:,:,1)));G_max = max(max(img_in(:,:,2)));B_max = max(max(img_in(:,:,3)));
R_min = min(min(img_in(:,:,1)));G_min = min(min(img_in(:,:,2)));B_min = min(min(img_in(:,:,3)));
Max_Lut = [R_max,G_max,B_max];Min_Lut = [R_min,G_min,B_min];

%% statistic the ndf(number distribution function )��pdf and cdf.
%% range from the 1-256 ,related to the pixel 8 bits range to 0-255.
for i = 1:he_in
    for j = 1:wd_in
        for k = 1:3
            ndf(k,ceil(img_in(i,j,k)) + 1) = ndf(k,ceil(img_in(i,j,k)) + 1) + 1;
        end
    end
end

for i = 1:dim
    for j = 1:range
        acc_sum(i) = acc_sum(i) + ndf(i,j);
        cdf(i,j) = acc_sum(i)./total_num;
    end
end
        
%% cal the mapping Lut of the global histeq
for i = 1:dim
    for j = 1:range
        Lut(i,j) = min(max((range*cdf(i,j)),Min_Lut(i)),Max_Lut(i));
    end
end

%% golbal histeq process
for i = 1:he_in
    for j = 1:wd_in
        for k = 1:3
            histeq_out(i,j,k) = Lut(k,ceil(img_in(i,j,k)) + 1);
        end
    end
end            
end


function [histeq_out] = Local_Histeq(img_in,BitDepth,width)
[he_in,wd_in,~] = size(img_in);
pad_width = (width - 1)/2;
img_pad_in = padding_array(img_in,pad_width,pad_width);

for i = 1:he_in
    for j = 1:wd_in
       cur_block = img_pad_in(i:i+2*pad_width,j:j+2*pad_width,:);
       out_block =  Global_Histeq(cur_block,BitDepth);
       histeq_out(i,j,:)  = out_block((width + 1)/2,(width + 1)/2,:);
    end
end
end

function [histeq_out] =Modified_Global_Histeq(img_in,BitDepth,u,v)
[he_in,wd_in,dim] = size(img_in);
range = bitshift(1,BitDepth);
ndf = zeros(dim,range);
pdf = zeros(dim,range);
total_num = he_in*wd_in;
acc_sum = zeros(dim);
cdf = zeros(dim,range);
Lut = zeros(dim,range);
pdf_u = zeros(1,3);

R_max = max(max(img_in(:,:,1)));G_max = max(max(img_in(:,:,2)));B_max = max(max(img_in(:,:,3)));
R_min = min(min(img_in(:,:,1)));G_min = min(min(img_in(:,:,2)));B_min = min(min(img_in(:,:,3)));
Max_Lut = [R_max,G_max,B_max];Min_Lut = [R_min,G_min,B_min];

%% statistic the ndf(number distribution function )��pdf and cdf.
%% range from the 1-256 ,related to the pixel 8 bits range to 0-255.
for i = 1:he_in
    for j = 1:wd_in
        for k = 1:3
            ndf(k,ceil(img_in(i,j,k)) + 1) = ndf(k,ceil(img_in(i,j,k)) + 1) + 1;
        end
    end
end


for i = 1:dim
    for j = 1:range
        pdf(i,j) = ndf(i,j)./total_num;
    end
end

%% modified the pdf and cdf according to the reference paper
pdf_u(1) = v.*max(pdf(1,:));pdf_u(2) = v.*max(pdf(2,:));pdf_u(3) = v.*max(pdf(3,:));

for i = 1:dim
    for j = 1:range
        if(pdf(i,j)<0.0001)
            pdf(i,j) = 0;
        elseif(pdf(i,j)>pdf_u(i))
            pdf(i,j) = pdf_u(i);
        else
            pdf(i,j) = pdf_u(i).* sqrt(((pdf(i,j) - 0.0001)./(pdf_u(i) - 0.0001)));
        end
    end
end


for i = 1:dim
    for j = 1:range
         pdf_out(i,j)= pdf(i,j)./ sum(pdf(i,:));
    end    
end

%% cal the cdf according to the pdf
for i = 1:dim
    for j = 1:range
        if(j == 1)
            cdf(i,j) = pdf_out(i,j);
        else
            cdf(i,j) = cdf(i,j - 1) + pdf_out(i,j);
        end
    end
end


%% cal the mapping Lut of the global histeq
for i = 1:dim
    for j = 1:range
        Lut(i,j) = min(max((range*cdf(i,j)),Min_Lut(i)),Max_Lut(i));
    end
end

%% golbal histeq process
for i = 1:he_in
    for j = 1:wd_in
        for k = 1:3
            histeq_out(i,j,k) = Lut(k,ceil(img_in(i,j,k)) + 1);
        end
    end
end 

end