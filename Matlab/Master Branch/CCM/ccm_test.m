%% This is the single test file of the module CCM (Color Correction Matrix),use the test case in the directory to test the CCM process.
%% The Position of CCM module in the ISP pipeline,is always after the AWB Process
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% The CCM Matrix is adopted to transfering the rgb color space to the srgb color space,here is just use eye(3,3) to replace while the real value needs to be demarcate

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                              
module_ccm_param = struct('camera_srgb_coeff',eye(3,3)); 
ccm_eb = 1;     %% enables the Auto White Balance process
addpath(genpath('../../'));
  
%% Read The Test Image In,always the data being processed after AWB filtering module
awb_out = imread('./Test Case/Case one/awb_test_out_mode0.png');

%% Main Process of CCM
ccm_out = module_ccm_process(awb_out,module_ccm_param,top_paramset,ccm_eb);

%% Disp Result in png format
figure(1),imshow(uint8(awb_out));
figure(2),imshow(uint8(ccm_out));
imwrite(uint8(ccm_out),'./Test Case/Case one/ccm_test_out.png');