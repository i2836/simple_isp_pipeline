%% This is the Process of CCM (Color Correction Matrix),for more detailed information,please look at the CCM_readme.txt and testcase_readme.txt
%% The position of the CCM module is always after by the AWB process in the ISP pipeline 
%% when ccm_eb = 1, enables the ccm process ,vice versa.

function [ccm_out] = module_ccm_process(awb_out,module_ccm_param,top_paramset,ccm_eb)
if(ccm_eb == 1) %% enables the awb process
    if(top_paramset.Auto == 1)
         ccm_matrix = eye(3,3); 
%          ccm_matrix = [1.2,-0.4,0.19;-0.3,1.4,0.5;0.1,0.38,1.6];
    else
         ccm_matrix = module_ccm_param.camera_srgb_coeff;  
    end
    
    [Img_Height,Img_Width,~] = size(awb_out);
    
    %% main process of CCM module for different mode 
    for i = 1:Img_Height
        for j = 1:Img_Width
              ccm_out(i,j,:) = reshape(double(awb_out(i,j,:)),1,3)*ccm_matrix;
        end
    end
    ccm_out = Matrix_Clip(ccm_out,top_paramset.BitDepth);
else
    ccm_out = awb_out;
end
end