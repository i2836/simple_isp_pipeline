This is the explantion of CCM (Color Correction Matrix) module, which is always occuring after the AWB processes.
The CCM adopts here is a 3*3 matrix which corrects from the rgb space to srgb color space. 
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                     %
%                                                                                                                                                                                                                        %
%             1.The CCM process is aimed at correcting from the rgb space to srgb color space. , there exist  one test case                                %
%                                                                                                                                                                                                                        %         
%                                                                                                                                                                                                                        %
%  File Explanation:                                                                                                                                                                                            %
%                                                                                                                                                                                                                        %
%             1. module_ccm_process.m: main process of ccm, input awb processed out and output the  CCM corrected  processed out          %
%                                                                                                                                                                                                                        %
%             2. ccm_test.m: test process of single ccm module, can be runned seperately beyond isp_top.m                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                       %
%                                                                                                                                                                                                                                          %
%             1. ccm_eb : 1 —— Open the ccm process , 0 ——Close the ccm process and directly output the input data                                                     %        
%                                                                                                                                                                                                                                          %                                                                                                          
%             2. module_ccm_param.matrix:  set the correction coefficient of 3*3 digtial matrix from the rgb space to the srgb space                                  %                                                                                                                                                                    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%