%% This is the OpenSource Version 0.1 of the basic ISP OnLine Demo
%% Author:Zhihan Zhang  Address: viplab  Department of Microelectric  Fudan University Time:2022/03/09
%% You can view and reuse it only for education but not commerical, some crucial code is encrypted as p-code,but you can also run it for test
%% for anyquestion, send the mails to address: 20212020087@fudan.edu.cn

%% This file is the top function of the Matlab Algorithm, it is used to play the onLine Demo, 
    %% user paramter adaptively set and global control of Pipeline
    %% params in this file: top_param、data_param、xxx_module_param
    
    
clear;clc;close all;
addpath(genpath('../'));

%% top param set 
top_paramset = struct("isp_eb",1, ...,
                        'Auto',1, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
top_eb = struct('fpn_eb',1,'blc_eb',1,'dgain_eb',1,'lsc_eb',1,'dpc_eb',1,...,
                 'gb_eb',1,'awb_eb',1,'sp_eb',1,'rawdns_eb',1,'demosiac_eb',1,'wavelet_eb',1,...,
                 'ae_eb',1,'ccm_eb',1,'histeq_eb',1,'ltm_eb',1,'sa_eb',1,'rgb_denoise_eb',1, ...,
                 'gamma_eb',1,'rgb2yuv_eb',1,'fcr_y_ee_eb',1,'y_denoise_eb',1,'yuv_fc_eb',1,'crop_eb',1);


%% seperately module param set and declaration,some should be updated as later
module_fpn_param = struct('mov_thre',30,'Cbsize',3 ,'Sbize',11);
module_blc_param = struct('blc_value',0);
module_dgain_param = struct('rgain',1,'bgain',1,'gain',1);
module_lsc_param = struct("lens_demarcate_value",ones(17,13),"mode",0);
module_dpc_param = struct("bsize",5,"mode",0);
module_gb_param = struct("bsize",5,"mode",0);
module_sp_param = struct("bsize",5,"thre",100);
module_rawdns_param = struct('sigma',30,"mode",0);
module_demosaic_param = struct("mode",0);
module_wavelet_param = struct('sigma',30);
module_awb_param = struct("rgain",1,'bgain',1,'bgain',1,"mode",0);
module_ccm_param = struct("camera_srgb_coeff",ones(3,4));
module_histeq_param = struct("mode",0);
module_ltm_param = struct("mode",0);
module_sa_prama = strcut("stretch_coeff",1);
module_rgb_denoise_param = struct('sigma',30,"mode",1);
module_gamma_param = struct("mode",0);
module_fcr_y_ee_param = struct("mode",0);
module_ydenoise_param = struct('sigma',30,"mode",0);



%% data space allocations for different modules
images_in = double(zeros(top_paramset.height,top_paramset.width,top_paramset.frames));
fpn_out = double(zeros(top_paramset.height,top_paramset.width));
blc_out = double(zeros(top_paramset.height,top_paramset.width));
dgain_out = double(zeros(top_paramset.height,top_paramset.width));
lsc_out = double(zeros(top_paramset.height,top_paramset.width));
dpc_out = double(zeros(top_paramset.height,top_paramset.width));
gb_out = double(zeros(top_paramset.height,top_paramset.width));
sp_out = double(zeros(top_paramset.height,top_paramset.width));
rawdns_out = double(zeros(top_paramset.height,top_paramset.width));
demosaic_out = double(zeros(top_paramset.height,top_paramset.width,3));
wavelet_out = double(zeros(top_paramset.height,top_paramset.width,3));
awb_out = double(zeros(top_paramset.height,top_paramset.width,3));
ccm_out = double(zeros(top_paramset.height,top_paramset.width,3));
histeq_out = double(zeros(top_paramset.height,top_paramset.width,3));
ltm_out = double(zeros(top_paramset.height,top_paramset.width,3));
sa_out = double(zeros(top_paramset.height,top_paramset.width,3));
rgb_denoise_out = double(zeros(top_paramset.height,top_paramset.width,3));
gamma_out = double(zeros(top_paramset.height,top_paramset.width,3));
rgb2yuv_out = double(zeros(top_paramset.height,top_paramset.width,3));
fcr_y_ee_out = double(zeros(top_paramset.height,top_paramset.width,3));
ydenoise_out = double(zeros(top_paramset.height,top_paramset.width,3));
yuv_fc_out = double(zeros(top_paramset.height,top_paramset.width,3));
crop_out = double(zeros(top_paramset.height,top_paramset.width,3));  


%% image read in :number of frames in top_paramset
cache = uint8(zeros(top_paramset.height*top_paramset.width,1));

for i = 1:top_paramset.frames
     [file,path] = uigetfile('*.raw');
     fileptr(i) = fopen([path,file],'r');
     cache = fread(fileptr(i),[top_paramset.height,top_paramset.width],'uint8');
     images_in(i) = cache.reshape(top_paramset.height,top_paramset.width);
     fclose(fileptr(i));
end

%% run isp in the pipeline modules
tic;
fpn_out = module_fpn_process(images_in,module_fpn_param,top_paramset,top_eb.fpn_eb);
blc_out = module_blc_process(fpn_out,module_blc_param,top_paramset,top_eb.blc_eb);
dgain_out = module_dgain_process(blc_out,module_dgain_param,top_paramset,top_eb.dgain_eb);
lsc_out = module_lsc_process(dgain_out,module_lsc_param,top_paramset,top_eb.lsc_eb);
dpc_out = module_dpc_process(lsc_out,module_dpc_param,top_paramset,top_eb.dpc_eb);
gb_out = module_gb_process(dpc_out,module_gb_param,top_paramset,top_eb.gb_eb);
sp_out = module_sp_process(gb_out,module_sp_param,top_paramset,top_eb.sp_eb);
rawdns_out = module_rawdns_process(sp_out,module_rawdns_param,top_paramset,top_eb.rawdns_eb);
demosaic_out = module_demosaic_process(rawdns_out,module_demosaic_param,top_paramset,top_eb.demosaic_eb);
wavelet_out = module_wavelet_process(demosaic_out,module_wavelet_param,top_paramset,top_eb.wavelet_eb);
awb_out = module_awb_process(wavelet_out,module_awb_param,top_paramset,top_eb.awb_eb);
ccm_out = module_ccm_process(awb_out,module_ccm_param,top_paramset,top_eb.ccm_eb);
histeq_out = module_histeq_process(ccm_out,module_histeq_param,top_paramset,top_eb.histeq_eb);
ltm_out = module_ltm_process(histeq_out,module_ltm_param,top_paramset,top_eb.ltm_eb);
sa_out = module_sa_process(ltm_out,module_sa_param,top_paramset,top_eb.sa_eb);
rgb_denoise_out = module_rgb_denoise_process(sa_out,module_rgb_denoise_param,top_paramset,top_eb.rgb_denoise_eb);
module_gamma_process(rgb_denoise_out,gamma_out,module_gamma_param,top_paramset,top_eb.gamma_eb);
module_rgb2yuv_process(gamma_out,rgb2yuv_out,module_rgb2yuv_param,top_paramset,top_eb.rgb2yuv_eb);
module_fcr_y_ee_process(rgb2yuv_out,fcr_y_ee_out,module_fcr_y_ee_param,top_paramset,top_eb.fcr_y_ee_eb);
module_ydenoise_process(fcr_y_ee_out,ydenoise_out,module_ydenoise_param,top_paramset,top_eb.y_denoise_eb);
module_yuv_fc_process(ydenoise_out,yuv_fc_out,top_paramset,top_eb.yuv_fc_eb);
module_crop_process(yuv_fc_out,crop_out,top_paramset,top_eb.crop_eb);
toc;


%%disp and store the final image

if(top_paramset.disp)
    figure(1);
        subplot(1,2,1),imshow(uint8(images_in(1))),title('fussion image one');subplot(1,2,2),imshow(uint8(images_in(2))),title('fussion image two');
        subplot(2,2,3),imshow(uint8(images_in(3))),title('fussion image three');subplot(2,2,4),imshow(uint8(images_in(4))),title('fussion image fourc');

    figure(2);
        imshow(uint8(fpn_out),[0,255]),title('fpn out image');

    figure(3);
        imshow(uint8(blc_out),[0,255]),title('blc out image');

    figure(4);
        imshow(uint8(dgain_out),[0,255]),title('dgain out image');

    figure(5);
        imshow(uint8(lsc_out),[0,255]),title('lsc out image');

    figure(6);
        imshow(uint8(dpc_out),[0,255]),title('dpc out image');
    
    figure(7);
        imshow(uint8(gb_out),[0,255]),title('gb out image');

    figure(8);
        imshow(uint8(sp_out),[0,255]),title('sp out image');

    figure(9);
        imshow(uint8(rawdns_out),[0,255]),title('rawdns out image');

    figure(10);
        imshow(uint8(demosaic_out),[0,255]),title('demosaic out image');

    figure(11);
        imshow(uint8(wavelet_out),[0,255]),title('wavelet out image');

    figure(12);
        imshow(uint8(awb_out),[0,255]),title('awb out image');

    figure(13);
        imshow(uint8(ccm_out),[0,255]),title('ccm out image');

    figure(14);
        imshow(uint8(histeq_out),[0,255]),title('histeq out image');

    figure(15);
        imshow(uint8(ltm_out),[0,255]),title('ltm out image');

    figure(16);
        imshow(uint8(sa_out),[0,255]),title('sa out image');

    figure(17);
        imshow(uint8(rgb_denoise_out),[0,255]),title('rgb denoise out image');

    figure(18);
        imshow(uint8(gamma_out),[0,255]),title('gamma out image');

    figure(19);
        imshow(uint8(yuv2rgb(rgb2yuv_out)),[0,255]),title('rgb2yuv out image');

    figure(20);
        imshow(uint8(yuv2rgb(fcr_y_ee_out)),[0,255]),title('fcr+y_ee out image');

    figure(21);
        imshow(uint8(yuv2rgb(ydenoise_out)),[0,255]),title('ydenoise out image');

    figure(22);
        imshow(uint8(crop(yuv2rgb(ydenoise_out))),[0,255]),title('cropped image');
end

imwrite(uint8(yuv2rgb(fcr_y_ee_out)),'test_out.raw');
fprintf('isp process down!\n');