%% This is the OpenSource Version 0.1 of the basic ISP OnLine Demo
%% Author:zzh1998   Time:2022/03/09
%% You can view and reuse it only for education but not commerical, some crucial code might be encrypted as p-code,but you can also run it for test
%% for anyquestion, send the mails to address: zzh_069983430@163.com

%% This file is the top function of the Matlab Algorithm, it is used to play the onLine Demo, 
    %% user paramter adaptively set and global control of Pipeline
    
   
clear;clc;close all;
addpath(genpath('../'));

%% top param set 
top_paramset = struct('isp_eb',1, ...,   %enable the top isp process
                        'Auto',0, ...,   %enable the auto param set
                        'BitDepth',8, ..., %set the bitdepth of image
                        'Bayer','rggb', ...,  %set the cfa pattern
                        'frames',2, ..., %set the number of multi-frames
                        'height', 1080, ..., %set the height of input image
                        'width', 1920, ...,  %set the width of input image
                        'format','raw',...,  %express the format of input image
                        'disp',1);  %whether disp the sub-module processed result
                    
top_eb = struct('fpn_eb',1,'blc_eb',1,'dgain_eb',1,'lsc_eb',1,'dpc_eb',1,...,
                 'gb_eb',1,'awb_eb',1,'sp_eb',1,'rawdns_eb',1,'demosaic_eb',1,'wavelet_eb',1,...,
                 'ae_eb',1,'ccm_eb',1,'histeq_eb',1,'ltm_eb',1,'sa_eb',1,'rgb_denoise_eb',1, ...,
                 'gamma_eb',1,'rgb2yuv_eb',1,'fcr_y_ee_eb',1,'y_denoise_eb',1,'yuv_fc_eb',1,'crop_eb',1);


%% seperately module param set and declaration,some should be updated as later
module_fpn_param = struct('mov_thre',50,'Cbsize',3 ,'Sbize',7);
module_blc_param = struct('blc_value',10);
module_dgain_param = struct('rDain',1,'bDain',1,'gDain',1);
module_lsc_param = struct('lens_demarcate_value',ones(13,17));
module_dpc_param = struct('mode',2,'Jeehoon_Th',20,'Jeehoon_Num',5000); %Three methods: 0 Jeehoon_Th,1 chans, 2 Chao-Yi 
module_gb_param = struct('bsize',5,'Threhold',5);
module_sp_param = struct('bsize',5,'Low_Thre',5,'High_Thre',250); 
module_rawdns_param = struct('LOG_bsize',9,'Filter_bsize',7,'Lmax',60,'Sigma_LOG',1.5); %% ABF filter
module_demosaic_param = struct('mode',3,'Tao',2);%%Tao is the paramter of the mode3��UVC 
module_2dwavelet_param = struct('bsize',4); %%2Dbsize is the width of the 2DWavelet Decomposition block,always be the even number
module_awb_param = struct('mode',6,'Dy_T',5,'PF_T',0.2,'rGain',1,'gGain',1,'bGain',1); %% mode:switch the adopted AWB method, Dy_T/Edge_T/PF_T are seperately related to the threhold/precentage in the Dynamic threhold/Edge Detection based/Perfect Reflecting methods
module_ccm_param = struct('camera_srgb_coeff',eye(3,3)); %% the initial set of CCM matrix is a 3*3 matrix
module_histeq_param = struct('mode',2,'Lbsize',15,'u',0.5,'v',0.5); %% u and v are paramters of methods: modified Global Histeq
module_ltm_param = struct('s',0.75,'a',4); %% parameter s and a are sepearately the params of the Index number of the remapping function/the degree of the compression curve
module_sa_param = struct('coeff',4); %% set the linear coeff of adjusted S components
module_gamma_param = struct('mode',1,'k',0.8,'s',1);%% two gamma methods:AEGMC/AGCWD, k and s are seperately the coeff of two methods
module_rgb_denoise_param = struct('mode',1,'sbize',7,'cbize',3); %% modes controls the 5 noise estiamtion methods and sbize/cbize seperately the search block size and comp block size of RGB nlm filter
module_rgb2yuv_param = struct('mode',1); %% mode control the different color transfer standard of RGB2YUV space
module_fcr_y_ee_param = struct('mode',2,'coeff',5); %% mode controls the y_ee methods while the coeff control the sharpen intensity
module_crop_param = struct('LIndex',200,'RIndex',1000,'TIndex',100,'DIndex',700); %% set the remain component inthe img


%% data space allocations for different modules
images_in = double(zeros(top_paramset.height,top_paramset.width,top_paramset.frames));
fpn_out = double(zeros(top_paramset.height,top_paramset.width));
blc_out = double(zeros(top_paramset.height,top_paramset.width));
dgain_out = double(zeros(top_paramset.height,top_paramset.width));
lsc_out = double(zeros(top_paramset.height,top_paramset.width));
sp_out = double(zeros(top_paramset.height,top_paramset.width));
dpc_out = double(zeros(top_paramset.height,top_paramset.width));
gb_out = double(zeros(top_paramset.height,top_paramset.width));
rawdns_out = double(zeros(top_paramset.height,top_paramset.width));
demosaic_out = double(zeros(top_paramset.height,top_paramset.width,3));
wavelet_out = double(zeros(top_paramset.height,top_paramset.width,3));
awb_out = double(zeros(top_paramset.height,top_paramset.width,3));
ccm_out = double(zeros(top_paramset.height,top_paramset.width,3));
histeq_out = double(zeros(top_paramset.height,top_paramset.width,3));
ltm_out = double(zeros(top_paramset.height,top_paramset.width,3));
sa_out = double(zeros(top_paramset.height,top_paramset.width,3));
rgb_denoise_out = double(zeros(top_paramset.height,top_paramset.width,3));
gamma_out = double(zeros(top_paramset.height,top_paramset.width,3));
rgb2yuv_out = double(zeros(top_paramset.height,top_paramset.width,3));
fcr_y_ee_out = double(zeros(top_paramset.height,top_paramset.width,3));
yuv_fc_out = double(zeros(top_paramset.height,top_paramset.width,3));
crop_out = double(zeros(module_crop_param.DIndex - module_crop_param.TIndex,module_crop_param.RIndex - module_crop_param.LIndex,3));  


%% image read in :number of frames in top_paramset
cache = uint8(zeros(top_paramset.height*top_paramset.width,1));

for i = 1:top_paramset.frames
     [file,path] = uigetfile('*.raw');
     fileptr(i) = fopen([path,file],'r');
     cache = fread(fileptr(i),[top_paramset.width,top_paramset.height],'uint8');
     images_in(:,:,i) = cache';
     fclose(fileptr(i));
end

%% run isp in the pipeline modules
tic;
fpn_out = module_fpn_process(images_in,module_fpn_param,top_paramset,top_eb.fpn_eb);
blc_out = module_blc_process(fpn_out,module_blc_param,top_paramset,top_eb.blc_eb);
dgain_out = module_dgain_process(blc_out,module_dgain_param,top_paramset,top_eb.dgain_eb);
lsc_out = module_lsc_process(dgain_out,module_lsc_param,top_paramset,top_eb.lsc_eb);
sp_out = module_sp_process(lsc_out,module_sp_param,top_paramset,top_eb.sp_eb);  %%test point,fixed the bug in raw domain
dpc_out = module_dpc_process(sp_out,module_dpc_param,top_paramset,top_eb.dpc_eb); %%test point
gb_out = module_gb_process(dpc_out,module_gb_param,top_paramset,top_eb.gb_eb);
rawdns_out = module_rawdns_process(gb_out,module_rawdns_param,top_paramset,top_eb.rawdns_eb);
demosaic_out = module_demosaic_process(rawdns_out,module_demosaic_param,top_paramset,top_eb.demosaic_eb);
wavelet_out = module_2dwavelet_process(demosaic_out,module_2dwavelet_param,top_paramset,top_eb.wavelet_eb);
awb_out = module_awb_process(wavelet_out,module_awb_param,top_paramset,top_eb.awb_eb);
ccm_out = module_ccm_process(awb_out,module_ccm_param,top_paramset,top_eb.ccm_eb);
histeq_out = module_histeq_process(ccm_out,module_histeq_param,top_paramset,top_eb.histeq_eb);
ltm_out = module_ltm_process(histeq_out,module_ltm_param,top_paramset,top_eb.ltm_eb);
sa_out = module_sa_process(ltm_out,module_sa_param,top_paramset,top_eb.sa_eb);
rgb_denoise_out = module_rgbdenoise_process(sa_out,module_rgb_denoise_param,top_paramset,top_eb.rgb_denoise_eb);
gamma_out = module_gamma_process(rgb_denoise_out,module_gamma_param,top_paramset,top_eb.gamma_eb);
rgb2yuv_out = module_rgb2yuv_process(gamma_out,module_rgb2yuv_param,top_paramset,top_eb.rgb2yuv_eb);
fcr_y_ee_out = module_FCR_YEE_process(rgb2yuv_out,module_fcr_y_ee_param,top_paramset,top_eb.fcr_y_ee_eb);
crop_out = module_crop_process(fcr_y_ee_out,module_crop_param, top_paramset,top_eb.crop_eb);
toc;

%% disp and store the final image

if(top_paramset.disp)
    figure(1);
        subplot(1,2,1),imshow(uint8(images_in(:,:,1))),title('fussion image one');subplot(1,2,2),imshow(uint8(images_in(:,:,2))),title('fussion image two');
    figure(2);
        imshow(uint8(fpn_out),[0,255]),title('fpn out image');
        imwrite(uint8(fpn_out),'fpn_out.png');
    figure(3);
        imshow(uint8(blc_out),[0,255]),title('blc out image');
        imwrite(uint8(blc_out),'blc_out.png');
    figure(4);
        imshow(uint8(dgain_out),[0,255]),title('dgain out image');
        imwrite(uint8(dgain_out),'dgain_out.png');
    figure(5);
        imshow(uint8(lsc_out),[0,255]),title('lsc out image');
        imwrite(uint8(lsc_out),'lsc_out.png');
    figure(6);
        imshow(uint8(dpc_out),[0,255]),title('dpc out image');   
        imwrite(uint8(dpc_out),'dpc_out.png');
    figure(7);
        imshow(uint8(gb_out),[0,255]),title('gb out image');
        imwrite(uint8(gb_out),'gb_out.png');
    figure(8);
        imshow(uint8(sp_out),[0,255]),title('sp out image');
        imwrite(uint8(sp_out),'sp_out.png');
    figure(9);
        imshow(uint8(rawdns_out),[0,255]),title('rawdns out image');
        imwrite(uint8(rawdns_out),'rawdns_out.png');
    figure(10);
        imshow(uint8(demosaic_out),[0,255]),title('demosaic out image');
        imwrite(uint8(demosaic_out),'demosaic_out.png');
    figure(11);
        imshow(uint8(wavelet_out),[0,255]),title('wavelet out image');
        imwrite(uint8(wavelet_out),'wavelet_out.png');
    figure(12);
        imshow(uint8(awb_out),[0,255]),title('awb out image');
        imwrite(uint8(awb_out),'awb_out.png');
    figure(13);
        imshow(uint8(ccm_out),[0,255]),title('ccm out image');
        imwrite(uint8(ccm_out),'ccm_out.png');
    figure(14);
        imshow(uint8(histeq_out),[0,255]),title('histeq out image');
        imwrite(uint8(histeq_out),'histeq_out.png');
    figure(15);
        imshow(uint8(ltm_out),[0,255]),title('ltm out image');
        imwrite(uint8(ltm_out),'ltm_out.png');
    figure(16);
        imshow(uint8(sa_out),[0,255]),title('sa out image');
        imwrite(uint8(sa_out),'sa_out.png');
    figure(17);
        imshow(uint8(rgb_denoise_out),[0,255]),title('rgb denoise out image');
        imwrite(uint8(rgb_denoise_out),'rgb_denoise_out.png');
    figure(18);
        imshow(uint8(gamma_out),[0,255]),title('gamma out image');
        imwrite(uint8(gamma_out),'gamma_out.png');
    figure(19);
        imshow(uint8(rgb2yuv_out),[0,255]),title('rgb2yuv out image');
        imwrite(uint8(rgb2yuv_out),'rgb2yuv_out.png');
    figure(20);
        imshow(uint8(fcr_y_ee_out),[0,255]),title('fcr+y_ee out image');
        imwrite(uint8(fcr_y_ee_out),'fcr_y_ee_out.png');
    figure(21);
        imshow(uint8(crop_out),[0,255]),title('cropped image');
        imwrite(uint8(crop_out),'crop_out.png');
end

imwrite(uint8(crop_out),'test_out.png');
fprintf('isp process down!\n');
