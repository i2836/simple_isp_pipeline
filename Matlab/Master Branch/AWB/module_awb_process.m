%% This is the Process of AWB (Auto White Balancing process),for more detailed information,please look at the AWB_readme.txt and testcase_readme.txt
%% The position of the AWB module is always after by the 2DWavelet filtering process in the ISP pipeline 
%% when awb_eb = 1, enables the awb process ,vice versa.
%% For the method of awb algorithm adopted here,please refers to the passages
    %% "Development and Implementation of Automatic White Balance Based on Luminance Compensation",for modes�� 0����3
    %% "A Novel Automatic White Balance Method  For Digital Still Cameras ",for mode 4
    %% "Fast Automatic White Balancing Method by Color Histogram Stretching", for mode 5
    %% "https://blog.csdn.net/yz2zcx/article/details/105328002", for mode 6

function [awb_out] = module_awb_process(Wavelet_out,module_awb_param,top_paramset,awb_eb)
if(awb_eb == 1) %% enables the awb process
    if(top_paramset.Auto ==1)
         mode = 0;
         Dy_T = 5;
         PF_T = 0.2;
         rGain = 1;
         gGain = 1;
         bGain = 1;
    else
         mode = module_awb_param.mode;
         Dy_T = module_awb_param.Dy_T;
         PF_T = module_awb_param.PF_T;
         rGain = module_awb_param.rGain;
         gGain = module_awb_param.gGain;
         bGain = module_awb_param.bGain;    
    end
    
    [~,~,dim] = size(Wavelet_out);
    
    %% main process of AWB module for different mode 
    switch mode
        case 0
            [rGain,gGain,bGain] = Grey_World(double(Wavelet_out));
        case 1
            [rGain,gGain,bGain] = Retinex(double(Wavelet_out));
        case 2
            [rGain,gGain,bGain] = MaxWhite(double(Wavelet_out));
        case 3
            awb_out =  Matrix_Clip(LCAWB(double(Wavelet_out),top_paramset.BitDepth),top_paramset.BitDepth);
        case 4
            [rGain,gGain,bGain] = Dynamic_Threhold(double(Wavelet_out),Dy_T);
        case 5
            awb_out = Matrix_Clip(Histogram_Strtched_AWB(double(Wavelet_out),top_paramset.BitDepth),top_paramset.BitDepth);
        case 6
            [rGain,gGain,bGain] = Perfect_Reflecing_AWB(double(Wavelet_out),top_paramset.BitDepth,PF_T);
        otherwise
            [rGain,gGain,bGain] = Grey_World(double(Wavelet_out));
    end
     
    gain = [rGain,gGain,bGain];
    
    if((mode~=3)&&(mode~=5))
        for i = 1:dim
            awb_out(:,:,i) =  Matrix_Clip(Wavelet_out(:,:,i).*gain(i),top_paramset.BitDepth);
        end 
    end
else
    awb_out = Wavelet_out;
end
end

function [rGain,gGain,bGain] = Grey_World(img_in)
R_sum = sum(sum(img_in(:,:,1)));
G_sum = sum(sum(img_in(:,:,2)));
B_sum = sum(sum(img_in(:,:,3)));

rGain = (R_sum+G_sum+B_sum)./(3*R_sum);
gGain = (R_sum+G_sum+B_sum)./(3*G_sum);
bGain = (R_sum+G_sum+B_sum)./(3*B_sum);
end

function [rGain,gGain,bGain] = Retinex(img_in)
R_max = max(max(img_in(:,:,1)));
G_max = max(max(img_in(:,:,2)));
B_max = max(max(img_in(:,:,3)));

rGain = G_max/R_max;
bGain = G_max/B_max;
gGain = 1;
end

function [rGain,gGain,bGain] = MaxWhite(img_in)
R_max = max(max(img_in(:,:,1)));
G_max = max(max(img_in(:,:,2)));
B_max = max(max(img_in(:,:,3)));

rGain = 255/R_max;
bGain = 255/B_max;
gGain = 255/G_max;
end

function [awb_out] = LCAWB(img_in,BitDepth)
R_max = max(max(img_in(:,:,1)));
G_max = max(max(img_in(:,:,2)));
B_max = max(max(img_in(:,:,3)));
R_avg = sum(sum(img_in(:,:,1)))./numel(img_in(:,:,1));
G_avg = sum(sum(img_in(:,:,2)))./numel(img_in(:,:,1));
B_avg = sum(sum(img_in(:,:,3)))./numel(img_in(:,:,1));

%% fsolve the non-linear equation set
x0 = [0,0]; 
R_Coeff = fsolve( @(x)LCAWB_root(x,R_max,R_avg,G_avg,B_avg),x0);
G_Coeff = fsolve( @(x)LCAWB_root(x,G_max,R_avg,G_avg,B_avg),x0);
B_Coeff = fsolve( @(x)LCAWB_root(x,B_max,R_avg,G_avg,B_avg),x0);

awb_out(:,:,1) = Matrix_Clip(R_Coeff(1)*(img_in(:,:,1).^2)+R_Coeff(2)*img_in(:,:,1),BitDepth);    
awb_out(:,:,2) = Matrix_Clip(G_Coeff(1)*(img_in(:,:,2).^2)+G_Coeff(2)*img_in(:,:,2),BitDepth);  
awb_out(:,:,3) = Matrix_Clip(B_Coeff(1)*(img_in(:,:,3).^2)+B_Coeff(2)*img_in(:,:,3),BitDepth);  
end

function [rGain,gGain,bGain] = Dynamic_Threhold(img_in,Dy_T)
[he_in,wd_in,~] = size(img_in);
Db = zeros(3,4); Dr = zeros(3,4);
Mb = zeros(3,4); Mr = zeros(3,4);
Pos = ones(3,4);

%% rgb color space to ycbcr color space
img_ycbcr_in = rgb2ycbcr(img_in);
Ymax = max(max(img_ycbcr_in(:,:,1)));

%% statistic the mean_cb/mean_cr and Diff_cb/Diff_cr
for i = 1:3
    for j = 1:4
        Mb(i,j) = sum(sum(img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,2)))/(he_in*wd_in/12);
        Mr(i,j) = sum(sum(img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,3)))/(he_in*wd_in/12);
    end
end

for i = 1:3
    for j = 1:4
        Db(i,j) = sum(sum(abs(img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,2) - Mb(i,j))))./(he_in*wd_in/12);
        Dr(i,j) = sum(sum(abs(img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,3) - Mr(i,j))))./(he_in*wd_in/12);
    end
end

%% ignores the Db/Dr block position when the Db/Dr value are both lower than the preset threhold
for i = 1:3
    for j = 1:4
         if((Db(i,j)<Dy_T)&&(Dr(i,j)<Dy_T))
             Pos(i,j) = 0;
         end
    end
end

%% pre-select the near-white region,the top 10% of the brightness point in the candidate region
for i = 1:3
    for j = 1:4
        if(Pos(i,j))
            y_temp = img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,1);
            cb_temp = img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,2);
            cr_temp = img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,3);
            
            for m = 1:he_in/3
                for n = 1:wd_in/4
                    if((abs(cb_temp(m,n)-Mb(i,j)-Db(i,j)*sign(Mb(i,j)))<(1.5*Db(i,j)))&&(abs(cr_temp(m,n)-1.5 * Mr(i,j)-Dr(i,j)*sign(Mr(i,j)))<(1.5*Dr(i,j))))
                        y_temp(m,n) = 0;
                    end
                end
            end
            img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,1) = y_temp;              
        else
            img_ycbcr_in((i-1)*he_in/3+1:i*he_in/3,(j-1)*wd_in/4+1:j*wd_in/4,1) = 0;
        end
    end
end


for i = 1:3
    img_ycbcr_out(:,:,i) = reshape(img_ycbcr_in(:,:,i),1,he_in*wd_in);
end

%% sort and find the top 10% of Brightness:Component Y in the YCbCr space
[~,Index_full] = sort(img_ycbcr_out(:,:,1),'descend');
Index_white = Index_full(1:he_in*wd_in/10);

R_in = img_in(:,:,1);G_in = img_in(:,:,2);B_in = img_in(:,:,3);
R_avg = sum(sum(R_in(Index_white)))./(numel(Index_white));
G_avg = sum(sum(G_in(Index_white)))./(numel(Index_white));
B_avg = sum(sum(B_in(Index_white)))./(numel(Index_white));

rGain = Ymax./R_avg;gGain = Ymax./G_avg;bGain = Ymax./B_avg;
end

function [awb_out] = Histogram_Strtched_AWB(img_in,BitDepth)
[he_in,wd_in,dim] = size(img_in);
R_min = min(min(img_in(:,:,1)));
G_min = min(min(img_in(:,:,2)));
B_min = min(min(img_in(:,:,3)));
range = bitshift(1,BitDepth);
Hist = zeros(dim,range);

%% statistic the R/G/B hist seperately
for i = 1:he_in
    for j = 1:wd_in
        for k = 1:dim
            Hist(k,ceil(img_in(i,j,k))+1) = Hist(k,ceil(img_in(i,j,k))+1) + 1;
        end
    end
end

%% ensure the  num_thre of R/G/B L/H band
L_num = he_in*wd_in/100;
H_num = he_in*wd_in*99/100;

accum_r = 0;accum_g = 0;accum_b= 0;
flag_rl = 0;flag_gl = 0;flag_bl = 0;
flag_rh = 0;flag_gh = 0;flag_bh = 0;
for i = 1:range
    accum_r = accum_r + Hist(1,i);
    accum_g = accum_g + Hist(2,i);
    accum_b = accum_b + Hist(3,i);
    
    if((accum_r>L_num) && (~flag_rl))
        LRbond = i;
        flag_rl = 1;
    end
    
    if((accum_g>L_num) && (~flag_gl))
        LGbond = i;
        flag_gl = 1;
    end
    
    if((accum_b>L_num) && (~flag_bl))
        LBbond = i;
        flag_bl = 1;
    end    
    
    if((accum_r>H_num) && (~flag_rh))
        HRbond = i;
        flag_rh = 1;
    end
    
    if((accum_g>H_num) && (~flag_gh))
        HGbond = i;
        flag_gh = 1;
    end
    
    if((accum_b>H_num) && (~flag_bh))
        HBbond = i;
        flag_bh = 1;
    end  
end

%% stretch the hist according to the L/H bands
awb_out(:,:,1) = Matrix_Clip(((img_in(:,:,1)-LRbond)/(HRbond-LRbond)*255 + R_min),BitDepth);
awb_out(:,:,2) = Matrix_Clip(((img_in(:,:,2)-LGbond)/(HGbond-LGbond)*255 + G_min),BitDepth);
awb_out(:,:,3) = Matrix_Clip(((img_in(:,:,3)-LBbond)/(HBbond-LBbond)*255 + B_min),BitDepth);
end

function [rGain,gGain,bGain] = Perfect_Reflecing_AWB(img_in,BitDepth,PF_T)
[he_in,wd_in,~] = size(img_in);
R_max = max(max(img_in(:,:,1)));
G_max = max(max(img_in(:,:,2)));
B_max = max(max(img_in(:,:,3)));
sum_map = sum(img_in,3);
T_num = he_in*wd_in*PF_T;
range = bitshift(1,BitDepth);
Sum_Hist = zeros(1,3*range);

%% statistic the hist of the sum of R/G/B
for i = 1:he_in
    for j = 1:wd_in
        Sum_Hist(ceil(sum_map(i,j))+1) = Sum_Hist(ceil(sum_map(i,j))+1) + 1;       
    end
end

% LBond = min(min(sum_map));HBond = max(max(sum_map));
accum = 0;
%% ensure the threhold of the white point
for i = 1:range*3
  accum = accum+Sum_Hist(range*3 - i + 1);
  if(accum>T_num)
      Thre = range*3 - i + 1;
      break;
  end    
end

accum_r_value = 0; accum_g_value = 0; accum_b_value = 0;
num = 0;  

%% cal the R_avg/G_avg/B_avg
for i = 1:he_in
    for j = 1:wd_in
        if(sum_map(i,j) >Thre)
            accum_r_value = accum_r_value + img_in(i,j,1);
            accum_g_value = accum_g_value + img_in(i,j,2);
            accum_b_value = accum_b_value + img_in(i,j,3);
            num = num + 1;
        end
    end
end

R_avg =  accum_r_value./num; G_avg =  accum_g_value./num;  B_avg =  accum_b_value./num;
rGain = R_max./R_avg;  gGain = G_max./G_avg;  bGain = B_max./B_avg;
end