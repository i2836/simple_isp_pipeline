       本AWB模块，在Matlab版本中共实现了7种算法，使用时根据参数Mode的位选值进行方法确定。具体测试效果见Test Case中的图像测试效果。
       简略来说，Rentinx、Max White、LCAWB等算法效果不算显著，但GW、RF、Hist and DT稍有效果。
       后续可以补充AWB算法效果的测试指标来具体评价AWB效果并且加入多色温下的测试图像。
