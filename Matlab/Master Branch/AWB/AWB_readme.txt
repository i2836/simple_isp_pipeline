This is the explantion of AWB (Auto White Balance) module, which is always occuring after the Demosaic and 2DWavelet processes.
For Genenal，here are 7 awb examples into open-source project ，which can be switched the paramter —— "mode"
For more information about the presented 7 awb methods, please refers to the description in the file ——"module_awb_process.m”   
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                     %
%                                                                                                                                                                                                                        %
%             1.The AWB process is aimed at recover the  Normal field scene temperature , there exist    one test case                                       %
%                                                                                                                                                                                                                        %         
%                                                                                                                                                                                                                        %
%  File Explanation:                                                                                                                                                                                            %
%                                                                                                                                                                                                                        %
%             1. module_awb_process.m: main process of dawb, input the demosaiced and 2D wavelet filtered processed out and output the  % 
%                                                         AWB corrected  processed out                                                                                                              %
%             2. awb_test.m: test process of single awb module, can be runned seperately beyond isp_top.m                                                      %
%             3. LCAWB_root.m： the function files adopted in the method: LCAWB,which is based on the fact that absolute value of color       %
%                                             temperature change is 0                                                                                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                       %
%                                                                                                                                                                                                                                          %
%             1. awb_eb : 1 —— Open the auto awb process , 0 ——Close the awb process and directly output the input data                                            %        
%                                                                                                                                                                                                                                          %                                                                                                           %
%             2. module_awb_param.mode:  control the awb methods adopts in the open-source project                                                                             %
%                                                               mode = 0/1/2/3/4/5/6/7, seperately related to the GW/Retinex/MaxWhite/LCAWB/DT/Hist/PF methods      %                                                                                % 
%             3. module_awb_param.Dy_T:  the threhold which is adopted in the  DT methods                                                                                               %
%             4. module_awb_param.PF_T:  the threhold which is adopted in the  PF methods                                                                                                %                                                                                                                                                                    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%