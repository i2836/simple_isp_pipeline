%% This is the single test file of the module AWB (Auto White Balancing),use the test case in the directory to test the AWB process.
%% The Position of AWB module in the ISP pipeline,is always after the 2DWavelet filtering Process
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% For the method of AWB algorithm adopted here,please refers to the passage ���� "daptive Wavelet Thresholding for Image Denoising and Compression"
%% Here are 7 simplest methods adopted in this module, which can be switched by the paramater����mode
     %% Mode 0��The Grey World Method��refers to ����"Development and Implementation of Automatic White Balance Based on Luminance Compensation"
     %% Mode 1��The Rentex Method��refers to ����"Development and Implementation of Automatic White Balance Based on Luminance Compensation"
     %% Mode 2��The Max White Method��refers to ����"Development and Implementation of Automatic White Balance Based on Luminance Compensation"
     %% Mode 3��The LCAWB method��refers to ����"Development and Implementation of Automatic White Balance Based on Luminance Compensation"
     %% Mode 4��Dynamic Threhold method, refers to ����"A Novel Automatic White Balance Method  For Digital Still Cameras " 
     %% Mode 5��Histogram Strtched based AWB method��refers to ���� "Fast Automatic White Balancing Method by Color Histogram Stretching" 
     %% Mode 6��Perfect Reflecing methods�� refers to ���� "https://blog.csdn.net/yz2zcx/article/details/105328002"
     
%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                              
module_awb_param = struct('mode',6,'Dy_T',5,'PF_T',0.2,'rGain',1,'gGain',1,'bGain',1); %% mode:switch the adopted AWB method, Dy_T/PF_T are seperately related to the threhold/precentage in the Dynamic threhold/Perfect Reflecting methods
awb_eb = 1;     %% enables the Auto White Balance process
addpath(genpath('../../'));
  
%% Read The Test Image In,always the data being processed after 2DWavelet filtering module
Wavelet_out = imread('./Test Case/Case one/2DWavelet_test_out.png');

%% Main Process of AWB
awb_out = module_awb_process(Wavelet_out,module_awb_param,top_paramset,awb_eb);

%% Disp Result in png format
figure(1),imshow(uint8(Wavelet_out));
figure(2),imshow(uint8(awb_out));
imwrite(uint8(awb_out),'./Test Case/Case one/awb_test_out_mode6.png');