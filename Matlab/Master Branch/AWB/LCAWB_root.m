function F = LCAWB_root(x,R_max,R_avg,G_avg,B_avg)
F(1) = x(1)*(R_max^2)+ x(2)*R_max -255;
F(2) = x(1)*(R_avg^2)+(x(2)+0.5752)*R_avg - 1.791*G_avg +0.2164*B_avg;
end