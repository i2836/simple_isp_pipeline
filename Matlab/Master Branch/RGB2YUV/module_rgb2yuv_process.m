%% This is the Process of RGB2YUV,for more detailed information,please look at the RGB2YUV_readme.txt and testcase_readme.txt
%% The position of rgb2yuv is always behinds the RGB Denoised process in the ISP pipeline
%% when rgb2yuv_eb = 1, enables the RGB2YUV process ,vice versa.

function [YUV_out] = module_rgb2yuv_process(rgbdenoised_out,module_rgb2yuv_param,top_paramset,rgb2yuv_eb)
if(rgb2yuv_eb)
    if(top_paramset.Auto == 1)
        mode = 0;
    else
        mode = module_rgb2yuv_param.mode;
    end
   
    switch mode
        case 0 
              YUV_out = RGB2YUV_601(rgbdenoised_out);
        case 1
              YUV_out = RGB2YUV_709(rgbdenoised_out);
        otherwise
              YUV_out = RGB2YUV_601(rgbdenoised_out);
    end   
else
    YUV_out = rgbdenoised_out;    
end
end

function [out] = RGB2YUV_601(in)
out(:,:,1) = 0.299*in(:,:,1) + 0.587*in(:,:,2) + 0.114*in(:,:,3);
out(:,:,2) = -0.147*in(:,:,1)- 0.289*in(:,:,2) + 0.436*in(:,:,3);
out(:,:,3) = 0.615*in(:,:,1) - 0.515*in(:,:,2) - 0.100*in(:,:,3);
end
function [out] = RGB2YUV_709(in)
out(:,:,1) = 0.2627.*in(:,:,1) + 0.6780.*in(:,:,2) + 0.0593.*in(:,:,3);
out(:,:,2) =-0.1396.*in(:,:,1) - 0.3604.*in(:,:,2) + 0.5.*in(:,:,3);
out(:,:,3) = 0.5.*in(:,:,1) - 0.4598.*in(:,:,2) - 0.0402.*in(:,:,3);
end