%% This is the single test file of the module rgb2yuv,use the test case in the directory to test the rgb2yuv process.
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_rgb2yuv_param = struct('mode',1);
rgb2yuv_eb = 1;
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after RGB Denoised process
rgbdenoised_out = imread('./Test Case/Case/rgbdenoised_test_out_mode0.png');
%rgbdenoised_out = imread('./test.png');
%% Main RGB2YUV Process
YUV_out = module_rgb2yuv_process(rgbdenoised_out,module_rgb2yuv_param,top_paramset,rgb2yuv_eb);

%% Disp Result in png format
figure(1),imshow(uint8(rgbdenoised_out));
figure(2),imshow(uint8(YUV_out));
imwrite(uint8(YUV_out),'./Test Case/Case/RGB2YUV_test_out_mode1.png');
%imwrite(uint8(YUV_out),'./test_out.png');
