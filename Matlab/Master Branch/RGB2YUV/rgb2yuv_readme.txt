This is the explantion of rgb2yuv module, which is always occuring after the RGB Denoised processes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation：                                                                                                                                                                                               %
%                                                                                                                                                                                                                                    %
%             1.The RGB2YUV process is aimed at transfering the color space , there exist one case                                                                                 %         
%                                                                                                                                                                                                                                    %
%  File Explanation:                                                                                                                                                                                                         %
%                                                                                                                                                                                                                                    %
%             1.module_rgb2yuv_process.m: main process of rgb2yuv , input the RGB Denoise processed images and output the  yuv processed out  %
%             2.rgb2yuv_test.m: test process of single rgb2yuv module, can be runned seperately beyond isp_top.m                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
       
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  param Explanation：                                                                                                                                                                                                 %
%                                                                                                                                                                                                                                    %
%             1. rgb2yuv_eb : 1 —  Open the rgb2yuv process ,  0 — Close the rgb2yuv process and output the input data                                            %                                                                                             
%             2. module_rgb2yuv_param.mode:   choose the standrad of transfer rules   0/1: IEEE 601 and IEEE 709                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

