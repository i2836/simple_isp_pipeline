%% This is the single test file of the module LTM (Local Tone Mapping process),use the test case in the directory to test the LTM process.
%% The Position of LTM module in the ISP pipeline,is always after the Histeq Process
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% There just adopts only one method to the local tone mapping process����refers to paper:"Natural HDR Image Tone Mapping Based on Retinex"
         %% parameter s:  set the Index number of the remapping function,default is 0.75  
         %% parameter a:  set the degree of the compression curve,default is 4  
       
%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                              
module_ltm_param = struct('s',0.75,'a',4); %% parameter s and a are sepearately the params of the Index number of the remapping function/the degree of the compression curve
ltm_eb = 1;     %% enables the Local Tone Mapping process
addpath(genpath('../../'));
  
%% Read The Test Image In,always the data being processed after Histeq processed module
% histeq_out = imread('./Test Case/Case one/histeq_test_out_mode2.png');
histeq_out = imread('./Test Case/Case Two/histeq_test_out_low_Exposure.png');
%% Main Process of LTM
ltm_out = module_ltm_process(histeq_out,module_ltm_param,top_paramset,ltm_eb);

%% Disp Result in png format
figure(1),imshow(uint8(histeq_out));
figure(2),imshow(uint8(ltm_out));
imwrite(uint8(ltm_out),'./Test Case/Case Two/ltm_test_out.png');