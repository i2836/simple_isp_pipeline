%% This is the Process of LTM (Local Tone Mapping),for more detailed information,please look at the LTM_readme.txt and testcase_readme.txt
%% The position of the LTM module is always after by the Histeq process in the ISP pipeline 
%% when ltm_eb = 1, enables the ltm process ,vice versa.
        %% s and a are seperately the paramter of the Index number of the remapping function,default is 0.75/the degree of the compression curve,default is 4 
function [ltm_out] = module_ltm_process(histeq_out,module_ltm_param,top_paramset,ltm_eb)
if(ltm_eb == 1) %% enables the histeq process
    if(top_paramset.Auto == 1)
        s = 0.75;
        a = 4;
    else
        s = module_ltm_param.s;
        a = module_ltm_param.a;
    end
    ltm_out = Matrix_Clip(Rentix_LTM(double(histeq_out),top_paramset.BitDepth,s,a),top_paramset.BitDepth);   
else
    ltm_out = histeq_out;
end
end


%% This is the main process function of the adopted Rentinex_LTM Algorithm
function [out] = Rentix_LTM(img_in,BitDepth,s,a)
%% initial and set the input data 
R_in = img_in(:,:,1);G_in = img_in(:,:,2);B_in = img_in(:,:,3);
L_in = 0.299.*R_in + 0.587.*G_in + 0.114.*B_in;

%% k factor ensures process
Kr = Curve_Lut(log(max(max(R_in))) - log(min(min(R_in)))); 
Kg = Curve_Lut(log(max(max(G_in))) - log(min(min(G_in)))); 
Kb = Curve_Lut(log(max(max(B_in))) - log(min(min(B_in))));

%% Rentinex process in the RGB channels
G_kernel = fspecial('gaussian',[3,3],1);
R_blur = conv2(R_in,G_kernel,'same');G_blur = conv2(G_in,G_kernel,'same');B_blur = conv2(B_in,G_kernel,'same');
R_ssr = exp(log(R_in) - Kr.*(log(R_blur +1)));
G_ssr = exp(log(G_in) - Kg.*(log(G_blur +1)));
B_ssr = exp(log(B_in) - Kb.*(log(B_blur +1)));

%% stretch the ssr result
R_ssr_max = max(max(R_ssr));R_ssr_min = min(min(R_ssr));
G_ssr_max = max(max(G_ssr));G_ssr_min = min(min(G_ssr));
B_ssr_max = max(max(B_ssr));B_ssr_min = min(min(B_ssr));

R_ssr = (R_ssr-R_ssr_min)./(R_ssr_max - R_ssr_min).*(bitshift(1,BitDepth));
G_ssr = (G_ssr-G_ssr_min)./(G_ssr_max - G_ssr_min).*(bitshift(1,BitDepth));
B_ssr = (B_ssr-B_ssr_min)./(B_ssr_max - B_ssr_min).*(bitshift(1,BitDepth));


%% Highlight compression process in the Y channel only 
L_ssr = 0.299.*R_ssr + 0.587.*G_ssr + 0.114.*B_ssr; Lmax = max(max(L_ssr));
L_out = (2*Lmax)./(1+exp((-1)*a*L_ssr/Lmax)) - Lmax;

%% clip the top 1%/ bottom 1% pixels out to get a more natural image
Clip_num = numel(L_out)/100; Max_Value = max(max(L_out));
Hist_L = zeros(1,ceil(Max_Value) + 1);
accum_L = 0; accum_H = 0;
LClip = 0; HClip = 0;

for i = 1:numel(L_out)
    Hist_L(ceil(L_out(i))+1) = Hist_L(ceil(L_out(i))+1) +1;
end

for i = 1:ceil(Max_Value) + 1
    accum_L =  accum_L + Hist_L(i);
    if(accum_L > Clip_num)
        LClip = i - 1;
        break;
    end
end

for i = 1:ceil(Max_Value) + 1
    accum_H =  accum_H + Hist_L(i);
    if(accum_H > (numel(L_out) - Clip_num))
        HClip = i - 1;
        break;
    end
end

L_out(L_out<LClip) = LClip;
L_out(L_out>HClip) = HClip;

R_out = L_out.* sqrt(sqrt(power((R_in./L_in),3)));
G_out = L_out.* sqrt(sqrt(power((G_in./L_in),3)));
B_out = L_out.* sqrt(sqrt(power((B_in./L_in),3)));

out = cat(3,R_out,G_out,B_out);
end

function [k_out] = Curve_Lut(k_in)
if(k_in<0.25)
    k_out = 0.2;
elseif(k_in>0.55)
    k_out = 0.5;
else
    k_out = 0.1*(k_in - 2.5) + 0.2;
%     k_out = k_in - 0.05;
end
end