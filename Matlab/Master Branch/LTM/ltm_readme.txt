This is the explantion of ltm (Local Tone Mapping) module, which is always occuring after the Histeq processes.
The LTM adopts here has one methods,refers to paper: "Natural HDR Image Tone Mapping Based on Retinex"
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                      %
%                                                                                                                                                                                                                         %
%             1.The LTM process is aimed at remapping the input HDR images into LDR imagse while remains the Contrast feature , there exist % 
%                one test case                                                                                                                                                                                    %
%                                                                                                                                                                                                                         %                                                                                                                                                                                                                                 %
%  File Explanation:                                                                                                                                                                                             %
%                                                                                                                                                                                                                         %
%             1. module_ltm_process.m: main process of ltm, input Histeq processed out and output the  LTM corrected  processed out            %
%                                                                                                                                                                                                                         %
%             2. ltm_test.m: test process of single ltm module, can be runned seperately beyond isp_top.m                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                             %
%                                                                                                                                                                                                                                                %
%             1. ltm_eb : 1 —— Open the ltm process , 0 ——Close the ltm process and directly output the input data                                                               %        
%                                                                                                                                                                                                                                                %                                                                                                          
%             2. module_ltm_param.s:  set the Index number of the remapping function,default is 0.75                                                                                        % 
%             3. module_ltm_param.a:  set the degree of the compression curve,default is 4                                                                                                         %                                                                                                                                                                   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%