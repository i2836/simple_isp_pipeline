%% This is the single test file of the module crop,use the test case in the directory to test the crop process.
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_crop_param = struct('LIndex',200,'RIndex',1000,'TIndex',100,'DIndex',700); %% set the remain component inthe img
crop_eb = 1;
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after the FCR+Y_EE process
fcr_out = imread('./Test Case/Case/fcr_yee_out_mode0.png');

%% Main Crop Process
crop_out = module_crop_process(fcr_out,module_crop_param,top_paramset,crop_eb);

%% Disp Result in png format
figure(1),imshow(uint8(fcr_out));
figure(2),imshow(uint8(crop_out));
imwrite(uint8(crop_out),'./Test Case/Case/crop_out.png');
