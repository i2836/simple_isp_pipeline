%% This is the Process of crop,for more detailed information,please look at the crop_readme.txt and testcase_readme.txt
%% The position of crop is always behinds the FCR+Y_EE process in the ISP pipeline
%% when crop_eb = 1, enables the crop process ,vice versa.

function [crop_out] = module_crop_process(fcr_out,module_crop_param,top_paramset,crop_eb)
if(crop_eb)
    if(top_paramset.Auto == 1)
       LIndex = 100;
       RIndex = 800;
       TIndex = 200;
       DIndex = 600;
    else
       LIndex = module_crop_param.LIndex;
       RIndex = module_crop_param.RIndex;
       TIndex = module_crop_param.TIndex;
       DIndex = module_crop_param.DIndex;
    end
       
    for k = 1:3
       crop_out(:,:,k) = fcr_out(TIndex:DIndex,LIndex:RIndex,k);
    end
 
else
    crop_out = fcr_out;    
end
end
