This is the explantion of cropmodule, which is always occuring after the FCR+Y_EE processes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation：                                                                                                                                                                                               %
%                                                                                                                                                                                                                                    %
%             1.The Crop process is aimed at adjusting the output imgsize , there exist one case                                                                                      %          
%                                                                                                                                                                                                                                    %
%  File Explanation:                                                                                                                                                                                                         %
%                                                                                                                                                                                                                                    %
%             1.module_crop_process.m: main process of crop , input the FCR+Y_EE processed images and output the  crop processed out                  %
%             2.crop_test.m: test process of single crop module, can be runned seperately beyond isp_top.m                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  param Explanation：                                                                                                                                                                                                 %
%                                                                                                                                                                                                                                    %
%             1. crop_eb : 1 —  Open the crop process ,  0 — Close the crop process and output the input data                                                             %                                                                                             
%             2. module_crop_param.LIndex.RIndex/TIndex/DIndex:  the remained index of input image                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

