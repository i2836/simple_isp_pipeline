%% This is the Process of FCR+Y_EE,for more detailed information,please look at the FCR_Y_EE_readme.txt and testcase_readme.txt
%% The position of FCR+Y_EE is always behinds the RGB2YUV process in the ISP pipeline
%% when fcr_y_ee_eb = 1, enables the FCR+Y_EE process ,vice versa.

%% This module is composed of two process : FCR(False Color Removal) and Y_EE(Sharpen), for FCR here just adopts one method, for Y_EE here adopts three methods
        %% mode 0 :FCR+USM 
        %% mode 1 :FCR+Wavelet Sharpen
        %% mode 2 :FCR+Gaussian Diff Sharprn
%% For  FCR methods adopts here,please refers to the paper:"False colors removal on the YCrCb color space"      

function [fcr_yee_out] = module_FCR_YEE_process(yuv_out,module_fcr_y_ee_param,top_paramset,fcr_y_ee_eb)
if(fcr_y_ee_eb)
    if(top_paramset.Auto == 1)
        mode = 0;
        coeff = 3;
    else
        mode = module_fcr_y_ee_param.mode;
        coeff = module_fcr_y_ee_param.coeff;
    end
   
    switch mode
        case 0 
              fcr_yee_out = FCR_USM(double(yuv_out),coeff);
        case 1
              fcr_yee_out = FCR_Wavelet(double(yuv_out),coeff);
        case 2
              fcr_yee_out = FCR_Gaussian_Diff(double(yuv_out),coeff);
        otherwise
              fcr_yee_out = FCR_USM(double(yuv_out),coeff);
    end       
else
    fcr_yee_out = yuv_out;    
end
end

function [img_out] = FCR_USM(img_in,coeff)
[he_in,wd_in,~] = size(img_in);
img_pad_in = padding_array(img_in,2,2);
G_kernel = fspecial('gaussian',[5,5],1);
G_filter_out = imfilter(img_in(:,:,1),G_kernel,'same');
U_filter_out = zeros(he_in,wd_in);V_filter_out = zeros(he_in,wd_in);

%% Y Component USM
Y_filter_out = (img_in(:,:,1) -  G_filter_out).*coeff + G_filter_out;
Y_filter_out(Y_filter_out<16)  = 16; 
Y_filter_out(Y_filter_out>235)  = 235;

%% UV component the False color 
% for i = 1:he_in
%    for j = 1:wd_in
%          temp_block = img_pad_in(i:i+4,j:j+4,:);
%          y_weight = 1./abs((temp_block(:,:,1) - temp_block(3,3,1) + 1));
%          sum_weight = sum(sum(y_weight));
%          U_filter_out(i,j) = sum(sum(y_weight.*temp_block(:,:,2)))./sum_weight; 
%          V_filter_out(i,j) = sum(sum(y_weight.*temp_block(:,:,3)))./sum_weight; 
%          P = sort(temp_block(:,:,2)); Q = sort(temp_block(:,:,3));
%          
%          if(abs(U_filter_out(i,j) - P(13))>10)
%              U_filter_out(i,j) = P(13);
%          end
%          
%          if(abs(V_filter_out(i,j) - Q(13))>10)
%              V_filter_out(i,j) = Q(13);
%          end
%    end
% end

% U_filter_out(U_filter_out<16)  = 16; U_filter_out(U_filter_out>240)  = 240; 
% V_filter_out(V_filter_out<16)  = 16; V_filter_out(V_filter_out>240)  = 240; 
for i = 1:he_in
    for j = 1:wd_in     
        U_filter_out(i,j) = median_filter(img_pad_in(i:i+4,j:j+4,2));
        V_filter_out(i,j) = median_filter(img_pad_in(i:i+4,j:j+4,3));
    end
end
% U_filter_out = img_in(:,:,2);V_filter_out = img_in(:,:,3);
img_out = cat(3,Y_filter_out,U_filter_out,V_filter_out);
end

function [img_out] = FCR_Wavelet(img_in,coeff)
[he_in,wd_in,~] = size(img_in);
img_pad_in = padding_array(img_in,2,2);
Col_H = zeros(5,4);Col_L = zeros(5,4);
Col_result = zeros(1,5);
Row_H = zeros(1,4);Row_L = zeros(1,4);

Y_filter_out = zeros(he_in,wd_in);U_filter_out = zeros(he_in,wd_in);V_filter_out = zeros(he_in,wd_in);
%% Y Component USM
for i = 1:he_in
    for j = 1:wd_in
        temp_block = img_pad_in(i:i+4,j:j+4,1);
        Thre = sum(sum(temp_block))/1024;
        for p =1:5
            for q = 1:4
                Col_H(q,p) = (temp_block (q,p) - temp_block (q + 1,p))/2;
                Col_L(q,p) = (temp_block (q,p) + temp_block (q + 1,p))/2;
                if(Col_H(q,p)>Thre)
                    Col_H(q,p) = Col_H(q,p) - Thre;
                elseif(Col_H(q,p)<-Thre)
                    Col_H(q,p) = Col_H(q,p) + Thre;
                else
                    Col_H(q,p) = 0;
                end
            end
            Col_result(p) = (Col_L(2,p)+Col_H(2,p)+Col_L(3,p)+Col_H(3,p))/2;
        end
        
        for l = 1:4
            Row_L(l) = (Col_result(l) + Col_result(l + 1))/2;
            Row_H(l) = (Col_result(l) - Col_result(l + 1))/2;
            if( Row_H(l)>Thre)
                Row_H(l) =  Row_H(l) - Thre;
            elseif(Row_H(l)<-Thre)
                Row_H(l) =  Row_H(l) + Thre;
            else
                Row_H(l) = 0;
            end
        end
        
        low_feq = (Row_L(2)+Row_H(2)+Row_L(3)+Row_H(3))/2;
        Y_filter_out(i,j) = (temp_block(3,3) - low_feq).*coeff + low_feq;
    end
end
Y_filter_out(Y_filter_out<16)  = 16; 
Y_filter_out(Y_filter_out>235)  = 235;

%% UV component the False color 
% for i = 1:he_in
%     for j = 1:wd_in
%         temp_block = img_pad_in(i:i+4,j:j+4,:);
%         y_weight = 1./(img_pad_in(i:i+4,j:j+4,1) - img_pad_in(i+2,j+2,1) + 1);
%         sum_weight = sum(sum(y_weight));
%         U_filter_out(i,j) = sum(sum(y_weight.*temp_block(:,:,2)))./sum_weight; 
%         V_filter_out(i,j) = sum(sum(y_weight.*temp_block(:,:,3)))./sum_weight; 
%     end
% end
% U_filter_out(U_filter_out<16)  = 16; U_filter_out(U_filter_out>240)  = 240; 
% V_filter_out(V_filter_out<16)  = 16; V_filter_out(V_filter_out>240)  = 240; 

for i = 1:he_in
    for j = 1:wd_in     
        U_filter_out(i,j) = median_filter(img_pad_in(i:i+4,j:j+4,2));
        V_filter_out(i,j) = median_filter(img_pad_in(i:i+4,j:j+4,3));
    end
end

% U_filter_out = img_in(:,:,2); V_filter_out = img_in(:,:,3);
img_out = cat(3,Y_filter_out,U_filter_out,V_filter_out);
end

function [img_out] = FCR_Gaussian_Diff(img_in,coeff)
%% Y Component Sharpen
[he_in,wd_in,dim] = size(img_in);
img_pad_in = padding_array(img_in,2,2);
U_filter_out = zeros(he_in,wd_in);V_filter_out = zeros(he_in,wd_in);

I_brightness=img_in;
I_bi=bifilter(I_brightness(:,:,1),0.1,5,5); %the first layer information
I_Guassian_Diff=Gaussian_Diff(I_brightness(:,:,1),0.5,1,5);%the seconde layer information
I_3=I_brightness(:,:,1)-I_bi;

out=merge(I_brightness(:,:,1),I_Guassian_Diff,2,I_3,coeff);

Y_filter_out =out +I_brightness(:,:,1);
Y_filter_out(Y_filter_out<16)  = 16; 
Y_filter_out(Y_filter_out>235)  = 235;

%% UV False Color Removal
% for i = 1:he_in
%     for j = 1:wd_in
%         temp_block = img_pad_in(i:i+4,j:j+4,:);
%         y_weight = 1./(img_pad_in(i:i+4,j:j+4,1) - img_pad_in(i+2,j+2,1) + 1);
%         sum_weight = sum(sum(y_weight));
%         U_filter_out(i,j) = sum(sum(y_weight.*temp_block(:,:,2)))./sum_weight; U_filter_out(U_filter_out<16)  = 16; U_filter_out(U_filter_out>240)  = 240; 
%         V_filter_out(i,j) = sum(sum(y_weight.*temp_block(:,:,3)))./sum_weight; V_filter_out(V_filter_out<16)  = 16; V_filter_out(V_filter_out>240)  = 240; 
%     end
% end

for i = 1:he_in
    for j = 1:wd_in     
        U_filter_out(i,j) = median_filter(img_pad_in(i:i+4,j:j+4,2));
        V_filter_out(i,j) = median_filter(img_pad_in(i:i+4,j:j+4,3));
    end
end

% U_filter_out = img_in(:,:,2); V_filter_out = img_in(:,:,3);
img_out = cat(3,Y_filter_out,U_filter_out,V_filter_out);
end

function [I_bi]=bifilter(I,g_sigma,w_sigma,r)
A=size(I);
B=padding_array(I,(r-1)/2,(r-1)/2);
I_bi=zeros(A(1),A(2));
block=zeros(r,r);
G_filter=Generate_Guassian(g_sigma,r);

for i=1:A(1)
    for j=1:A(2)
         block(1:r,1:r)=B(i:i+r-1,j:j+r-1);
         W_filter=exp(-(block-block((r-1)/2+1,(r-1)/2+1)).^2/(2*w_sigma^2)); %cal the w_weight_33
         W_filter=W_filter.*G_filter;
         I_bi(i,j)=sum(sum(block.*W_filter))/sum(sum(W_filter));
     end
 end
end

function [merge_out]=merge(original_I,I_in_1,coeff_1,I_in_2,coeff_2)
A=size(original_I);
merge_out=zeros(A(1),A(2));

for i=1:A(1)
    for j=1:A(2)
%         if (I_in_2(i,j)>original_I(i,j))&&(original_I(i,j)>I_in_1(i,j))||(I_in_2(i,j)<original_I(i,j))&&(original_I(i,j)<I_in_1(i,j))
          if (I_in_2(i,j)>I_in_1(i,j))||(I_in_2(i,j)<I_in_1(i,j))
             merge_out(i,j)=coeff_2*I_in_2(i,j)+coeff_1*I_in_1(i,j);
         else
             merge_out(i,j)=I_in_2(i,j)+I_in_1(i,j);
         end
    end
end
end

function [I_out]=Gaussian_Diff(I_in,sigma_1,sigma_2,r) %adopt 3*3 guassian prototype ,sigma2>sigma1
A=size(I_in);
B=padding_array(I_in,(r-1)/2,(r-1)/2);
Block=zeros(r,r);

I_out=zeros(A(1),A(2));
Guass_1=zeros(A(1),A(2));
Guass_2=zeros(A(1),A(2));

Filter_1=Generate_Guassian(sigma_1,r);
Filter_2=Generate_Guassian(sigma_2,r);

for i=1:A(1)
    for j=1:A(2)
         for k=1:r 
             for l=1:r
                 Block(k,l)=B(i+k-1,j+l-1);
                 Guass_1(i,j)= Guass_1(i,j)+Block(k,l)*Filter_1(k,l);
                 Guass_2(i,j)= Guass_2(i,j)+Block(k,l)*Filter_2(k,l);
             end
         end
         I_out(i,j)=Guass_1(i,j)-Guass_2(i,j);
    end
end

end

function [out]=Generate_Guassian(sigma,r) %r should be odd
out=zeros(r,r);
distance_map=zeros(r,r);

%calculation
for i=1:r
    for j=1:r
        distance_map(i,j)=sqrt((i-(r+1)/2)^2+(j-(r+1)/2)^2);
        out(i,j)=exp(-distance_map(i,j)^2/(2*sigma^2));
    end
end

sum_A=sum(sum(out));
%Normalizition
for i=1:j
    for j=1:r
      out(i,j)=out(i,j)/sum_A;
    end
end

end

function [I_out]=rgb2yuv(I)
A=size(I);
I_out=zeros(A(1),A(2),A(3));
for i=1:A(1)
    for j=1:A(2)
              I_out(i,j,1)=0.299*I(i,j,1)+0.587*I(i,j,2)+0.114*I(i,j,3);
              I_out(i,j,2)=-0.147*I(i,j,1)-0.289*I(i,j,2)+0.436*I(i,j,3);
              I_out(i,j,3)=0.615*I(i,j,1)-0.515*I(i,j,2)-0.100*I(i,j,3);
    end
end
end

function [med] = median_filter(array_in)
[h,w] = size(array_in); 
array_sort = reshape(array_in,[1,h*w]);

for i = 1:h*w - 1
    for j = i:h*w-1
        if array_sort(j)>array_sort(j+1)
            temp = array_sort(j+1);
            array_sort(j+1) = array_sort(j);
            array_sort(j) = temp;
        end
    end
end
med = array_sort((h*w+1)/2);
end