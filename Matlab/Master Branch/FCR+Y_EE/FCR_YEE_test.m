%% This is the single test file of the module FCR+Y_EE,use the test case in the directory to test the FCR_Y_EE process.
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% This module is composed of two process : FCR(False Color Removal) and Y_EE(Sharpen), for FCR here just adopts one method, for Y_EE here adopts three methods
        %% mode 0 :FCR+USM 
        %% mode 1 :FCR+Wavelet Sharpen
        %% mode 2 :FCR+Gaussian Diff Sharprn
%% For  FCR methods adopts here,please refers to the paper:"False colors removal on the YCrCb color space"      

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_fcr_y_ee_param = struct('mode',2,'coeff',5); %% mode controls the y_ee methods while the coeff control the sharpen intensity
fcr_y_ee_eb = 1;
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after RGB2YUV process
yuv_out = imread('./Test Case/Case/RGB2YUV_test_out_mode0.png');

%% Main FCR+Y_EE Process
fcr_yee_out = module_FCR_YEE_process(yuv_out,module_fcr_y_ee_param,top_paramset,fcr_y_ee_eb);

%% Disp Result in png format
figure(1),imshow(ycbcr2rgb(uint8(yuv_out)));
figure(2),imshow(ycbcr2rgb(uint8((fcr_yee_out))));
imwrite(uint8(fcr_yee_out),'./Test Case/Case/fcr_yee_out_mode2.png');
