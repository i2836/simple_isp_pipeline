This is the explantion of FCR+Y_EE module, which is always occuring after the RGB2YUV processes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation：                                                                                                                                                                                               %
%                                                                                                                                                                                                                                    %
%             1.The FCR+Y_EE process is aimed at elinamating the color artifact in the UV color component in the YUV space and sharpend the Y        %         
%                                                                                                                                                                                                                                    %
%  File Explanation:                                                                                                                                                                                                         %
%                                                                                                                                                                                                                                    %
%             1.module_FCR_YEE_process.m: main process of FCR_YEE , input the RGB2YUV processed images and output the modul  processed out   %
%             2.FCR_YEE_test.m: test process of single FCR_YEE module, can be runned seperately beyond isp_top.m                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  param Explanation：                                                                                                                                                                                                 %
%                                                                                                                                                                                                                                    %
%             1. FCR_YEE_eb : 1 —  Open the FCR_YEE process ,  0 — Close the rFCR_YEE process and output the input data                                          %                                                                                             
%             2. module_FCR_YEE_param.mode:   choose the mode of Y_EE while keeps the same FCR methods unchanged                                           % 
%                                                                      mode 0 : the USM sharpen methods of Y component                                                                          %
%                                                                      mode 1 : the Wavelet + SoftThrehold methods of Y component                                                          %
%                                                                      mode 2 : the gaussian diff filtering process sharpening of Y component                                              %
%	3. module_FCR_YEE_param.coeff:    choose the enhancement coefficient of   Sharpen Y componet.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

