%% This is the Process of RGBDenoise filter��Adopts the NLM filter in the RGB domain here��,for more detailed information,please look at the RGBdenoise_readme.txt and testcase_readme.txt
%% The position of the RGBdenoise module is always after by the Gamma process in the ISP pipeline 
%% when rgbdenoise_eb = 1, enables the RGBdenoise process ,vice versa.
%% There adopt one method to the Denoise process(NLM filter in the RGB Domain) and two methods to estimate the varicance of noise
        %% mode 0:   estimating the sigma by the block based methods,refers to paper ����"Block-based Noise Estimation Using Adaptive Filtering Gaussian "
        %% mode 1:   refers to the  paper���� ��Fast Noise Variance Estimation��
        %% sbize:    the search block width of RGB nlm filter
        %% cbize:    the comp block width of RGB nlm filter
                 
                 
function [denoised_out] = module_rgbdenoise_process(gamma_out,module_rgb_denoise_param,top_paramset,rgbdenoise_eb)
if(rgbdenoise_eb == 1) %% enables the sa process
    if(top_paramset.Auto == 1)
        mode = 1;
        sbize = 7;
        cbize = 3;
    else
        mode = module_rgb_denoise_param.mode;
        sbize = module_rgb_denoise_param.sbize;
        cbize = module_rgb_denoise_param.cbize;
    end
    
    switch mode
        case 0
            estimated_sigma =  Block_based_Noise_Estimation(double(gamma_out));
        case 1
            estimated_sigma =  Fast_Noise_Variance_Estimate(double(gamma_out));
        otherwise
            estimated_sigma =  Fast_Noise_Variance_Estimate(double(gamma_out));
    end

    denoised_out  = Matrix_Clip(NLMfilter(double(gamma_out),sbize,cbize,estimated_sigma),top_paramset.BitDepth);
else
    denoised_out = gamma_out;
end
end

function [NL_means_Image]  = NLMfilter(image_in,seb_width,cob_width,sigma)
[height,width,dim] = size(image_in);

if((sigma>0.0)&&(sigma<=25.0))
    h = 0.55*sigma;
elseif((sigma>25.0) && (sigma<=55.0))
    h = 0.40*sigma;
else
    h = 0.35*sigma;
end

hf_sch_wd = (seb_width - 1)/2;
hf_cob_wd = (cob_width - 1)/2;

for i = 1:dim
    image_in_padded(:,:,i) = padding_array(image_in(:,:,i),hf_sch_wd,hf_sch_wd);
end

for i = 1 + hf_sch_wd:height + hf_sch_wd 
    for j = 1 + hf_sch_wd:width + hf_sch_wd 
           accum_value_r = 0;accum_value_g = 0;accum_value_b = 0;
           max_weight = 0;accum_weight = 0;
           nlm_block = image_in_padded(i - hf_sch_wd:i + hf_sch_wd,j - hf_sch_wd:j + hf_sch_wd,:);                 
           for m = 1 + hf_cob_wd:seb_width - hf_cob_wd
               for n = 1 + hf_cob_wd:seb_width - hf_cob_wd
                     if((m~= 1 + hf_sch_wd) || (n~=1 + hf_sch_wd))
                         eur_distance = Eur_distance(nlm_block(m-hf_cob_wd:m+hf_cob_wd,n-hf_cob_wd:n+hf_cob_wd,:),nlm_block(1 + hf_sch_wd - hf_cob_wd:1 + hf_sch_wd + hf_cob_wd,1 + hf_sch_wd - hf_cob_wd:1 + hf_sch_wd + hf_cob_wd,:),cob_width)./(3 * (cob_width^2));
                         weight = exp(-(max(eur_distance - 2 * sigma ^2,0.0)/(h^2)));
   
                       if(weight > max_weight)
                           max_weight = weight;                          
                       end
 
                       accum_weight = accum_weight + weight;
                       accum_value_r = accum_value_r + weight.*nlm_block(m,n,1);
                       accum_value_g = accum_value_g + weight.*nlm_block(m,n,2);
                       accum_value_b = accum_value_b + weight.*nlm_block(m,n,3);
                     end 
               end
           end
           accum_weight = accum_weight + max_weight;
           accum_value_r  = accum_value_r + max_weight.*nlm_block(1 + hf_sch_wd,1 + hf_sch_wd,1);
           accum_value_g  = accum_value_g + max_weight.*nlm_block(1 + hf_sch_wd,1 + hf_sch_wd,2);
           accum_value_b  = accum_value_b + max_weight.*nlm_block(1 + hf_sch_wd,1 + hf_sch_wd,3);
                    
           if(accum_weight == 0)
               NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,1) = image_in_padded(i,j,1);
               NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,2) = image_in_padded(i,j,2);
               NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,3) = image_in_padded(i,j,3);
           else
               NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,1) = accum_value_r./accum_weight;
               NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,2) = accum_value_g./accum_weight;
               NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,3) = accum_value_b./accum_weight;
           end
           
           for k = 1:3
               if(NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,k) > 255)
                  NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,k) = 255;
               elseif(NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,k) < 0)
                  NL_means_Image(i - hf_sch_wd,j - hf_sch_wd,k) = 0;
               end
           end         
    end
end
end

function [eur_distance] = Eur_distance(center_block,compare_block,cob_width)
[height,width,dim] = size(center_block);
eur_distance = 0; 
for i = 1:cob_width
    for j = 1:cob_width
        for k =1:dim
            eur_distance =eur_distance + (center_block(i,j,k) - compare_block(i,j,k))^2;
        end
    end
end
end

function [fast_sigma_varia_es] = Fast_Noise_Variance_Estimate(image_in)
[height,width,~] = size(image_in);
Conv_1 = [1,-2,1;-2,4,-2;1,-2,1];
% Conv_1 = [0.5,-1.414,0.5;-1.414,3.656,-1.414,;0.5,-1.414,0.5];
img_in_grey = double(rgb2gray(uint8(image_in)));
img_in_grey = padarray(img_in_grey,[1,1],'symmetric');

for i = 1:height 
    for  j = 1:width         
        Intensity_map(i,j) = abs(sum(sum(img_in_grey(i:i+2,j:j+2).* Conv_1)));
    end
end

% Intensity_map = Intensity_map.*Intensity_map;
% fast_sigma_varia_es  = sum(sum(Intensity_map))/(36*(height)*(width));
fast_sigma_varia_es = sqrt(pi/2).* (sum(sum(Intensity_map)))./(6*(height)*(width));

end

% select the one block which has the miniest variance to estiamte the noise sigma
function [sigma_out] = Block_based_Noise_Estimation(image_in)
[he_in,wd_in,~] = size(image_in);
block_sigma = zeros(3,16);
temp_block = zeros(he_in/3,wd_in/16,3); 
sel_block = zeros(he_in/3,wd_in/16,3);
avg = zeros(3,16);
sigma = 10000;

for i = 1:3 
    for j = 1:16    
       temp_block = image_in(1+(i-1)*he_in/3:i*he_in/3,1+(j-1)*wd_in/16:j*wd_in/16,:);
       avg(i,j) = sum(sum(sum(temp_block)))./numel(temp_block);
       block_sigma(i,j) = sqrt(sum(sum(sum((temp_block - avg(i,j)).^2)))./(numel(temp_block)));
       if(block_sigma(i,j)<sigma)  
           sigma = block_sigma(i,j);
           sel_block = temp_block;
       end
    end
end

G_kernel = fspecial('gaussian',[5,5],sigma);
Filter_block = imfilter(sel_block,G_kernel,'same');
sigma_out = sqrt(sum(sum(sum((sel_block - Filter_block).^2)))./(numel(sel_block)));
end

