%% This is the single test file of the module RGBDenoise ,use the test case in the directory to test the RGBDenoise process.
%% The Position of RGBDenoise module in the ISP pipeline,is always after the Gamma Process in the RGB domain
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% There adopt one method to the Denoise process(NLM filter in the RGB Domain) and two methods to estimate the varicance of noise
        %% mode 0:   estimating the sigma by the block based methods,refers to paper ����"Block-based Noise Estimation Using Adaptive Filtering Gaussian "
        %% mode 1:   refers to the  paper���� ��Fast Noise Variance Estimation��
        %% sbize:    the search block width of RGB nlm filter
        %% cbize:    the comp block width of RGB nlm filter
                 
         
%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                              
module_rgb_denoise_param = struct('mode',1,'sbize',7,'cbize',3); %% modes controls the 5 noise estiamtion methods and sbize/cbize seperately the search block size and comp block size of RGB nlm filter
RGBDenoise_eb = 1;     %% enables the Saturation Adjustment process
addpath(genpath('../../'));
  
%% Read The Test Image In,always the data being processed after Gamma processed module
gamma_out = imread('./Test Case/Case one/noisy_30_sigma.png');

%% Main Process of RGBDenoise
rgbdenoised_out = module_rgbdenoise_process(gamma_out,module_rgb_denoise_param,top_paramset,RGBDenoise_eb);

%% Disp Result in png format
figure(1),imshow(uint8(gamma_out));
figure(2),imshow(uint8(rgbdenoised_out));
imwrite(uint8(rgbdenoised_out),'./Test Case/Case one/rgbdenoised_test_out_mode1.png');