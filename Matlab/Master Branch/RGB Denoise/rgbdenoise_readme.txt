This is the explantion of RGBDenoise module, which is always occuring after the Gamma processes.
The RGBDenoise module adopted here is the NLM filter in the RGB Domain，but there have two methods of estimation the noise variance factor——sigma
          %% mode 0 ：estimating the sigma by the block based methods,refers to paper ——"Block-based Noise Estimation Using Adaptive Filtering Gaussian "
          %% mode 1:   refers to the  paper—— “Fast Noise Variance Estimation”

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                                                       %
%                                                                                                                                                                                                                                                         %
%             1.The RGBDenoise process is aimed at elinamating the gaussian noise in the RGB domain, there exist only one test case                                                 %                                                                                                                                                                                   
%                                                                                                                                                                                                                                                          %                                                                                                                                                                                                                                
%  File Explanation:                                                                                                                                                                                                                               %
%                                                                                                                                                                                                                                                          %
%             1. module_rgbdenoise_process.m: main process of rgbdenoise, input gamma processed out and output the  rgbdenoise corrected  processed out     %
%                                                                                                                                                                                                                                                          %  
%             2. rgbdenoise_test.m: test process of single rgbdenoise module, can be runned seperately beyond isp_top.m                                                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                             %
%                                                                                                                                                                                                                                                %
%             1. rgbdenoise_eb : 1 —— Open the rgbdenoise process , 0 ——Close the rgbdenoise process and directly output the input data                        %        
%                                                                                                                                                                                                                                                %                                                                                                          
%             2. module_rgbdenoise_param.mode:   switch the mode of adopted gamma methods                                                                                             %  
%             3. module_rgbdenoise_param.Sbsize:  set the Search block size of RGB NLM filter                                                                                                   %
%             4. module_rgbdenoise_param.Cbsize:  set the Comp  block size of  RGB NLM filter                                                                                                 %                                                                                                                                                                                                                                                                  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%