lut = zeros(3,256);
for i= 1:3
    for j = 1:256
        if( i == 1)
          lut(i,j) = exp(-max(j^2 - 200,0)./30);
        elseif( i == 2)
          lut(i,j) = exp(-max(j^2 - 1800,0)./144);
        else
          lut(i,j) = exp(-max(j^2 - 7200,0)./441);  
        end
    end
end