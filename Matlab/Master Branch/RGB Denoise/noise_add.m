function [noisy_image_out] = noise_add(image_in,n)
[height,width,dim] = size(image_in);
noisy_image_out = clip(image_in + n.*randn(height,width,dim));
for i = 1:height
    for j = 1:width
        for k = 1:dim
            noisy_image_out(i,j,k) = clip(noisy_image_out(i,j,k));
        end
    end
end
fprintf('guassian simga2 add is %f\n',n);
end