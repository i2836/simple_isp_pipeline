function [out] = clip(pix_in)
if(pix_in > 255.0)
    out = 255.0;
elseif(pix_in<0.0)
    out = 0.0;
else
    out = pix_in;
end
end