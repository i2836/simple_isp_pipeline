This is the explantion of GB (Green Balance) module, which is always occuring after the DPC processes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation：                                                                                                                                                                          %
%                                                                                                                                                                                                               %
%             1.The GB process is aimed at Correcting the luminance difference caused by the Double-channel parallel imaging sensor    %
%                there exist one test case                                                                                                                                                         %         
%                                                                                                                                                                                                               %
%  File Explanation:                                                                                                                                                                                    %
%                                                                                                                                                                                                               %
%             1.module_gb_process.m: main process of gb, input the DPC processed out and output the GB processed out                      %
%             2.gb_test.m: test process of single gb module, can be runned seperately beyond isp_top.m                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                     %
%                                                                                                                                                                                                                                        %
%             1. gb_eb : 1 —  Open the GB process ,0 — Close the dpc process and directly output the input data                                                              %       
%                                                                                                                                                                                                                                        %                                                                                                           %
%             2. module_gb_param.bsize：the search block size to sum of  Gr-Gbs, larger  causes better GB performances but higer computional cost    %                                  %                                             
%             3. module_gb_param.Threhold：judge if needs to correct the GB difference,  served like the threholds in the Soft-threholding process       %                                                         
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%