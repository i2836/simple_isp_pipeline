%% This is the Process of GB(Green Balance),for more detailed information,please look at the GB_readme.txt and testcase_readme.txt
%% The position of the GB module is always after by the DPC process in the ISP pipeline 
%% when gb_eb = 1, enables the Green Balance process ,vice versa.

function [gb_out] = module_gb_process(dpc_out,module_gb_param,top_paramset,gb_eb)
if(gb_eb == 1)
    %% Initilize the param set and under-processed data
    if(top_paramset.Auto == 1)
        bsize = 7;
        thre = 5;
    else
        bsize = module_gb_param.bsize;
        thre = module_gb_param.Threhold;
    end
    
    [Img_Height,Img_Width] = size(dpc_out); 
    pad_width = (bsize+1)/2;
    dpc_pad_out = padding_array(dpc_out,pad_width,pad_width); %%when  bsize = 7, padding out 4 cols and 4 rows
    
    %% main GB Process for 4 kinds of Bayer Patterns
    for i = 1:Img_Height
        for j = 1:Img_Width
            switch top_paramset.Bayer
                case 'rggb'  
                     if(mod(i,2) && mod(j,2)) %R position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'R',thre);
                     elseif(mod(i+1,2) && mod(j+1,2))  %B position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'B',thre);
                     elseif(mod(i + 1,2) && mod(j,2)) %Gb position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gb',thre);
                     else  %Gb position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gr',thre);
                     end    
                case 'grbg'
                     if(mod(i,2) && mod(j,2)) %Gr position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gr',thre);
                     elseif(mod(i+1,2) && mod(j+1,2))  %Gb position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gb',thre);
                     elseif(mod(i+1,2) && mod(j,2)) %B position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'B',thre);
                     else  %R position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'R',thre);
                     end                    
                case 'gbrg' 
                     if(mod(i,2) && mod(j,2)) %Gb position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gb',thre);
                     elseif(mod(i+1,2) && mod(j+1,2))  %Gr position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gr',thre);
                     elseif(mod(i+1,2) && mod(j,2)) %R position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'R',thre);
                     else  %B position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'B',thre);
                     end                                      
                case 'bggr'
                     if(mod(i,2) && mod(j,2)) %B position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'B',thre);
                     elseif(mod(i+1,2) && mod(j+1,2))  %R position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'R',thre);
                     elseif(mod(i+1,2) && mod(j,2)) %Gr position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gr',thre);
                     else  %Gb position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gb',thre);
                     end                  
                otherwise %% the default Bayer pattern is set to rggb 
                     if(mod(i,2) && mod(j,2)) %R position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'R',thre);
                     elseif(mod(i+1,2) && mod(j+1,2))  %B position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'B',thre);
                     elseif(mod(i + 1,2) && mod(j,2)) %Gr position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gr',thre);
                     else  %Gb position
                         gb_out(i,j) = gb_intra_process(dpc_pad_out(i+1:i+bsize,j+1:j+bsize),'Gb',thre);
                     end                    
            end
        end
    end
    gb_out = Matrix_Clip(gb_out,top_paramset.BitDepth);       
else
    gb_out = dpc_out;
end
end

function [out] = gb_intra_process(block,pos,Thre)
temp_result = 0;temp_num = 0;
[he_in,wd_in] = size(block);
if(strcmp(pos,'R'))  %% when the bsize = 7, cal num :6 * 4 = 24 
    for i = 2:2:he_in -1
        for j = 3:2:wd_in-2
            temp_result = temp_result + (4 * block(i,j) - block(i-1,j-1) - block(i-1,j+1) - block(i+1,j-1) - block(i+1,j+1));
            temp_num = temp_num+4;
        end
    end 
elseif(strcmp(pos,'Gr')) %% when the bsize = 7, cal num :9 * 4 = 36 
    for i = 2:2:he_in -1
        for j = 2:2:wd_in-1
            temp_result = temp_result + (4 * block(i,j) - block(i-1,j-1) - block(i-1,j+1) - block(i+1,j-1) - block(i+1,j+1));
            temp_num = temp_num+4;
        end
    end
elseif(strcmp(pos,'Gb'))  %% when the bsize = 7, cal num :4 * 4 = 16 
    for i = 3:2:he_in -2
        for j = 3:2:wd_in-2
            temp_result = temp_result + (4 * block(i,j) - block(i-1,j-1) - block(i-1,j+1) - block(i+1,j-1) - block(i+1,j+1));
            temp_num = temp_num+4;
        end
    end
else  %% when the bsize = 7, B position cal num: 6 * 4 = 24
    for i = 3:2:he_in -2
        for j = 2:2:wd_in-1
            temp_result = temp_result + (4 * block(i,j) - block(i-1,j-1) - block(i-1,j+1) - block(i+1,j-1) - block(i+1,j+1));
            temp_num = temp_num+4;
        end
    end
end

if((temp_result/(2*temp_num))>Thre)
    if(strcmp(pos,'Gr')||strcmp(pos,'R'))
        out = block((he_in+1)/2,(wd_in+1)/2) - temp_result/(2*temp_num); 
    else
        out = block((he_in+1)/2,(wd_in+1)/2) + temp_result/(2*temp_num); 
    end
else
    out = block((he_in+1)/2,(wd_in+1)/2);
end

end