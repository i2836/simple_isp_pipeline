%% This is the single test file of the module GB(Green Balance),use the test case in the directory to test the GB process.
%% The Position of GB module in the ISP pipeline,is always after the DPC Process
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_gb_param = struct('bsize',5,'Threhold',5);  %%bsize is the the search block width of the GB module,needs to be set as a odd number
gb_eb = 1;     %% enables the GB process
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after DPC
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
dpc_out = fread(fileinptr,[top_paramset.width, top_paramset.height],'uint8');
dpc_out = dpc_out';
fclose(fileinptr);

%% Main Process
gb_out = module_gb_process(dpc_out,module_gb_param,top_paramset,gb_eb);

%% Disp Result in png format
figure(1),imshow(uint8(dpc_out));
figure(2),imshow(uint8(gb_out));
imwrite(uint8(gb_out),'./Test Case/Case one/GB_test_out_mode.png');

%% Store the Result in the Raw Foramt
path = './Test Case/Case One/';
file = 'gb_test_out_mode.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,gb_out','uint8');
fclose(fileoutptr);