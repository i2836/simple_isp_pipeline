%% This is the single test file of the module SP,use the test case in the directory to test the SP process.
%% The position of the SP module is always behinds the LSC process in the ISP pipeline
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% This is the reshows and slightly modified type of the adaptive median filters in the passage: "Salt-and-Pepper Noise Removal by Median-Type Noise Detectors and Detail-Preserving Regularization"


%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_sp_param = struct('bsize',5,'Low_Thre',5,'High_Thre',250);
sp_eb = 1;
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after LSC
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
lsc_out = fread(fileinptr,[top_paramset.width, top_paramset.height],'uint8');
lsc_out = lsc_out';
fclose(fileinptr);

%% Main SP Process
sp_out = module_sp_process(lsc_out,module_sp_param,top_paramset,sp_eb);

%% Disp Result in png format
figure(1),imshow(uint8(lsc_out));
figure(2),imshow(uint8(sp_out));
imwrite(uint8(sp_out),'./Test Case/Case Three/SP_test_out.png');

%% Store the Result in the Raw Foramt
path = './Test Case/Case Three/';
file = 'SP_test_out.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,sp_out','uint8');
fclose(fileoutptr);