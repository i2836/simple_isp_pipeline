This is the explantion of SP (Salt - Pepper Noise Removal ) module, which is always occuring after the LSC (Lens Shading Correction) processes.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                          %
%                                                                                                                                                                                                            %
%             1.The SP process is aimed at eliminating the Salt-and-Pepper Noise in the image , there exist one test case                       %         
%                                                                                                                                                                                                            %
%  File Explanation:                                                                                                                                                                                %
%                                                                                                                                                                                                            %
%             1.module_sp_process.m: main process of sp, input the lsc processed out and output the sp processed out                        %
%             2.sp_test.m: test process of single sp module, can be runned seperately beyond isp_top.m                                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                     %
%                                                                                                                                                                                                                                        %
%             1. sp_eb : 1 —— Open the sp process ,0 ——Close the sp process and directly output the input data                                                            %        
%                                                                                                                                                                                                                                        %                                                                                                           %
%             2. module_sp_param.bsize:  the maximum search block size of the Adaptive Median Filtering                                                                        % 
%             3. module_sp_param.Low_Thre:  Pepper Pixel judge threhold                                                                                                                           %
%             4. module_sp_param.High_Thre:  Salt Pixel judge threhold                                                                                                                                %                                                                                                                                         
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%