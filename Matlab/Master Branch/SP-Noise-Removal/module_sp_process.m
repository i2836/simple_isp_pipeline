%% This is the Process of SP,for more detailed information,please look at the SP_readme.txt and testcase_readme.txt
%% The position of SP is always behinds the LSC in the still ISP pipelines
%% when sp_eb = 1, enables the Salt-Pepper-Removal Process ,vice versa.

function [sp_out] = module_sp_process(lsc_out,module_sp_param,top_paramset,sp_eb)
if(sp_eb)
    %% initialize the param and prepares the image data
    if(top_paramset.Auto == 1)
        bsize = 5;
        Low_Thre = 5;
        High_Thre = 250;
    else
        bsize = module_sp_param.bsize;
        Low_Thre = module_sp_param.Low_Thre;
        High_Thre = module_sp_param.High_Thre;
    end
    
    [Img_Height,Img_Width] = size(lsc_out); 
    pad_width = (bsize - 1)/2;
    lsc_pad_out = padding_array(lsc_out,pad_width,pad_width);
    
    %% main process of SP Noise Removal, the modified adaptive median filters in the reference passages:
    %% "Salt-and-Pepper Noise Removal by Median-Type Noise Detectors and Detail-Preserving Regularization"
    for i = 1:Img_Height
        for j = 1:Img_Width
            if((lsc_pad_out(i+pad_width,j+pad_width)> High_Thre) || (lsc_pad_out(i+pad_width,j+pad_width)< Low_Thre))
                sp_out(i,j) = Modified_AMD(lsc_pad_out(i:i+2*pad_width,j:j+2*pad_width));
            else
                sp_out(i,j) = lsc_pad_out(i+pad_width,j+pad_width);
            end
        end
    end   
    sp_out = Matrix_Clip(sp_out,top_paramset.BitDepth);
else
    sp_out = lsc_out;    
end
end

%% Main Process of Modified Adaptive Median Fliter,
%% First modified: add the manual set pixel value threhold����Low_Thre/High_Thre
%% Second Modified: Cancels the arbitrary max window size and fix at 5 the max window size 
function [out] = Modified_AMD(block_in)
[he_in,wd_in] = size(block_in);

%% pre-correct for the Binear
if(abs(block_in(1,3) - block_in(5,3))>abs(block_in(3,1) - block_in(3,5)))
    block_in(3,3) = (block_in(3,1) + block_in(3,5))/2;
else
    block_in(3,3) = (block_in(1,3) + block_in(5,3))/2;
end

block = bilinear(block_in);
fetch_block = block((he_in -1)/2:(he_in +3)/2,(wd_in -1)/2:(wd_in +3)/2);
[Value,~] = sort(reshape(fetch_block,[1,9]));
MAX = Value(9); MED = Value(5); MIN = Value(1);

% for i = 1:length(Value)
%     fprintf('Value%d = %d ',i,Value(i));
% end
% fprintf('\n ');
% fprintf('max = %d, med = %d, min = %d,center = %d \n',MAX,MED,MIN,block((he_in+1)/2,(wd_in+1)/2));

%% if in 3*3 blocks, the min<med<max, removal the SP noise as follows
if((MIN<MED)&&(MED<MAX))
     if((MIN<block((he_in+1)/2,(wd_in+1)/2)) &&(block((he_in+1)/2,(wd_in+1)/2)<MAX))
          out = block((he_in+1)/2,(wd_in+1)/2); %% Good Pixels not SP Noise
     elseif(((MIN == block((he_in+1)/2,(wd_in+1)/2))) || (((MAX == block((he_in+1)/2,(wd_in+1)/2))))) %% can be excluded as the pepper and salt noise 
         out = (sum(Value) - block((he_in+1)/2,(wd_in+1)/2))/8;  
     else
          out = MED;
     end
else %% use the average value of the minimum gradient direction to removal the SP noise (5*5 Window)
    [Value,~] = sort(block((he_in -3)/2:(he_in +5)/2,(wd_in -3)/2:(wd_in +5)/2));
    out = Value(13);
end
% fprintf('pixel_in = %d, pixel_out = %d\n\n',block(3,3),out);
end