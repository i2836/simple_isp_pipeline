function [full_map_out] = bilinear(full_map_in)
[he_in,wd_in] = size(full_map_in);
full_map_out = full_map_in;
full_map_out(1:2:he_in,2:2:wd_in-1) = (full_map_in(1:2:he_in,1:2:wd_in-2) +  full_map_in(1:2:he_in,3:2:wd_in))/2;
full_map_out(2:2:he_in-1,1:2:wd_in) = (full_map_in(1:2:he_in - 2,1:2:wd_in) +  full_map_in(3:2:he_in,1:2:wd_in))/2;
full_map_out(2:2:he_in-1,2:2:wd_in-1) = (full_map_in(1:2:he_in -2,1:2:wd_in-2) + full_map_in(1:2:he_in -2,3:2:wd_in) + full_map_in(3:2:he_in ,1:2:wd_in - 2) +  full_map_in(3:2:he_in,3:2:wd_in))/4;
end