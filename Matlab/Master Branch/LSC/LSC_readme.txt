This is the explantion of LSC (Lens Shading Correction) module, which is always occuring after the DGain processes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation：                                                                                                                                                                       %
%                                                                                                                                                                                                            %
%             1.The LSC process is aimed at Correcting the Axial illumination difference of the Lens , there exist one test case                 %         
%                                                                                                                                                                                                            %
%  File Explanation:                                                                                                                                                                                %
%                                                                                                                                                                                                            %
%             1.module_lsc_process.m: main process of lsc , input the Dgain processed out and output the LSC processed out               %
%             2.lsc_test.m: test process of single lsc module, can be runned seperately beyond isp_top.m                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              
       
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                 %
%                                                                                                                                                                                                                                    %
%             1. lsc_eb : 1 —  Open the LSC process ,0 — Close the LSC process and directly output the input data                                                         %       
%                                                                                                                                                                                                                                    %            
%             2. module_lsc_param.lens_demarcate_value:  calibration parameter for the 4:3 resolution pictures captures from the camera lens             %
%                                                                                    for general here is  ones matrix for 17*13 positions                                                               %                                                                                                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%