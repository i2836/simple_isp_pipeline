%% This is the single test file of the module lsc (lens shading correction),use the test case in the directory to test the lsc process.
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 

%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                    
module_lsc_param = struct('lens_demarcate_value',ones(13,17));
lsc_eb = 1;
addpath(genpath('../../'));

%% Read The Test Image In,always the data being processed after Dgain
[file,path] = uigetfile('*.raw');
fileinptr = fopen([path,file],'r');
dgain_out = fread(fileinptr,[top_paramset.width, top_paramset.height],'uint8');
dgain_out = dgain_out';
fclose(fileinptr);

%% Main LSC Process
lsc_out = module_lsc_process(dgain_out,module_lsc_param,top_paramset,lsc_eb);

%% Disp Result in png format
figure(1),imshow(uint8(dgain_out));
figure(2),imshow(uint8(lsc_out));
imwrite(uint8(lsc_out),'./Test Case/Case/LSC_test_out.png');

%% Store the Result in the Raw Foramt
path = './Test Case/Case/';
file = 'LSC_test_out.raw';
fileoutptr = fopen([path,file],'w');
fwrite(fileoutptr,lsc_out','uint8');
fclose(fileoutptr);