%% This is the main process of LSC module(Lens Shading Correction),which is used to compensate the Lumainance diff caused by the Axial refraction difference
%% The Position of LSC module in ISP Pipeline is always after the Dgain process. 
%% lsc_eb = 1,enables the LSC process, vice versa.

function [lsc_out] = module_lsc_process(dgain_out,module_lsc_param,top_paramset,lsc_eb)
if(lsc_eb)
    if(~top_paramset.Auto)
       lsc_param = module_lsc_param.lens_demarcate_value;
    else
       lsc_param = ones(13,17); % divided the whole picture int 16*12 regions
    end
    [Img_Height,Img_Width] = size(dgain_out); 
    He_Interval = Img_Height/12; Wd_Interval = Img_Width/16;
    
    for i = 1:Img_Height
        for j = 1:Img_Width
            top = min(floor(i/He_Interval)+1,12); %top-left point in the paramset
            left = min(floor(j/Wd_Interval)+1,16);
            offset_x_left = j - (left  - 1) * Wd_Interval;
            offset_y_top =  i - (top  - 1) * He_Interval;

            Bilinear_x_intp_1 = (lsc_param(top,left+1)*(offset_x_left) + lsc_param(top,left)*(Wd_Interval - offset_x_left))./Wd_Interval;
            Bilinear_x_intp_2 = (lsc_param(top + 1,left+1)*(offset_x_left) + lsc_param(top + 1,left)*(Wd_Interval - offset_x_left))./Wd_Interval;
          
            Gain = (Bilinear_x_intp_2.*offset_y_top + Bilinear_x_intp_1.*(He_Interval - offset_y_top))./He_Interval;
            lsc_out(i,j) = dgain_out(i,j).* Gain;
        end
    end
    lsc_out = Matrix_Clip(lsc_out,top_paramset.BitDepth);
else
    lsc_out = dgain_out;    
end
end