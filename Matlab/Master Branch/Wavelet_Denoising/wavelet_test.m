%% This is the single test file of the module 2Dwavelet (RGB Domain High-Freq Sub-band filtering),use the test case in the directory to test the 2DWavelet process.
%% The Position of 2DWavelet module in the ISP pipeline,is always after the Demosaic Process
%% For the more detailed expression different test case ,please look at the read_me.txt in the test case dir. 
%% For the method of 2DWavelet algorithm and Soft-Threholding ensuring adopted here,please refers to the passage ���� "daptive Wavelet Thresholding for Image Denoising and Compression"


%% initializa the param-set
addpath(genpath('../'));
top_paramset = struct('isp_eb',1, ...,
                        'Auto',0, ...,
                        'BitDepth',8, ...,
                        'Bayer','rggb', ...,
                        'frames',2, ...,
                        'height', 1080, ...,
                        'width', 1920, ...,
                        'format','raw',...,
                        'disp',0);
                              
module_2dwavelet_param = struct('bsize',4); %%2Dbsize is the width of the 2DWavelet Decomposition block
Wavelet_eb = 1;     %% enables the 2DWavelet process
addpath(genpath('../../'));
  
%% Read The Test Image In,always the data being processed after demosaiced module
demosaic_out = imread('./Test Case/Case one/Demosaic_out_mode_0.png');
% demosaic_out = imread('./Test Case/Case one/noisy.png');

%% Main Process of RawDns: the ABF filter
Wavelet_out = module_2dwavelet_process(demosaic_out,module_2dwavelet_param,top_paramset,Wavelet_eb);

%% Disp Result in png format
figure(1),imshow(uint8(demosaic_out));
figure(2),imshow(uint8(Wavelet_out));
imwrite(uint8(Wavelet_out),'./Test Case/Case one/2DWavelet_test_out.png');
% imwrite(uint8(Wavelet_out),'./Test Case/Case one/denoised.png');