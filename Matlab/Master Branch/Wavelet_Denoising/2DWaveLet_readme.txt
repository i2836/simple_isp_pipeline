This is the explantion of 2DWavelet  module, which is always occuring after the demosaiced processes.
The Method adopts here  is the subband self-adapted Soft-Threholding Filtering that from the reference paper:“Adaptive Wavelet Thresholding for Image Denoising and Compression”

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Module Explanation:                                                                                                                                                                                     %
%                                                                                                                                                                                                                        %
%             1.The 2DWaveLet process is aimed at eliminating the High-Freq Noise unelininated or amplified by the RawDns module              %
%              （ABF filter）, there exist one test case                                                                                                                                             %         
%                                                                                                                                                                                                                        %
%  File Explanation:                                                                                                                                                                                            %
%                                                                                                                                                                                                                        %
%             1.module_2dwavelet_process.m: main process of 2dwavelet, input the demosaiced processed out and output the 2dwavelet       % 
%                                                                 processed out                                                                                                                                 %
%             2.2dwavelet_test.m: test process of single 2dwavelet module, can be runned seperately beyond isp_top.m                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

              

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Param Explanation：                                                                                                                                                                                                       %
%                                                                                                                                                                                                                                          %
%             1. 2dwavelet_eb : 1 —— Open the 2dwavelet process , 0 ——Close the 2dwavelet process and directly output the input data                        %        
%                                                                                                                                                                                                                                          %                                                                                                           %
%             2. module_2dwavelet_param.2Dbsize:  the width of 2D Wavelet decompostion block                                                                                       %                                                                                                                    %                                                                                                                                                                       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%