%% This is the Process of 2DWavelet (RGB Domain High-Feq Subband Wavelet Denoise),for more detailed information,please look at the 2DWaveLet_readme.txt and testcase_readme.txt
%% The position of the 2DWavelet module is always after by the demosaic process in the ISP pipeline 
%% when Wavelet_eb = 1, enables the Wavelet process ,vice versa.
%% For the method of 2DWavelet algorithm adopted here,please refers to the passage ���� "Adaptive Wavelet Thresholding for Image Denoising and Compression"

function [Wavelet_out] = module_2dwavelet_process(demosaic_out,module_2dwavelet_param,top_paramset,Wavelet_eb)
if(Wavelet_eb == 1)
    if(top_paramset.Auto == 1)
          Wbsize = 4;
    else
          Wbsize = module_2dwavelet_param.bsize;
    end
            
    [Img_Height,Img_Width,Dim] = size(demosaic_out);
    pad_width = Wbsize/2;
    demosaic_pad_out = double(padarray(demosaic_out,[pad_width,pad_width],'replicate'));

    for i = 1:Img_Height
        for j =1:Img_Width
            for k = 1:Dim
                Wavelet_out(i,j,k) = Wavelet2d_ST(demosaic_pad_out(i:i+Wbsize,j:j+Wbsize,k));
            end
        end
    end
    Wavelet_out = Matrix_Clip(Wavelet_out,top_paramset.BitDepth); 
else
    Wavelet_out = demosaic_out;
end
end

function [wavelet_out] = Wavelet2d_ST(block_in)
%% information and space prepare
[he_in,wd_in] = size(block_in);

Wave2d = zeros(he_in-1,wd_in-1);  %% Input a odd width block but the Wavelet result needs the even width block,so ignores the last row and column in the input block
InvWave2d = zeros(he_in-1,wd_in-1);

%% 2dwavelet decomposition ,ST and recomposition
[Wave2d,Thre] = Wavelet2dTransform(block_in);
InvWave2d = InvWavelet2dTransform(Wave2d,Thre);

%% output the center point in the block after recompostion ,finish the 2d wavelet recomposion
wavelet_out = InvWave2d(floor(he_in/2)+1,floor(wd_in/2)+1);
end

function [Wave2d,Thre] = Wavelet2dTransform(block_in)
[he_in,wd_in] = size(block_in);
temp_result = zeros(he_in-1,wd_in-1);
%% 1D Wavelet Decompostion: First in the Column Direction
for j = 1:wd_in-1  
    for i = 1:(he_in-1)/2
        temp_result(i,j) = (block_in(2*i-1,j)+block_in(2*i,j))/2;  %% cal the L feq in the col dir
        temp_result(i+(he_in - 1)/2,j) = (block_in(2*i-1,j) - block_in(2*i,j))/2; %% cal the H feq in the col dir
    end
end

%% 1D Wavelet Decompostion: Second in the Row Direction
for i = 1:he_in-1 
    for j = 1:(wd_in-1)/2
        Wave2d(i,j) = (temp_result(i,2*j-1) + temp_result(i,2*j))/2;
        Wave2d(i,j+(wd_in-1)/2) = (temp_result(i,2*j-1) - temp_result(i,2*j))/2;
    end
end

sigma_y2 = sum(sum(Wave2d((he_in-1)/2+1:he_in-1, (wd_in-1)/2+1:wd_in-1).^2))./(numel(Wave2d).^2/16);
sigma_n = median(abs(Wave2d((he_in-1)/2+1:he_in-1, (wd_in-1)/2+1:wd_in-1)))./0.6745;
sigma_n = sum(sigma_n)/numel(sigma_n);
sigma_x = sqrt(max(sigma_y2 - (sigma_n.^2),1));        
Thre = (sigma_n.^2)./sigma_x;

end

function [InvWave2d] = InvWavelet2dTransform(Wave2d,Thre)
[he_in,wd_in] = size(Wave2d);
temp_result = zeros(he_in,wd_in);
Wave2d(1+he_in/2:he_in,1+wd_in/2:wd_in) = Wavelet2dSoft(Wave2d(1+he_in/2:he_in,1+wd_in/2:wd_in),Thre); %% Soft Threhold  the Wave2ded img block
% Wave2d = Wavelet2dSoft(Wave2d,Thre);

%% 1D Wavelet Recompostion: First in the Row Direction
for i = 1:he_in 
    for j = 1:wd_in/2
         temp_result(i,2*j-1) = (Wave2d(i,j) + Wave2d(i,j+wd_in/2));
         temp_result(i,2*j)= (Wave2d(i,j) - Wave2d(i,j+wd_in/2));
    end
end

%% 1D Wavelet Recompostion: Second in the Column Direction
for j = 1:wd_in  
    for i = 1:he_in/2  
         InvWave2d(2*i-1,j) = (temp_result(i,j) + temp_result(i+wd_in/2,j));
         InvWave2d(2*i,j) = (temp_result(i,j) - temp_result(i+wd_in/2,j));
    end
end

end

function [Matrix_out] = Wavelet2dSoft(Matrix_In,thre)
[he_in,wd_in] = size(Matrix_In);

for i = 1:he_in
    for j =1:wd_in
        if(Matrix_In(i,j)>thre)
            Matrix_out(i,j) = Matrix_In(i,j) - thre;
        elseif(Matrix_In(i,j)<(-thre))
            Matrix_out(i,j) = Matrix_In(i,j) + thre;
        else
            Matrix_out(i,j) = 0;
        end
    end
end

end

