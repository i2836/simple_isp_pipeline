function [clip_out] = Single_Value_Clip(data_in,BitDepth)
if(data_in>(bitshift(1,BitDepth) - 1))
    clip_out = bitshift(1,BitDepth) - 1;
elseif(data_in < 0)
    clip_out = 0;
else
    clip_out = data_in;  
end
end