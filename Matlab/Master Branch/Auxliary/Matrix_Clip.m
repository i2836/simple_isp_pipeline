function [matrix_out] = Matrix_Clip(matrix_in,BitDepth)
matrix_in(matrix_in>(bitshift(1,BitDepth) - 1)) = bitshift(1,BitDepth) - 1;
matrix_in(matrix_in<0) = 0;
matrix_out = matrix_in;
end