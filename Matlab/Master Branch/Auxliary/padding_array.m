% % padding_col_num and padding_row_num shoule be the even number
function [padding_out] = padding_array(padding_in,col_num,row_num)
[he_in,wd_in,dim] = size(padding_in);
he_out = he_in + 2 * row_num;
wd_out = wd_in + 2 * col_num;
padding_out = zeros(he_out,wd_out,dim);


% % row padding process,now ignore the col padding process
for i = 1:he_out
    if(i<=row_num)
        padding_out(i,col_num+1:wd_in+col_num,:) = padding_in(i,1:wd_in,:);
    elseif(i>he_in+row_num)
        padding_out(i,col_num+1:wd_in+col_num,:) = padding_in(i-2*row_num,1:wd_in,:);
    else
        padding_out(i,col_num+1:wd_in+col_num,:) = padding_in(i-row_num,1:wd_in,:);
    end
end

% % col padding process, cascode process is added after the row padding process
for i = 1:he_out
    padding_out(i,1:col_num,:) = padding_out(i,1+col_num:2*col_num,:);
    padding_out(i,wd_in+col_num+1:wd_in+2*col_num,:) = padding_out(i,wd_in+1:wd_in+col_num,:); 
end

end