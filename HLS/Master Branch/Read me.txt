/*  This is the hardware implement version in hls tools related to the c model of the simpleset isp pipeline                                            
*    compared with the c models, here just choose 7 most important and representative sub module   
*    The 7 choosed repreesntative sub modules are listed as follows:
*           1. DGC (Digital Gain Correction):  contains the BLC、DGAIN and LSC process in the m files
*           2. SP (Salt and Pepper Noise Removal):  related to the SP module in the m files
*           3. Raw2RGB (Demosaic process):  related to the demosaic module in the m files
*           4. CCP (Color Correction Process):  contains the AWB、CCM and Histeq modules in the m files
*           5. RGBDNS （Denoise in the RGB Domain)：related to the RGBDNS module in the m files
*           6. RGB2YCBCR (Color Domain Transfer):  related to the RGB2YUV process in the m files
*           7. YSharpen (Y Component Sharpen): related to the Ysharpen process in the m files
*           8. isp_top:  the top function of the 7 sub module pipelined isp
*
*  Each sub module is packaged in the related folder, which always includes the solutions
*  (contains the c simulation result、syn result and co-simulation result、The Packaged IP）
*  Each choosed sub module has already been verfied and optimized
* 
*  the clock is constrained to 20ns、uncertain set is 12.5% 、target on the U250 FPGA boards
*/
