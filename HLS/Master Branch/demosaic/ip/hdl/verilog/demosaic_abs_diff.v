// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2020.1
// Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module demosaic_abs_diff (
        ap_ready,
        value1_V,
        value2_V,
        ap_return
);


output   ap_ready;
input  [7:0] value1_V;
input  [7:0] value2_V;
output  [7:0] ap_return;

wire   [0:0] icmp_ln895_fu_18_p2;
wire   [7:0] sub_ln214_fu_30_p2;
wire   [7:0] sub_ln214_1_fu_24_p2;

assign ap_ready = 1'b1;

assign ap_return = ((icmp_ln895_fu_18_p2[0:0] === 1'b1) ? sub_ln214_fu_30_p2 : sub_ln214_1_fu_24_p2);

assign icmp_ln895_fu_18_p2 = ((value1_V > value2_V) ? 1'b1 : 1'b0);

assign sub_ln214_1_fu_24_p2 = (value2_V - value1_V);

assign sub_ln214_fu_30_p2 = (value1_V - value2_V);

endmodule //demosaic_abs_diff
