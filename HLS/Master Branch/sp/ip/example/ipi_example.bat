:: ==============================================================
:: Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2020.1 (64-bit)
:: Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
:: ==============================================================

@echo off

D:/vivado/Vivado/2020.1/bin/vivado  -notrace -mode batch -source ipi_example.tcl -tclargs xcu250-figd2104-2L-e ../xilinx_com_hls_SpModuleProcess_1_0.zip
