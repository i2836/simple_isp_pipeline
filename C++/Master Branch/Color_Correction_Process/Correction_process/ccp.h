/*  CopyRight��ZhiHanZhang
*   FileName��  ccp.h
*   Description�� This the header file of module Color Correction Process (ccp) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module demosaic process: CcpParam
*                      2.class definition of module RawDns process: CcpModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/31
*/

#pragma once
#ifndef _ISP_CCP_H
#define _ISP_CCP_H

#include "common.h"

/* This is the local param of the isp module -- Color Correction Process, main contains the value of parameter:enable,ccm_matrix
		parameter description:
				1. enable: controls whether enables the ccp process
				2. ccm_matrix: controls the color correction matrix mapping from rgb 2 srgb camera domain
*/
typedef  struct
{
	bool enable;                     /* Determine whether enables the ccp process */
	bool histeq_enable;							 /* Determine whether enables the histeq process in the ccp module */
	uint8_t ccm_matrix[3][3];        /* Determine the 3*3 color correction matrix adopted to transfer from rgb to srgb domain */
}CcpParam;


/* This is the class definition of module ccp process, private param only includes the CcpParam
	 Functions includes the data read in/ data write out �� main ccp process ��data copy process ��
	 ccp_param initialize process ��AWB process��CCM  process and Histeq process 
*/

class CcpModule
{
private:
	CcpParam local_param;
protected:
	void Ccp_Param_Initial(bool enable, bool manual, bool histeq_enable, uint8_t ccm_matrix[3][3]);
public:
	CcpModule()
	{

	};
	~CcpModule()
	{

	};
	void  CcpModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, bool histeq_enable, uint8_t ccm_matrix[3][3], TopParam& Top);
	void  StatisticProcess(uint8_t* src_in, uint8_t gain[3], uint8_t mapping_matrix[3][256],TopParam& Top);
	void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};

#endif

