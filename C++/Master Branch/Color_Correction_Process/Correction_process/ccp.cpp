/*  CopyRight��ZhiHanZhang
*   FileName��    ccp.cpp
*   Description�� This the C model of module Color Correction Process (Ccp) in the simplest camera ISP pipeline
*                 The Color Correction Process (Ccp) module is the combination of 3 processes:
*                      1. r/g/b Gain and num_hist statistic process
*                      2. the lut calculation for the histeq process
*                      3. gain correction for combination of awb��ccm and histeq mapping in turn
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class CcpModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/31
*/

#include "ccp.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void CcpModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}

/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void CcpModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the ccp process is not enabled
parameter description :
1.src_in : the allocated input data space and stores the input length size data
2.dst_out : the allocated output data space and stores the output length size data
3.length : the data size length of src_in / dst_out data stream
*/
void CcpModule::CopyData(uint8_t * src_in, uint8_t * dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}

/* This Function is adopted for the ccp module local param initialzation
	 parameter description:
			1.enable: the control signal of the demosaic process, default value is 1
			2.ccm_matrix:  controls the color correction matrix mapping from rgb 2 srgb camera domain
*/
void CcpModule::Ccp_Param_Initial(bool enable, bool manual, bool histeq_enable, uint8_t ccm_matrix[3][3])
{
	memset(&local_param, 0, sizeof(CcpParam));

	if (manual)
	{
		local_param.enable = enable;
		local_param.histeq_enable = 0;
		for (uint32_t i = 0; i < 3; i++)
		{
			for (uint32_t j = 0; j < 3; j++)
			{
				local_param.ccm_matrix[i][j] = ccm_matrix[i][j];
			}
		}
	}
	else
	{
		local_param.enable = 1;
		local_param.histeq_enable = histeq_enable;
		local_param.ccm_matrix[0][0] = 1;
		local_param.ccm_matrix[0][1] = 0;
		local_param.ccm_matrix[0][2] = 0;
		local_param.ccm_matrix[1][0] = 0;
		local_param.ccm_matrix[1][1] = 1;
		local_param.ccm_matrix[1][2] = 0;
		local_param.ccm_matrix[2][0] = 0;
		local_param.ccm_matrix[2][1] = 0;
		local_param.ccm_matrix[2][2] = 1;
	}
}

/* This Function is adopted for the main process of the Ccp module
	 parameter description:
			1.src_in: the allocated input data space and stores the input length size data
			2.dst_out: the allocated output data space and stores the output length size data
			3.enable: the signal controls whether enables the ccp process
			4.ccm_matrix: controls the color correction matrix mapping from rgb 2 srgb camera domain
			5.top_param: the top param struct
*/

void  CcpModule::CcpModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, bool histeq_enable, uint8_t ccm_matrix[3][3], TopParam& Top)
{
	Ccp_Param_Initial(enable, Top.Manual, histeq_enable, ccm_matrix);
	if (local_param.enable)
	{
		uint8_t map_matrix[3][256] = { 0 };
		uint8_t gain[3] = { 0 };
		uint8_t r_in, g_in, b_in;
		uint8_t r_out, g_out, b_out;
		uint16_t temp_r, temp_g, temp_b;
		uint16_t temp_r_out, temp_g_out, temp_b_out;
		StatisticProcess(src_in, gain, map_matrix,Top);

		//printf("rgain = %d, ggain = %d, bgain = %d\n", gain[0], gain[1], gain[2]);
		for (uint32_t i = 0; i < Top.Img_Height; i++)
		{
			for (uint32_t j = 0; j < Top.Img_Width; j++)
			{
				r_in = src_in[i * 3 * Top.Img_Width + 3 * j];
				g_in = src_in[i * 3 * Top.Img_Width + 3 * j + 1];
				b_in = src_in[i * 3 * Top.Img_Width + 3 * j + 2];
				
				temp_r = ((static_cast<uint16_t>(r_in) * static_cast<uint16_t>(gain[0])) >> 6); 				
				temp_g = ((static_cast<uint16_t>(g_in) * static_cast<uint16_t>(gain[1])) >> 6); 				
				temp_b = ((static_cast<uint16_t>(b_in) * static_cast<uint16_t>(gain[2])) >> 6); 

				temp_r_out = (static_cast<uint16_t>(temp_r) * static_cast<uint16_t>(ccm_matrix[0][0]) + static_cast<uint16_t>(temp_g) * static_cast<uint16_t>(ccm_matrix[1][0]) + static_cast<uint16_t>(temp_b) * static_cast<uint16_t>(ccm_matrix[2][0]));
				temp_g_out = (static_cast<uint16_t>(temp_r) * static_cast<uint16_t>(ccm_matrix[0][1]) + static_cast<uint16_t>(temp_g) * static_cast<uint16_t>(ccm_matrix[1][1]) + static_cast<uint16_t>(temp_b) * static_cast<uint16_t>(ccm_matrix[2][1]));
				temp_b_out = (static_cast<uint16_t>(temp_r) * static_cast<uint16_t>(ccm_matrix[0][2]) + static_cast<uint16_t>(temp_g) * static_cast<uint16_t>(ccm_matrix[1][2]) + static_cast<uint16_t>(temp_b) * static_cast<uint16_t>(ccm_matrix[2][2]));

				if (local_param.histeq_enable)
				{
					 r_out = map_matrix[0][clip(temp_r_out, 0, 255)];
					 g_out = map_matrix[1][clip(temp_g_out, 0, 255)];
					 b_out = map_matrix[2][clip(temp_b_out, 0, 255)];
				}
				else
				{
					 r_out = static_cast<uint8_t>(clip(temp_r_out, 0, 255));
					 g_out = static_cast<uint8_t>(clip(temp_g_out, 0, 255));
					 b_out = static_cast<uint8_t>(clip(temp_b_out, 0, 255));
				}
				dst_out[i * 3 * Top.Img_Width + 3 * j] = r_out;
				dst_out[i * 3 * Top.Img_Width + 3 * j + 1] = g_out;
				dst_out[i * 3 * Top.Img_Width + 3 * j + 2] = b_out;

				//if ((i == 5) && (j == 5))
				//{
					//printf("b_in = %d, temp_b = %d, temp_b_out = %d, b_out = %d\n", b_in, temp_b, temp_b_out,b_out);
				//}
			}
		}
		printf("ISP ccp process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, 3 * Top.Img_Height * Top.Img_Width);
		printf("ISP ccp process is by passed!\n");
	}
}

/* This Function is adopted for the parameter statistic process in the CcpModuleProcess
*  The parameters which are later statisticed include the gain of AWB and the mappint matrix of histeq 
*  When this model comes to the related hardware implementation, this function is not includes in the hardware
*      and all these statisticed paramter will be made into preset lut 
*  parameter description:
*     1.src_in: the allocated input data space and stores the input length size data
*     2.gain: the array which is adopted to store the gain of AWB process, seperately related to r/g/b
*     3.mapping_matrix: the mapping matrix of histeq process which adopts the pdf and cdf
*     4.Top: the top control struct which include the size of input image
*/

void  CcpModule::StatisticProcess(uint8_t* src_in, uint8_t gain[3], uint8_t mapping_matrix[3][256], TopParam& Top)
{
	double r_sum = 0, g_sum = 0, b_sum = 0;
	uint32_t num[3][256] = { {0} };
	double r_gain = 0.0f, g_gain = 0.0f, b_gain = 0.0f;
	double temp_r = 0.0f, temp_g = 0.0f, temp_b = 0.0f;
	double accum_r = 0.0f, accum_g = 0.0f, accum_b = 0.0f;
	uint8_t max_r = 0, max_g = 0, max_b = 0;
	uint8_t min_r = 255, min_g = 255, min_b = 255;

	/* Implement the paramter statistic process */
	for(uint32_t i = 0; i < Top.Img_Height; i++)
	{
		for (uint32_t j = 0; j < Top.Img_Width; j++)
		{
			r_sum += src_in[3 * i * Top.Img_Width + 3 * j];
			g_sum += src_in[3 * i * Top.Img_Width + 3 * j + 1];
			b_sum += src_in[3 * i * Top.Img_Width + 3 * j + 2];
			num[0][src_in[3 * i * Top.Img_Width + 3 * j]]++;
			num[1][src_in[3 * i * Top.Img_Width + 3 * j + 1]]++;
			num[2][src_in[3 * i * Top.Img_Width + 3 * j + 2]]++;			

			if (src_in[3 * i * Top.Img_Width + 3 * j] > max_r)
			{
				max_r = src_in[3 * i * Top.Img_Width + 3 * j];
			}
			if (src_in[3 * i * Top.Img_Width + 3 * j] < min_r)
			{
				min_r = src_in[3 * i * Top.Img_Width + 3 * j];
			}

			if (src_in[3 * i * Top.Img_Width + 3 * j + 1]  > max_g)
			{
				max_g = src_in[3 * i * Top.Img_Width + 3 * j + 1];
			}
			if (src_in[3 * i * Top.Img_Width + 3 * j + 1] <  min_g)
			{
				min_g = src_in[3 * i * Top.Img_Width + 3 * j + 1];
			}

			if (src_in[3 * i * Top.Img_Width + 3 * j + 2] > max_b)
			{
				max_b = src_in[3 * i * Top.Img_Width + 3 * j + 2];
			}
			if (src_in[3 * i * Top.Img_Width + 3 * j + 2] < min_b)
			{
				min_b = src_in[3 * i * Top.Img_Width + 3 * j + 2];
			}
		}
	}

	
	/* Getting the lut according to the statistic result */
	r_gain = (r_sum + g_sum + b_sum) / (3 * r_sum);
	g_gain = (r_sum + g_sum + b_sum) / (3 * g_sum);
	b_gain = (r_sum + g_sum + b_sum) / (3 * b_sum);
	
		for (uint32_t j = 0; j < 256; j++)
		{
			temp_r = static_cast<double>(num[0][j]) / static_cast<double>(Top.Img_Height * Top.Img_Width);
			temp_g = static_cast<double>(num[1][j]) / static_cast<double>(Top.Img_Height * Top.Img_Width);
			temp_b = static_cast<double>(num[2][j]) / static_cast<double>(Top.Img_Height * Top.Img_Width);
			
			mapping_matrix[0][j] = static_cast<uint8_t>(clip(floor(accum_r * 255),min_r,max_r));
			mapping_matrix[1][j] = static_cast<uint8_t>(clip(floor(accum_g * 255),min_g,max_g));
			mapping_matrix[2][j] = static_cast<uint8_t>(clip(floor(accum_b * 255),min_b,max_b));

			accum_r += temp_r;
			accum_g += temp_g;
			accum_b += temp_b;
		}

	gain[0] = static_cast<uint8_t>(floor(r_gain * 64));
	gain[1] = static_cast<uint8_t>(floor(g_gain * 64));
	gain[2] = static_cast<uint8_t>(floor(b_gain * 64));

	
	//for (uint32_t j = 0; j < 256; j++)
	//{
		//printf("j = %d,map_r = %d, map_g = %d, map_b = %d\n", j, mapping_matrix[0][j], mapping_matrix[1][j], mapping_matrix[2][j]);
	//}	
}