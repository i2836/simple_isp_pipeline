/*  CopyRight��ZhiHanZhang
*   FileName��    rawdns.cpp
*   Description�� This the C model of module Raw Domain Denoise (RAWDNS) in the simplest camera ISP pipeline
*                 The Raw Domain Denoise (RawDns) module is the combination of 3 processes:
*                      1.Local 9*9 block fetch process: being finished by the function ���� Get_Rawdns_Block
*                      2.Calculting the LoG operaton result of the 9*9 block ,get the parameter ���� log_result
*                      3.Adjusting the parameter of BF according to log_result and BF the block,output the final result
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class RawdnsModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/29
*/

#include "rawdns.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void RawdnsModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}


/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void RawdnsModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the rawdns process is not enabled
parameter description :
1.src_in : the allocated input data space and stores the input length size data
2.dst_out : the allocated output data space and stores the output length size data
3.length : the data size length of src_in / dst_out data stream
*/
void RawdnsModule::CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}

/* This Function is adopted for the rawdns module local param initialzation
	 parameter description:
			1.enable: the control signal of the rawdns process, default value is 1
*/
void RawdnsModule::Rawdns_Param_Initial(bool enable, bool manual)
{
	memset(&local_param, 0, sizeof(RawdnsParam));

	if (manual)
	{
		local_param.enable = enable;
	}
	else
	{
		local_param.enable = 1;
	}
}

/* This Function is adopted for the rawdns module local img data block fetch process (size is 9*9)
	 Enables this function to get the 9*9 data block which is centered as the cur scan point
	 parameter description:
			1.block:  the 2D array to store the fetched 9*9 local img data region
			2.src_in: the allocated input data space and stores the input length size data
			3.cur_x:  the horizontial position index of the current scan point
			4.cur_y:  the vertical position index of the current scan point
			5.width:  the width of the input image size
 */

void  RawdnsModule::Get_Rawdns_Block(uint8_t block[9][9], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width)
{
	for (uint32_t i = 0; i < 9; i++)
	{
		for (uint32_t j = 0; j < 9; j++)
		{
			block[i][j] = src_in[(cur_y - 4 + i) * width + cur_x - 4 + j];
		}
	}
}

/* This Function is adopted for the main process of the Rawdns module 
	 parameter description:
			1.src_in: the allocated input data space and stores the input length size data
			2.dst_out: the allocated output data space and stores the output length size data
			3.enable: the signal controls whether enables the rawdns process
			4.top_param: the top param struct
*/
void RawdnsModule::RawDnsModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, TopParam& top_param)
{
	Rawdns_Param_Initial(enable, top_param.Manual);
	if (local_param.enable)
	{
		uint8_t local_block[9][9];
		int32_t log_result;
		memcpy(dst_out, src_in, sizeof(uint8_t) * top_param.Img_Height * top_param.Img_Width);

		for(uint32_t i = 4; i < top_param.Img_Height - 4; i++)
		{
			for (uint32_t j = 4; j < top_param.Img_Width - 4; j++)
			{
				Get_Rawdns_Block(local_block, src_in, j, i, top_param.Img_Width);
				//Bilinear_Intp(local_block);
				log_result = LoGOperationProcess(local_block);
				dst_out[i* top_param.Img_Width + j] = BifilterProcess(local_block, log_result,i,j);
			}
		}
		printf("ISP rawdns process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, top_param.Img_Height * top_param.Img_Width);
		printf("ISP rawdns process is bypassed!\n");
	}
}

/* This Function is adopted for the LoG operation process of the Rawdns module
	 parameter description:
			1.block_in: the local img data block centered at the current scan point 
*/
int32_t RawdnsModule::LoGOperationProcess(uint8_t block[9][9])
{
	int32_t log_operator[9][9] = { {-5,-18,-42,-65,-75,-65,-42,-18,-5},
																{-18,-57,-108,-136,-139,-136,-108,-57,-18},
																{-42,-108,-135,-38,47,-38,-135,-108,-42},
																{-65,-136,-38,365,638,365,-38,-136,-65},
																{-75,-139,47,638,1023,638,47,-139,-75},
																{-65,-136,-38,365,638,365,-38,-136,-65},
																{-42,-108,-135,-38,47,-38,-135,-108,-42},
																{-18,-57,-108,-136,-139,-136,-108,-57,-18},
																{-5,-18,-42,-65,-75,-65,-42,-18,-5} };       /* The Parameter of log_operator Should be right shift 10 bit*/
	int32_t sum = 0;
	for (uint32_t i = 0; i < 9; i++)
	{
		for (uint32_t j = 0; j < 9; j++)
		{
			sum += static_cast<int32_t>(block[i][j]) * log_operator[i][j];
		}
	}
	sum = sum >> 10;
	if (sum > 60)
	{
		sum = 60;
	}
	else if (sum < -60)
	{
		sum = -60;
	}
	else
	{
		sum = sum;
	}
	return sum;
}

/* This Function is adopted for the Bifilter operation process of the Rawdns module
	 parameter description:
			1.block_in: the local img data block centered at the current scan point
			2.log_reult: the LoG result from the function ���� LoGOperationProcess()
*/
uint8_t RawdnsModule::BifilterProcess(uint8_t block[9][9], int32_t log_result,uint32_t i, uint32_t j)
{
	/* The paramset initialzition process */
	
	/*
	uint32_t gkernel[9][9] = { {0,0,0,0,0,0,0,0,0},
														 {0,0,0,1,2,1,0,0,0},
														 {0,0,3,13,22,13,3,0,0},
														 {0,1,13,60,99,60,13,1,0},
														 {0,2,22,99,163,99,22,2,0},
														 {0,1,13,60,99,60,13,1,0},
														 {0,0,3,13,22,13,3,0,0},
		                         {0,0,0,1,2,1,0,0,0},
														 {0,0,0,0,0,0,0,0,0} };  */        /* The Parameter of Guassian Filtyer Should be right shift 10 bit*/
	
	
	uint32_t gkernel[5][5] = { {3,14,23,14,3},
															{14,61,101,61,14},
															{22,101,166,101,22},
															{14,61,101,61,14},
															{3,14,23,14,3}};
														
	int32_t  offset_r[121];
	uint8_t  range_lut[6][55] = { { 255,251,236,214,186,155,125,96,71,51,35,23,14,9,5,3,2,1,1},
		                            { 255,254,246,234,218,198,177,155,133,112,92,75,59,46,35,26,19,14,10,7,4,3,2,1,1,1},
															  { 255,255,251,245,236,226,214,200,186,171,155,140,125,110,96,83,71,60,51,42,35,28,23,18,14,11,9,7,5,4,3,2,2,1,1,1,1 },
															  { 255,255,252,247,240,231,221,209,197,183,169,155,141,127,114,101,89,78,67,58,49,42,35,29,24,19,16,13,10,8,6,5,4,3,2,2,1,1,1,1 },
															  { 255,255,253,249,244,238,230,222,212,202,191,179,167,155,143,132,120,109,98,88,79,70,61,54,47,40,35,30,25,21,18,15,12,10,8,7,6,5,4,3,2,2,2,1,1,1,1},
															  { 255,255,254,251,247,242,236,230,222,214,205,196,186,176,166,155,145,135,125,115,105,96,87,79,71,64,57,51,45,40,35,30,26,23,20,17,14,12,10,9,7,6,5,4,4,3,2,2,2,1,1,1,1,1,1 } }; 

	uint32_t value_sum = 0,weight_sum = 0;
	uint32_t  pos = log_result + 60;
	uint8_t  abs_value = 0, lut_pos = 0, range_value = 0,pixel_out;

	/* Find the best paramset according to the LoG result, 
	   the range of LoG result is limited into [-60,60] and rearranged into [0,120]
		 so the relationship between the lof result and lut is 60 offest
  */

	for (uint32_t i = 0; i < 121; i++)
	{
		if (i <= 60)
		{
			offset_r[i] = (- 60 + i) / 2;
		}
		else
		{
			offset_r[i] = (i - 60) / 3;
		}
	}



		if ((pos < 30) || (pos >= 90))
		{
			lut_pos = 0;				
		}
		else if (((pos >= 30) && (pos < 40)) || (pos >= 80) && (pos < 90))
		{
			lut_pos = 2;
		}
		else if ((pos >= 40) && (pos < 50))
		{
			lut_pos = 5;
		}
		else if (((pos >= 50) && (pos < 58)) || (pos >= 70) && (pos < 80))
		{
			lut_pos = 4;
		}
		else if ((pos >= 58) && (pos < 61))
		{
			lut_pos = 1;
		}
		else if ((pos >= 61) && (pos < 70))
		{
			lut_pos = 3;
		}
		else
		{
			lut_pos = 2;
		}

		for (uint32_t i = 0; i < 9; i+=2)
		{
			for (uint32_t j = 0; j < 9; j+=2)
			{
				range_value = clip(abs(static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[4][4]) - offset_r[pos]), 0, 54);
				value_sum += (block[i][j] * gkernel[i/2][j/2] * range_lut[lut_pos][range_value]);
				weight_sum += (gkernel[i/2][j/2] * range_lut[lut_pos][range_value]);
			}
		}
	/*
	for (uint32_t i = 0; i < 7; i++)
	{
		for (uint32_t j = 0; j < 7; j++)
		{
			range_value = clip(abs(static_cast<int32_t>(block[1 + i][1 + j]) - static_cast<int32_t>(block[4][4]) - offset_r[pos]),0,54);
			value_sum += (block[1 + i][1 + j] * gkernel[i][j] * range_lut[lut_pos][range_value]);
			weight_sum += (gkernel[i][j] * range_lut[lut_pos][range_value]) ;
		}
	}
	*/
	if (weight_sum  == 0)
	{
		pixel_out = block[4][4];
	}
	else
	{
		pixel_out = static_cast<uint8_t>(clip(value_sum / weight_sum, 0, 255));
	}
	return pixel_out;
}


/*
void  RawdnsModule::Bilinear_Intp(uint8_t block[9][9])
{
	for (uint32_t i = 0; i < 9; i += 2)
	{
		for (uint32_t j = 1; j < 9; j += 2)
		{
			block[i][j] = block[i][j - 1] / 2 + block[i][j + 1] / 2;
		}
	}

	for (uint32_t i = 0; i < 9; i += 2)
	{
		for (uint32_t j = 1; j < 9; j += 2)
		{
			block[j][i] = block[j - 1][i] / 2 + block[j + 1][i] / 2;
		}
	}

	for (uint32_t i = 1; i < 9; i += 2)
	{
		for (uint32_t j = 1; j < 9; j += 2)
		{
			block[i][j] = block[i - 1][j - 1] / 4 + block[i - 1][j + 1] / 4 + block[i + 1][j - 1] / 4 + block[i + 1][j + 1] / 4;
		}
	}
}
*/