/*  CopyRight��ZhiHanZhang
*   FileName��  rawdns.h
*   Description�� This the header file of module Raw Domain Denoise (RawDns) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module rawdns process: RawdnsParam
*                      2.class definition of module RawDns process: RawDnsModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/29
*/

#pragma once
#ifndef _ISP_RAWDNS_H
#define _ISP_RAWDNS_H

#include "common.h"

/* This is the local param of the isp module -- RawDns, main contains the value of parameter:enable,
    controls whether enables the rawdns process
*/
typedef  struct
{
	bool enable;          /* Determine whether enables the rawdns process */
}RawdnsParam;


/* This is the class definition of module rawdns process, private param only includes the RawdnsParam
	 Functions includes the data read in/ data write out �� main rawdns process ��data copy process ��
	 rawdns_param initialize process ��the block get process�� LoG operation result calculation and 
	 Bifilter process
*/

class RawdnsModule
{
private:
	RawdnsParam local_param;
protected:
	void Rawdns_Param_Initial(bool enable, bool manual);
public:
	RawdnsModule()
	{

	};
	~RawdnsModule()
	{

	};
	void  Get_Rawdns_Block(uint8_t block[9][9], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width);
	void  RawDnsModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, TopParam& Top);
	int32_t LoGOperationProcess(uint8_t block[9][9]);
	uint8_t BifilterProcess(uint8_t block[9][9], int32_t log_result, uint32_t i, uint32_t j);
	//void  Bilinear_Intp(uint8_t block[9][9]);
	void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};

#endif

