/*  CopyRight��ZhiHanZhang
*   FileName��    dpc.cpp
*   Description�� This the C model of module Defected Pixel Correction (DPC) in the simplest camera ISP pipeline
*                 The Defected Pixel Correction (DPC) module is the combination of 2 processes:
*                      1.Local 9*9 block fetch process: being finished by the function ���� Get_Dpc_Block()
*                      2.The block gradient calculation and defected judge and correction process
                         being finished by the function ���� DpcLocalProcess()
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class DpcModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/28
*/

#include "dpc.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void DpcModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}

/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void DpcModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the dpc process is not enabled
	 parameter description:
			1.src_in: the allocated input data space and stores the input length size data
			2.dst_out: the allocated output data space and stores the output length size data
			3.length: the data size length of src_in/dst_out data stream
*/
void DpcModule::CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}


/* This Function is adopted for the dpc module local param initialzation, when the manual = 1,
   adopts the outside input param to initialize the DpcParam, vice adopts the default value
	 parameter description:
			1.enable: the control signal of the dpc process, default value is 1
			2.Manual: choose the initialize methods of the dpc param value, default value is 1
			3.grad_value: the assumed gradient value threhold of the judeement process to defected pixel, default value is 40
*/
void DpcModule::Dpc_Param_Initial(bool enable, bool Manual, uint8_t grad_value1, uint8_t grad_value2)
{
	memset(&local_param, 0, sizeof(DpcParam));

if (Manual)
{
	local_param.enable = enable;
	local_param.grad_thre1 = grad_value1;
	local_param.grad_thre2 = grad_value2;
}
else
{
	local_param.enable = 1;
	local_param.grad_thre1 = 60;
	local_param.grad_thre2 = 20;
}
}


/* This Function is adopted for the dpc module local img data block fetch process (size is 5*5)
	 Enables this function to get the 5*5 data block which is centered as the cur scan point
	 parameter description:
			1.block:  the 2D array to store the fetched 5*5 local img data region
			2.src_in: the allocated input data space and stores the input length size data
			3.cur_x:  the horizontial position index of the current scan point
			4.cur_y:  the vertical position index of the current scan point
			5.width:  the width of the input image size
 */

void  DpcModule::Get_Dpc_Block(uint8_t block[5][5], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width)
{
	for (uint32_t i = 0; i < 5; i++)
	{
		for (uint32_t j = 0; j < 5; j++)
		{
			block[i][j] = src_in[(cur_y - 2 + i) * width + cur_x - 2 + j];
		}
	}
}


/* This Function is the main process of the module DPC(Defeated Pixel Correction),the whole process is as follows:
*       1.Scan the whole input img data stored in the allocted space src_in, for each scan point, operaters as follows
								1. 5*5 data block fetch. Finishing by the function ���� Get_Dpc_Block()
								2. 5*5 data block Gradient calculation��Defected Pixel judge and Correction Process
	 parameter description:
			1.src_in:       the allocated input data space and stores the input length size data
			2.dst_out:      the allocated output data space and stores the output length size data
			3.enanle:       the signal which controls whether enable the dpc process
			4.grad_value1:  Preset grad_thre1 to judge whether the current scan point is a defected point
			5.grad_value2:  Preset grad_thre2 to judge whether the current scan point is a defected point
			6.Top:          the top paramater definition
*/
void  DpcModule::DpcModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t grad_value1, uint8_t grad_value2, TopParam& Top)
{
	Dpc_Param_Initial(enable, Top.Manual, grad_value1, grad_value2);
	if (local_param.enable)
	{
		memcpy(dst_out, src_in, sizeof(uint8_t) * Top.Img_Height * Top.Img_Width);
		uint8_t local_block[5][5] = { {0} };
		for (uint32_t i = 2; i < Top.Img_Height - 2; i++)
		{
			for (uint32_t j = 2; j < Top.Img_Width - 2; j++)
			{
				Get_Dpc_Block(local_block, src_in, j, i, Top.Img_Width);
				dst_out[i * Top.Img_Width + j] = clip(DpcLocalProcess(local_block, grad_value1, grad_value2), 0, (1 << Top.BitDepth) - 1);
			}
		}
		printf("ISP dpc process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, Top.Img_Height * Top.Img_Width);
		printf("ISP dpc process is bypassed!\n");
	}
}


/* This Function is the local process of the module DPC(Defeated Pixel Correction),the whole process is as follows:
*       1.calculate the Gradient value of Horzontial/45��Degree/Vertical/135��Degree
*       2.compare the min value with the preset grad_value1 ,if lower, the cur scan point is seemed as normal
*         otherwise, it seems as the candidate of defected pixel; calculate the abs_diff_diff along the min gradient
*         direction,if the result is more than the grad_value2 , it keeps the defected point otherwise the normal point
*       3.for the candidated defected point in the step2,cal the gradient value along the min grad direction and compare
*         with the preset grad_value1
*
* 	 parameter description:
*			1.block: the 9*9 local data block
*			2.grad_value1:  Preset grad_thre1 to judge whether the current scan point is a defected point
*			3.grad_value2:  Preset grad_thre2 to judge whether the current scan point is a defected point
*/

uint8_t DpcModule::DpcLocalProcess(uint8_t block[5][5], uint8_t grad_value1, uint8_t grad_value2)
{
	uint32_t grad_v, grad_h, grad_d, grad_nd;
	uint32_t single_grad;
	uint8_t  pixel_out;

	grad_h = clip(static_cast<uint32_t>(abs_diff(block[2][0], block[2][4])) + static_cast<uint32_t>(abs_diff(block[2][1], block[2][3])),0, 255);
	grad_v = clip(static_cast<uint32_t>(abs_diff(block[0][2], block[4][2])) + static_cast<uint32_t>(abs_diff(block[1][2], block[3][2])),0, 255);
	grad_d = clip(static_cast<uint32_t>(abs_diff(block[1][3], block[3][1])) + static_cast<uint32_t>(abs_diff(block[0][4], block[4][0])),0, 255);
	grad_nd = clip(static_cast<uint32_t>(abs_diff(block[0][0], block[4][4])) + static_cast<uint32_t>(abs_diff(block[1][1], block[3][3])), 0, 255);

	if ((min(min(min(grad_h, grad_v), grad_d), grad_nd) == grad_h) && (grad_h < grad_value1))
	{
		single_grad = abs_diff(block[2][2], (block[2][0]) / 2 + (block[2][4] + 1) / 2);
		if ((single_grad > grad_value2))
		{
			pixel_out = block[2][0] / 2 + (block[2][4] + 1) / 2;
		}
		else
		{
			pixel_out = block[2][2];
		}
  }
	else if ((min(min(min(grad_h, grad_v), grad_d), grad_nd) == grad_v) && (grad_v < grad_value1))
	{
		single_grad = abs_diff(block[2][2], (block[0][2]) / 2 + (block[4][2] + 1) / 2);
		if ((single_grad > grad_value2))
		{
			pixel_out = block[0][2] / 2 + (block[4][2] + 1) / 2;
		}
		else
		{
			pixel_out = block[2][2];
		}
	}
	else if ((min(min(min(grad_h, grad_v), grad_d), grad_nd) == grad_d) && (grad_d < grad_value1))
	{
		single_grad = abs_diff(block[2][2],(block[0][4]+1)/4+(block[4][0]+1)/4+block[1][3]/4+block[3][1]/4);
		if ((single_grad > grad_value2))
		{
			pixel_out = block[0][4] / 2 + (block[4][0] + 1) / 2;
		}
		else
		{
			pixel_out = block[2][2];
		}
	}
	else if ((min(min(min(grad_h, grad_v), grad_d), grad_nd) == grad_nd) && (grad_nd < grad_value1))
	{
		single_grad = abs_diff(block[2][2], (block[0][0] + 1) / 4 + (block[4][4] + 1) / 4 + block[1][1] / 4 + block[3][3] / 4);
		if ((single_grad > grad_value2))
		{
			pixel_out = block[0][0] / 2 + (block[4][4] + 1) / 2;
		}
		else
		{
			pixel_out = block[2][2];
		}
	}
	else
	{
			pixel_out = block[2][2];
	}

	return pixel_out;
}