/*  CopyRight��ZhiHanZhang
*   FileName��  dpc.h
*   Description�� This the header file of module Defected Pixel Correction (DPC) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module dpc process: DpcParam
*                      2.class definition of module DPC process: DpcModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/28
*/

#pragma once
#ifndef _ISP_DPC_H
#define _ISP_DPC_H

#include "common.h"

/* This is the local param of the isp module -- Defected Pixel Correction, main contains the value of parameter: grad_thre, 
   which is adopted to determine whether the current scan point is a defected pixel */
typedef  struct
{
	bool enable;          /* Determine whether enables the dgc process */
	uint8_t grad_thre1;   /* Preset grad_thre1 to judge whether the current scan point is a defected point,default value is 10 */
	uint8_t grad_thre2;   /* Preset grad_thre2 to judge whether the current scan point is a defected point,default value is 60 */
}DpcParam;


/* This is the class definition of module dpc process, private param only includes the DpcParam
	 Functions includes the data read in/ data write out �� main dpc process ��data copy process ��
	 dpc_param initialize process ��the block get process�� grad calculation and correction process
*/
class DpcModule
{
private:
	DpcParam local_param;
protected:
	void Dpc_Param_Initial(bool enable, bool Manual, uint8_t grad_value1,uint8_t grad_value2);
public:
	DpcModule()
	{

	};
	~DpcModule()
	{

	};
	void  Get_Dpc_Block(uint8_t block[5][5], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width);
	void  DpcModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable,uint8_t grad_value1,uint8_t grad_value2, TopParam& Top);
	uint8_t  DpcLocalProcess(uint8_t block[5][5],uint8_t grad_value1, uint8_t grad_value2);
	void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};

#endif

