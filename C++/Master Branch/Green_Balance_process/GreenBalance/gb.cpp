/*  CopyRight��ZhiHanZhang
*   FileName��    gb.cpp
*   Description�� This the C model of module Green Balance (GB) in the simplest camera ISP pipeline
*                 The Green Balance (GB) module is the combination of 2 processes:
*                      1.Local 5*5 block fetch process: being finished by the function ���� Get_Gb_Block()
*                      2.The block average abs(Gb - Gr) value calculation and Green Balance judge and correction
                         being finished by the function ���� GbLocalProcess()
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class GbModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/28
*/

#include "gb.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void GbModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}


/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void GbModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the gb process is not enabled
parameter description :
1.src_in : the allocated input data space and stores the input length size data
2.dst_out : the allocated output data space and stores the output length size data
3.length : the data size length of src_in / dst_out data stream
*/
void GbModule::CopyData(uint8_t * src_in, uint8_t * dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}

/* This Function is adopted for the gb module local param initialzation, when the manual = 1,
	 adopts the outside input param to initialize the GbParam, vice adopts the default value
	 parameter description:
			1.enable: the control signal of the gb process, default value is 1
			2.Manual: choose the initialize methods of the gb param value, default value is 1
			3.gb_value: Preset gb_value to judge whether the current scan point should be green balance corrected 
*/
void GbModule::Gb_Param_Initial(bool enable, bool Manual, uint8_t gb_value, uint8_t thre_value)
{
	memset(&local_param, 0, sizeof(GbParam));

	if (Manual)
	{
		local_param.enable = enable;
		local_param.gb_value = gb_value;
		local_param.thre_value = thre_value;
	}
	else
	{
		local_param.enable = 1;
		local_param.gb_value = 5;
		local_param.thre_value = 20;
	}
}

/* This Function is adopted for the gb module local img data block fetch process (size is 5*5)
	 Enables this function to get the 5*5 data block which is centered as the cur scan point
	 parameter description:
			1.block:  the 2D array to store the fetched 5*5 local img data region
			2.src_in: the allocated input data space and stores the input length size data
			3.cur_x:  the horizontial position index of the current scan point
			4.cur_y:  the vertical position index of the current scan point
			5.width:  the width of the input image size
 */

void  GbModule::Get_Gb_Block(uint8_t block[5][5], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width)
{
	for (uint32_t i = 0; i < 5; i++)
	{
		for (uint32_t j = 0; j < 5; j++)
		{
			block[i][j] = src_in[(cur_y - 2 + i) * width + cur_x - 2 + j];
		}
	}
}


/* This Function is the main process of the module GB(Green Balance),the whole process is as follows:
      1.Scan the whole input img data stored in the allocted space src_in, for each scan point, operaters as follows
								1. 5*5 data block fetch. Finishing by the function ���� Get_Gb_Block()
								2. average abs difference between the Gr and Gb pairs in the 5*5 data block, and judge whether 
								   implement the Green Balance Process ,Finished by the function ���� GbLocalProcess()  
	 parameter description:
			1.src_in:       the allocated input data space and stores the input length size data
			2.dst_out:      the allocated output data space and stores the output length size data
			3.enanle:       the signal which controls whether enable the Gb noise remove process
			4.gb_value:     Preset gb_value to judge whether the current scan point should be green balance corrected
			5.Top:          the top paramater definition
*/
void  GbModule::GbModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t gb_value, uint8_t thre_value, TopParam& Top)
{
	Gb_Param_Initial(enable, Top.Manual, gb_value, thre_value);
	uint8_t type;
	if (local_param.enable)
	{
		memcpy(dst_out, src_in, sizeof(uint8_t) * Top.Img_Height * Top.Img_Width);
		uint8_t local_block[5][5] = { {0} };
		uint8_t flag;
		for (uint32_t i = 2; i < Top.Img_Height - 2; i++)
		{
			for (uint32_t j = 2; j < Top.Img_Width - 2; j++)
			{
				flag = ((i & 1) << 1) | (j & 1);
				if (Top.BayerPattern == 0)    /* rggb */
				{
					switch (flag)
					{
					  case 0: type = 0; break;       /* R */
						case 1: type = 1; break;       /* Gr */
						case 2: type = 2; break;       /* Gb */
						case 3: type = 3; break;       /* B */
						default: type = 0;						
					}		
				}
				else if (Top.BayerPattern == 1)  /* grbg*/
				{
					switch (flag)
					{
						case 0: type = 1; break;       /* Gr */
						case 1: type = 0; break;       /* R */
						case 2: type = 3; break;       /* B */
						case 3: type = 2; break;       /* Gb */
						default: type = 0;
					}
				}
				else if (Top.BayerPattern == 2)  /*gbrg*/
				{
					switch (flag)
					{
						case 0: type = 2; break;       /* Gb */
						case 1: type = 3; break;       /* B */
						case 2: type = 0; break;       /* R */
						case 3: type = 1; break;       /* Gr */
						default: type = 0;
					}
				}
				else                         /*bggr*/
				{
					switch (flag)
					{
						case 0: type = 3; break;       /* B */
						case 1: type = 2; break;       /* Gb */
						case 2: type = 1; break;       /* Gr */
						case 3: type = 0; break;       /* R */
						default: type = 0;
					}
				}
				Get_Gb_Block(local_block, src_in, j, i, Top.Img_Width);
				dst_out[i * Top.Img_Width + j] = clip(GbLocalProcess(local_block, gb_value, thre_value,type), 0, (1 << Top.BitDepth) - 1);
			}
		}
		printf("ISP gb process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, Top.Img_Height * Top.Img_Width);
		printf("ISP gb process is bypassed!\n");
	}
}

/* This Function is the local process of the module GB(Green Balance),the whole process is as follows:
*       1.calculate the average value of abs difference between all the available Gr and Gb neightbor pairs in the 5*5 block
*       2.compare the calculation result of stpe 1 and judge whether enables the Green Balance Process 
*
* 	 parameter description:
*			1.block:     the 5*5 local data block
*			2.gb_value:  Preset gb_value to judge whether the current scan point should be green balance corrected
*			3.type:      represent the cfa type of current scan point, 0/1/2/3 seperately represents the cfa type R/Gr/Gb/B
*/
int32_t  GbModule::GbLocalProcess(uint8_t block[5][5], uint8_t gb_value, uint8_t thre_value, uint8_t type)
{
	int32_t diff_sum = 0;
	int32_t gb_result = 0;
	switch (type)
	{
			case 0:  
				for (uint32_t i = 1; i < 5; i += 2)
				{
					for (uint32_t j = 0; j <= 4; j += 2)
					{
						if (j == 0)
						{
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j + 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j + 1]);
						}
						else if (j == 2)
						{
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j + 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j + 1]);
						}
						else
						{
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j - 1]);
						}
					}
				}
				break;
			case 1:
				for (uint32_t i = 1; i < 5; i += 2)
				{
					for (uint32_t j = 1; j < 5; j += 2)
					{
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j + 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j + 1]);
					}
				}
				break;
			case 2:
				for (uint32_t i = 1; i < 5; i += 2)
				{
					for (uint32_t j = 1; j < 5; j += 2)
					{
							diff_sum += static_cast<int32_t>(block[i - 1][j - 1]) - static_cast<int32_t>(block[i][j]);
							diff_sum += static_cast<int32_t>(block[i - 1][j + 1]) - static_cast<int32_t>(block[i][j]);
							diff_sum += static_cast<int32_t>(block[i + 1][j - 1]) - static_cast<int32_t>(block[i][j]);
							diff_sum += static_cast<int32_t>(block[i + 1][j + 1]) - static_cast<int32_t>(block[i][j]);
					}
				}
				break;
			case 3:
				for (uint32_t i = 0; i < 5; i += 2)
				{
					for (uint32_t j = 1; j <5; j += 2)
					{
						if (i == 0)
						{
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j + 1]);
						}
						else if (i == 2)
						{
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j + 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i + 1][j + 1]);
						}
						else
						{
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j - 1]);
							diff_sum += static_cast<int32_t>(block[i][j]) - static_cast<int32_t>(block[i - 1][j + 1]);
						}
					}
				}
				break;
			default:diff_sum = 0;																
	}
	gb_result = diff_sum >> 5;

	if ((abs(gb_result) > gb_value) && (abs(gb_result) < thre_value))
	{
		if ((type == 2) || (type == 3)) /*B or Gb*/
		{
			return static_cast<int32_t>(block[2][2]) - gb_result;
		}
		else /*R or Gr*/
		{
			return static_cast<int32_t>(block[2][2]) + gb_result;
		}	
	}
	else
	{
		return static_cast<int32_t>(block[2][2]);
	}
}