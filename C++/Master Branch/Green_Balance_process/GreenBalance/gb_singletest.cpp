/*  CopyRight��ZhiHanZhang
*   FileName��  gb_singletest.cpp
*   Description�� This the Test file of module Green Balance process (GB) in the simplest camera ISP pipeline
*                 This file contains the following content:
*                      1.TopParam initial
*                      2.The input/output data space allocation and read/write
*                      3.Test the GB process
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/28
*/

#include "common.h"
#include "gb.h"

int main(int argc, char** argv)
{
	/* Top param��class and FILE pointer initialization */
	TopParam t1;
	GbModule gb;
	FILE* fp1, * fp2;
	errno_t err_1, err_2;

	if (argc != 6)
		printf("Error! Not input required number of paramter!\n");

	//fp1 = fopen(argc[0], "rb");
	//fp2 = fopen(argc[1], "wb");
	err_1 = fopen_s(&fp1, argv[1], "rb");
	err_2 = fopen_s(&fp2, argv[2], "wb");

	memset(&t1, 0, sizeof(TopParam));

	t1.BitDepth = 8;
	t1.BayerPattern = 0;
	t1.Enable = 1;
	t1.Img_Height = 1080;
	t1.Img_Width = 1920;
	t1.Manual = 1;

	/* In/Out data space allocation */
	uint8_t* src_in = (uint8_t*)malloc(sizeof(uint8_t) * t1.Img_Height * t1.Img_Width);
	uint8_t* dst_out = (uint8_t*)malloc(sizeof(uint8_t) * t1.Img_Height * t1.Img_Width);


	/* Data input ��Main GB process and Data output */
	gb.ImgReadin(fp1, t1.Img_Height * t1.Img_Width, src_in);
	gb.GbModuleProcess(src_in, dst_out, atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), t1);
	gb.ImgWriteout(fp2, t1.Img_Height * t1.Img_Width, dst_out);


	free(src_in);
	free(dst_out);
	printf("ISP GB process finish!\n");
	return 0;
}