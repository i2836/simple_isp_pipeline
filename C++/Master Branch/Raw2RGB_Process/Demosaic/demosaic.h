/*  CopyRight��ZhiHanZhang
*   FileName��  demosaic.h
*   Description�� This the header file of module Raw2RGB Process (Demosaic) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module demosaic process: DemosaicParam
*                      2.class definition of module RawDns process: DemosaicModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/30
*/

#pragma once
#ifndef _ISP_DEMOSAIC_H
#define _ISP_DEMOSAIC_H

#include "common.h"

/* This is the local param of the isp module -- Demosaic, main contains the value of parameter:enable,ratio
		parameter description: 
				1. enable: controls whether enables the demosaicf process
				2. ratio : controls the type judge(Sharpen/Flat) in the local image block  
*/
typedef  struct
{
	bool enable;          /* Determine whether enables the Demosaic process */
	uint8_t ratio;        /* Determine the judge the type of the local image block according too the  ratio value */
}DemosaicParam;


/* This is the class definition of module demosaic process, private param only includes the DemosaicParam
	 Functions includes the data read in/ data write out �� main demosaic process ��data copy process ��
	 demosaic_param initialize process ��the block fetch process�� G intp process and RB intp process
*/

class DemosaicModule
{
private:
	DemosaicParam local_param;
protected:
	void Demosaic_Param_Initial(bool enable, bool manual,uint8_t ratio_value);
public:
	DemosaicModule()
	{

	};
	~DemosaicModule()
	{

	};
	void  Get_Demosaic_Block(uint8_t block[11][11], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width);
	void  DemosaicModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t ratio_value, TopParam& Top);
	uint8_t GintpProcess(uint8_t block[11][11],uint8_t center_x, uint8_t center_y,uint8_t ratio_value, uint32_t row, uint32_t col);
	void LocalintpProcess(uint8_t block[11][11],uint8_t ratio_value, uint8_t pos_flag_r, uint8_t pos_flag_gr, uint8_t  pos_flag_gb, 
		                    uint8_t pos_flag_b, uint8_t rgb_out[3], uint32_t row, uint32_t col);
	void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};

#endif


