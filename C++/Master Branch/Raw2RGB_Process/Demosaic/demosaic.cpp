/*  CopyRight��ZhiHanZhang
*   FileName��    demosaic.cpp
*   Description�� This the C model of module Raw2RGB Process (Demosaic) in the simplest camera ISP pipeline
*                 The Raw2RGB Process (Demosaic) module is the combination of 3 processes:
*                      1.Local 11*11 block fetch process: being finished by the function ���� Get_Demosaic_Block()
*                      2.G intp for the Current Scan Point and neightbor point for RB intp process
*                      3.RB Intp Process
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class DemosaicModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/30
*/

#include "demosaic.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void DemosaicModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}


/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void DemosaicModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}


/* This Function is adopted for the input img data directly write out when the demosaic process is not enabled
parameter description :
1.src_in : the allocated input data space and stores the input length size data
2.dst_out : the allocated output data space and stores the output length size data
3.length : the data size length of src_in / dst_out data stream
*/
void DemosaicModule::CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}


/* This Function is adopted for the demosaic module local param initialzation
	 parameter description:
			1.enable: the control signal of the demosaic process, default value is 1
			2.ratio_value:  Determine the judge the type of the local image block according too the  ratio value
*/
void DemosaicModule::Demosaic_Param_Initial(bool enable, bool manual,uint8_t ratio_value)
{
	memset(&local_param, 0, sizeof(DemosaicParam));

	if (manual)
	{
		local_param.enable = enable;
		local_param.ratio = ratio_value;
	}
	else
	{
		local_param.enable = 1;
		local_param.ratio = 2;
	}
}


/* This Function is adopted for the demosaic module local img data block fetch process (size is 11*11)
	 Enables this function to get the 11*11 raw data block which is centered as the cur scan point
	 parameter description:
			1.block:  the 2D array to store the fetched 11*11 local raw img data region
			2.src_in: the allocated input data space and stores the input length size data
			3.cur_x:  the horizontial position index of the current scan point
			4.cur_y:  the vertical position index of the current scan point
			5.width:  the width of the input image size
 */
void  DemosaicModule::Get_Demosaic_Block(uint8_t block[11][11], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width)
{
	for (uint32_t i = 0; i < 11; i++)
	{
		for (uint32_t j = 0; j < 11; j++)
		{
			block[i][j] = src_in[(cur_y - 5 + i) * width + cur_x - 5 + j];
		}
	}
}


/* This Function is adopted for the main process of the Demosaic module
	 parameter description:
			1.src_in: the allocated input data space and stores the input length size data
			2.dst_out: the allocated output data space and stores the output length size data
			3.enable: the signal controls whether enables the Demosaic process
			4.ratio_value: Determine the judge the type of the local image block according too the  ratio value
			5.top_param: the top param struct
*/
void  DemosaicModule::DemosaicModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t ratio_value, TopParam& Top)
{
	Demosaic_Param_Initial(enable, Top.Manual, ratio_value);
	if (local_param.enable)
	{
		uint8_t local_block[11][11];
		uint8_t pos_flag_y, pos_flag_x;
		uint8_t pos_flag_r, pos_flag_gr, pos_flag_gb, pos_flag_b;
		uint8_t rgb_out[3];
		memset(dst_out, 0, sizeof(uint8_t) * 3 * Top.Img_Height * Top.Img_Width);

		for (uint32_t i = 5; i < Top.Img_Height - 5; i++)
		{
			for (uint32_t j = 5; j < Top.Img_Width - 5; j++)
			{
				
				pos_flag_y = static_cast<uint8_t>(i & 1);
				pos_flag_x = static_cast<uint8_t>(j & 1);
		

				switch (Top.BayerPattern)
				{
						case 0:
							pos_flag_r  = (pos_flag_y == 0) && (pos_flag_x == 0);
							pos_flag_gr = (pos_flag_y == 0) && (pos_flag_x == 1);
							pos_flag_gb = (pos_flag_y == 1) && (pos_flag_x == 0);
							pos_flag_b =  (pos_flag_y == 1) && (pos_flag_x == 1);
							break;
						case 1:
							pos_flag_r  = (pos_flag_y == 0) && (pos_flag_x == 1);
							pos_flag_gr = (pos_flag_y == 0) && (pos_flag_x == 0);
							pos_flag_gb = (pos_flag_y == 1) && (pos_flag_x == 1);
							pos_flag_b =  (pos_flag_y == 1) && (pos_flag_x == 0);
							break;
						case 2:
							pos_flag_r  = (pos_flag_y == 1) && (pos_flag_x == 0);
							pos_flag_gr = (pos_flag_y == 1) && (pos_flag_x == 1);
							pos_flag_gb = (pos_flag_y == 0) && (pos_flag_x == 0);
							pos_flag_b  = (pos_flag_y == 0) && (pos_flag_x == 1);
							break;
						case 3:
							pos_flag_r  = (pos_flag_y == 1) && (pos_flag_x == 1);
							pos_flag_gr = (pos_flag_y == 1) && (pos_flag_x == 0);
							pos_flag_gb = (pos_flag_y == 0) && (pos_flag_x == 1);
							pos_flag_b  = (pos_flag_y == 0) && (pos_flag_x == 0);
							break;
						default:                                                      
							pos_flag_r = (pos_flag_y == 0) && (pos_flag_x == 0);
							pos_flag_gr = (pos_flag_y == 0) && (pos_flag_x == 1);
							pos_flag_gb = (pos_flag_y == 1) && (pos_flag_x == 0);
							pos_flag_b = (pos_flag_y == 1) && (pos_flag_x == 1);
				}
			
				Get_Demosaic_Block(local_block, src_in, j, i, Top.Img_Width);

				/*
				if ((i == 5) && (j == 5))
				{
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[0][0], local_block[0][1], local_block[0][2], local_block[0][3], local_block[0][4], local_block[0][5], local_block[0][6], local_block[0][7], local_block[0][8], local_block[0][9], local_block[0][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[1][0], local_block[1][1], local_block[1][2], local_block[1][3], local_block[1][4], local_block[1][5], local_block[1][6], local_block[1][7], local_block[1][8], local_block[1][9], local_block[1][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[2][0], local_block[2][1], local_block[2][2], local_block[2][3], local_block[2][4], local_block[2][5], local_block[2][6], local_block[2][7], local_block[2][8], local_block[2][9], local_block[2][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[3][0], local_block[3][1], local_block[3][2], local_block[3][3], local_block[3][4], local_block[3][5], local_block[3][6], local_block[3][7], local_block[3][8], local_block[3][9], local_block[3][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[4][0], local_block[4][1], local_block[4][2], local_block[4][3], local_block[4][4], local_block[4][5], local_block[4][6], local_block[4][7], local_block[4][8], local_block[4][9], local_block[4][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[5][0], local_block[5][1], local_block[5][2], local_block[5][3], local_block[5][4], local_block[5][5], local_block[5][6], local_block[5][7], local_block[5][8], local_block[5][9], local_block[5][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[6][0], local_block[6][1], local_block[6][2], local_block[6][3], local_block[6][4], local_block[6][5], local_block[6][6], local_block[6][7], local_block[6][8], local_block[6][9], local_block[6][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[7][0], local_block[7][1], local_block[7][2], local_block[7][3], local_block[7][4], local_block[7][5], local_block[7][6], local_block[7][7], local_block[7][8], local_block[7][9], local_block[7][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[8][0], local_block[8][1], local_block[8][2], local_block[8][3], local_block[8][4], local_block[8][5], local_block[8][6], local_block[8][7], local_block[8][8], local_block[8][9], local_block[8][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[9][0], local_block[9][1], local_block[9][2], local_block[9][3], local_block[9][4], local_block[9][5], local_block[9][6], local_block[9][7], local_block[9][8], local_block[9][9], local_block[9][10]);
					printf("%d %d %d %d %d %d %d %d %d %d %d\n", local_block[10][0], local_block[10][1], local_block[10][2], local_block[10][3], local_block[10][4], local_block[10][5], local_block[10][6], local_block[10][7], local_block[10][8], local_block[10][9], local_block[10][10]);
				}
				*/

				LocalintpProcess(local_block, local_param.ratio, pos_flag_r, pos_flag_gr, pos_flag_gb, pos_flag_b, rgb_out,i,j);
				dst_out[i * Top.Img_Width * 3 + 3 * j] = rgb_out[0];
				dst_out[i * Top.Img_Width * 3 + 3 * j + 1] = rgb_out[1];
				dst_out[i * Top.Img_Width * 3 + 3 * j + 2] = rgb_out[2];
				
				//printf("in = %d ,r = %d, g = %d, b  = %d \n", src_in[i* Top.Img_Width + j], dst_out[i * Top.Img_Width * 3 + 3 * j], dst_out[i * Top.Img_Width * 3 + 3 * j + 1], dst_out[i * Top.Img_Width * 3 + 3 * j + 2]);
			}
		}
		printf("ISP demosaic process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, Top.Img_Height* Top.Img_Width);
		printf("ISP demosaic process is bypassed!\n");
	}
}

/* This Function is adopted for the local Demosaic process in the local fetched 11*11 data region
*  In the local fetched data region ,the demosaic process contains two process(in turn)
*     1.Intp the missing g and neight g in each seperate 9*9 data region
*     2.Intp the missing r and b component of the current scan point
	 parameter description:
			1.local_block:         the 2D array to store the fetched 11*11 local raw img data region
			2.ratio:               determine the judge the type of the local image block according too the  ratio value
			3.pos_flag_r/gr/gb/b:  the signal controls whether seperately resembles the cur scan point type
			4.rgb_out:             stores the output full intped r/g/b value
*/
void DemosaicModule::LocalintpProcess(uint8_t local_block[11][11], uint8_t ratio,uint8_t  pos_flag_r, uint8_t pos_flag_gr, uint8_t pos_flag_gb, uint8_t pos_flag_b, uint8_t rgb_out[3] , uint32_t row, uint32_t col)
{
	int16_t temp_g[4] = { 0 };
	int16_t temp;
	if (pos_flag_r)
	{
		rgb_out[0] = local_block[5][5];
		rgb_out[1] = GintpProcess(local_block, 5, 5, ratio,row,col);
		temp_g [0] = static_cast<int16_t>(GintpProcess(local_block, 4, 4, ratio, row, col));
		temp_g [1] = static_cast<int16_t>(GintpProcess(local_block, 4, 6, ratio, row, col));
		temp_g [2] = static_cast<int16_t>(GintpProcess(local_block, 6, 4, ratio, row, col));
		temp_g [3] = static_cast<int16_t>(GintpProcess(local_block, 6, 6, ratio, row, col));

		temp  =  static_cast<int16_t>(local_block[4][4]/4 + local_block[4][6]/4 + local_block[6][4]/4 + local_block[6][6]/4) 
			       - (temp_g[0] / 4 + temp_g[1] / 4 + temp_g[2] / 4 + temp_g[3] / 4);
		rgb_out[2] = static_cast<uint8_t>(clip(temp + static_cast<int16_t>(rgb_out[1]), 0, 255));
	}
	else if (pos_flag_gr)
	{
		temp_g [0] = static_cast<int16_t>(GintpProcess(local_block, 5, 4, ratio, row, col));
		temp_g [1] = static_cast<int16_t>(GintpProcess(local_block, 5, 6, ratio, row, col));
		temp_g [2] = static_cast<int16_t>(GintpProcess(local_block, 4, 5, ratio, row, col));
		temp_g [3] = static_cast<int16_t>(GintpProcess(local_block, 6, 5, ratio, row, col));

		temp = static_cast<int16_t>(local_block[5][4] / 2 + local_block[5][6] / 2 )- (temp_g[0] / 2 + temp_g[1] / 2);
		rgb_out[0] = static_cast<uint8_t>(clip(temp + static_cast<int16_t>(local_block[5][5]), 0, 255));
		rgb_out[1] = local_block[5][5];
		temp = static_cast<int16_t>(local_block[4][5] / 2 + local_block[6][5] / 2) - (temp_g[2] / 2 + temp_g[3] / 2);
		rgb_out[2] = static_cast<uint8_t>(clip(temp + static_cast<int16_t>(local_block[5][5]), 0, 255));
	}
	else if (pos_flag_gb)
	{
		temp_g[0] = static_cast<int16_t>(GintpProcess(local_block, 4, 5, ratio, row, col));
		temp_g[1] = static_cast<int16_t>(GintpProcess(local_block, 6, 5, ratio, row, col));
		temp_g[2] = static_cast<int16_t>(GintpProcess(local_block, 5, 4, ratio, row, col));
		temp_g[3] = static_cast<int16_t>(GintpProcess(local_block, 5, 6, ratio, row, col));

		temp = static_cast<int16_t>(local_block[4][5] / 2 + local_block[6][5] / 2) - (temp_g[0] / 2 + temp_g[1] / 2);
		rgb_out[0] = static_cast<uint8_t>(clip(temp + static_cast<int16_t>(local_block[5][5]), 0, 255));
		rgb_out[1] = local_block[5][5];
		temp = static_cast<int16_t>(local_block[5][4] / 2 + local_block[5][6] / 2) - (temp_g[2] / 2 + temp_g[3] / 2);
		rgb_out[2] = static_cast<uint8_t>(clip(temp + static_cast<int16_t>(local_block[5][5]), 0, 255));
	}
	else
	{
		rgb_out[2] = local_block[5][5];
		rgb_out[1] = GintpProcess(local_block, 5, 5, ratio, row, col);
		temp_g[0] =  static_cast<int16_t>(GintpProcess(local_block, 4, 4, ratio, row, col));
		temp_g[1] =  static_cast<int16_t>(GintpProcess(local_block, 4, 6, ratio, row, col));
		temp_g[2] =  static_cast<int16_t>(GintpProcess(local_block, 6, 4, ratio, row, col));
		temp_g[3] =  static_cast<int16_t>(GintpProcess(local_block, 6, 6, ratio, row, col));

		temp = static_cast<int16_t>(local_block[4][4] / 4 + local_block[4][6] / 4 + local_block[6][4] / 4 + local_block[6][6] / 4)
			- (temp_g[0] / 4 + temp_g[1] / 4 + temp_g[2] / 4 + temp_g[3] / 4);
		rgb_out[0] = static_cast<uint8_t>(clip(temp + static_cast<int16_t>(rgb_out[1]), 0, 255));
	}	
	if ((row == 6) && (col == 6))
	{
		printf("pos_flag_r = %d, pos_flag_gr = %d, pos_flag_gb = %d, pos_flag_b = %d\n", pos_flag_r, pos_flag_gr, pos_flag_gb, pos_flag_b);
		printf("rgb_out[1] = %d, temp_g[0] = %d, temp_g[1] = %d, temp_g[2]= %d, temp_g[3] = %d\n", rgb_out[1],temp_g[0], temp_g[1], temp_g[2], temp_g[3]);
	}
}


/* This Function is adopted for the G component intp of the local Demosaic process in the local fetched 11*11 data region
*  In the local fetched data region ,the G intp process contains two process(in turn)
*     1.judge the type of input local data block region according to the ratio_value of H/V gradient: sharp or flat
*     2.Seperately intp the G component according to the type of local data block
	 parameter description:
			1.local_block:   the 2D array to store the fetched 11*11 local raw img data region,only used 9*9 for single pos g intp			
			2.center_x:      the center in x direction of 9*9 data block(single G intp need) in the fetched 11*11 region
			3.center_y:      the center in y direction of 9*9 data block(single G intp need) in the fetched 11*11 region
			4.ratio:         determine the judge the type of the local image block according too the  ratio value
*/

uint8_t DemosaicModule::GintpProcess(uint8_t block[11][11], uint8_t center_x, uint8_t center_y, uint8_t ratio_value, uint32_t row, uint32_t col)
{
	uint16_t lh = 0, lv = 0;
	uint16_t grad_h = 0, grad_v = 0, grad_d = 0;
	int16_t   temp, temp_h, temp_v;
	uint8_t local_block[9][9];
	uint8_t g_out;

	for (uint32_t i = 0; i < 9; i++)
	{
		for (uint32_t j = 0; j < 9; j++)
		{
			local_block[i][j] = block[center_y - 4 + i][center_x - 4 + j];
		}
	}

	for (uint32_t i = 0; i < 5; i++)
	{
		lh += static_cast<uint16_t>(abs_diff(block[center_y - 2 + i][4], block[center_y - 2 + i][2])) + static_cast<uint16_t>(abs_diff(block[center_y - 2 + i][4], block[center_y - 2 + i][6]));
		lh += static_cast<uint16_t>(abs_diff(block[center_y - 2 + i][4], block[center_y - 2 + i][3])) + static_cast<uint16_t>(abs_diff(block[center_y - 2 + i][4], block[center_y - 2 + i][5]));

		lv += static_cast<uint16_t>(abs_diff(block[4][center_x - 2 + i], block[2][center_x - 2 + i])) + static_cast<uint16_t>(abs_diff(block[4][center_x - 2 + i], block[6][center_x - 2 + i]));
		lv += static_cast<uint16_t>(abs_diff(block[4][center_x - 2 + i], block[3][center_x - 2 + i])) + static_cast<uint16_t>(abs_diff(block[4][center_x - 2 + i], block[5][center_x - 2 + i]));
	}

	if (lv > (ratio_value * lh))
	{
		temp = static_cast<int16_t>(local_block[4][4]) / 2 - static_cast<int16_t>(local_block[4][2] / 4 + local_block[4][6] / 4);
		g_out = clip(static_cast<int16_t>(local_block[4][3] / 2 + local_block[4][5] / 2) + temp, 0, 255);
		//printf("value_1 = %d, value_2 = %d, temp =%d\n", static_cast<int16_t>(local_block[4][4]) / 2, static_cast<int16_t>(local_block[4][2] / 4 + local_block[4][6] / 4), temp);
		//printf("G_out = %d\n", g_out);
	}
	else if (lh > (ratio_value * lv))
	{
		temp = static_cast<int16_t>(local_block[4][4]) / 2 - static_cast<int16_t>(local_block[2][4] / 4 + local_block[6][4] / 4);
		g_out = clip(static_cast<int16_t>(local_block[3][4] / 2 + local_block[5][4] / 2) + temp, 0, 255);
	}
	else
	{
		for (uint32_t i = 0; i < 6; i += 2)
		{
			temp_h = static_cast<int16_t>(local_block[4][2 + i] / 2) - static_cast<int16_t>(local_block[4][i] / 4 + local_block[4][i + 4] / 4);
			temp_v = static_cast<int16_t>(local_block[2 + i][4] / 2) - static_cast<int16_t>(local_block[i][4] / 4 + local_block[i + 4][4] / 4);
			switch (i)
			{
			case 0:
				grad_h += 3 * clip(abs(static_cast<int16_t>(local_block[4][2 + i]) - static_cast<int16_t>(local_block[4][1 + i] / 2 + local_block[4][3 + i] / 2) - temp_h), 0, 255) / 2;
				grad_v += 3 * clip(abs(static_cast<int16_t>(local_block[2 + i][4]) - static_cast<int16_t>(local_block[1 + i][4] / 2 + local_block[3 + i][4] / 2) - temp_v), 0, 255) / 2;
				break;
			case 2:
				grad_h += 2 * clip(abs(static_cast<int16_t>(local_block[4][2 + i]) - static_cast<int16_t>(local_block[4][1 + i] / 2 + local_block[4][3 + i] / 2) - temp_h), 0, 255);
				grad_v += 2 * clip(abs(static_cast<int16_t>(local_block[2 + i][4]) - static_cast<int16_t>(local_block[1 + i][4] / 2 + local_block[3 + i][4] / 2) - temp_v), 0, 255);
				break;
			case 4:
				grad_h += 3 * clip(abs(static_cast<int16_t>(local_block[4][2 + i]) - static_cast<int16_t>(local_block[4][1 + i] / 2 + local_block[4][3 + i] / 2) - temp_h), 0, 255) / 2;
				grad_v += 3 * clip(abs(static_cast<int16_t>(local_block[2 + i][4]) - static_cast<int16_t>(local_block[1 + i][4] / 2 + local_block[3 + i][4] / 2) - temp_v), 0, 255) / 2;
				break;
			}			
		}
		grad_d = grad_h / 2 + grad_v / 2;
		if ((grad_d < grad_h) && (grad_d < grad_v))
		{
			temp = static_cast<int16_t>(local_block[4][4] / 2) - static_cast<int16_t>(local_block[3][3] / 8 + local_block[3][5] / 8 + local_block[5][3] / 8 + local_block[5][5] / 8);
			g_out = clip(static_cast<int16_t>(local_block[3][4] / 4 + local_block[4][3] / 4 + local_block[4][5] / 4 + local_block[5][4] / 4) - temp, 0, 255);
		}
		else if ((grad_h < grad_d) && (grad_h < grad_v))
		{
			temp = static_cast<int16_t>(local_block[4][4] / 2) - static_cast<int16_t>(local_block[4][2] / 4 + local_block[4][6] / 4);
			g_out = clip(static_cast<int16_t>(local_block[4][3] / 2 + local_block[4][5] / 2) - temp, 0, 255);
		}
		else if ((grad_v < grad_d) && (grad_v < grad_h))
		{
			temp = static_cast<int16_t>(local_block[4][4] / 2) - static_cast<int16_t>(local_block[2][4] / 4 + local_block[6][4] / 4);
			g_out = clip(static_cast<int16_t>(local_block[3][4] / 2 + local_block[5][4] / 2) - temp, 0, 255);
		}
		else
		{
			temp = static_cast<int16_t>(local_block[4][4] / 2) - static_cast<int16_t>(local_block[3][3] / 8 + local_block[3][5] / 8 + local_block[5][3] / 8 + local_block[5][5] / 8);
			g_out = clip(static_cast<int16_t>(local_block[3][4] / 4 + local_block[4][3] / 4 + local_block[4][5] / 4 + local_block[5][4] / 4) - temp, 0, 255);
		}
	}

	if ((row == 6) && (col == 6))
	{
		printf("center_y = %d ,center_x = %d, lh = %d, lv = %d, grad_h = %d, grad_v = %d ,grad_d = %d \n", center_y, center_x, lh, lv, grad_h, grad_v, grad_d);
		printf("%d %d %d %d %d\n", local_block[4][4], local_block[4][2], local_block[4][6], local_block[4][3], local_block[4][5]);
		printf("%d %d\n", temp, g_out);
	}
	return g_out;
}