/*  CopyRight��ZhiHanZhang
*   FileName��    rgb2ycbcr.cpp
*   Description�� This the C model of module RGB Domain to Ycbcr Domain transfer process (RGB2YCBCR) in the simplest camera ISP pipeline
*                 The RGB2YCbcr module is the combination of 2 processes:
*                      1.read 3 pixel datas in ,seperately the r/g/b
*                      2.calculate the yuv out
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class Rgb2ycbcrModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/2
*/

#include "rgb2ycbcr.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void Rgb2ycbcrModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}

/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void Rgb2ycbcrModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the rgb2ycbcr process is not enabled
	parameter description :
		 1.src_in : the allocated input data space and stores the input length size data
		 2.dst_out : the allocated output data space and stores the output length size data
		 3.length : the data size length of src_in / dst_out data stream
*/
void Rgb2ycbcrModule::CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}

/* This Function is adopted for the rgb2ycbcr module local param initialzation
	 parameter description :
			1.enable : the control signal of the rgb2yuv process, default value is 1
*/
void Rgb2ycbcrModule::Rgb2ycbcr_Param_Initial(bool enable, bool manual)
{
	memset(&local_param, 0, sizeof(Rgb2ycbcrParam));

	if (manual)
	{
		local_param.enable = enable;
	}
	else
	{
		local_param.enable = 1;
	}
}

/* This Function is adopted for the main process of the Rgb2ycbcr module
	 parameter description:
			1.src_in:    the allocated input data space and stores the input length size data
			2.dst_out:   the allocated output data space and stores the output length size data
			3.enable:    the signal controls whether enables the rgb2ycbcr process
			4.top_param: the top param struct
*/
void Rgb2ycbcrModule::Rgb2ycbcrModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable,TopParam& Top)
{
	Rgb2ycbcr_Param_Initial(enable, Top.Manual);
	if (local_param.enable)
	{
		int32_t r, g, b;
		int32_t temp_y, temp_u, temp_v;
		uint8_t y, u, v;
		for (uint32_t i = 0; i < Top.Img_Height; i++)
		{
			for (uint32_t j = 0; j < Top.Img_Width; j++)
			{
				r = static_cast<int32_t>(src_in[i * 3 * Top.Img_Width + 3 * j]);
				g = static_cast<int32_t>(src_in[i * 3 * Top.Img_Width + 3 * j + 1]);
				b = static_cast<int32_t>(src_in[i * 3 * Top.Img_Width + 3 * j + 2]);

				temp_y = 77 * r + 150 * g + 29 * b;
				temp_u = -43 * r - 85 * g + 128 * b + 32768;
				temp_v = 128 * r - 107 * g - 21 * b + 32768;
		
				//y = clip(temp_y>>6, 16, 235);
				//u=  clip(temp_u>>6, 16, 240);
				//v = clip(temp_v>>6, 16, 240);
				y = clip(static_cast<int32_t>(temp_y >> 8), 16, 235);
				u = clip(static_cast<int32_t>(temp_u >> 8), 16, 240);
			  v = clip(static_cast<int32_t>(temp_v >> 8), 16, 240);
				dst_out[i * 3 * Top.Img_Width + 3 * j] = y;
				dst_out[i * 3 * Top.Img_Width + 3 * j + 1] = u;
				dst_out[i * 3 * Top.Img_Width + 3 * j + 2] = v;
			}
		}
		printf("ISP rgb2yuv process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, 3*Top.Img_Height*Top.Img_Width);
		printf("ISP rgb2yuv process is bypassed!\n");
	}
}