/*  CopyRight��ZhiHanZhang
*   FileName��  rgb2ycbcr.h
*   Description�� This the header file of module RGB Domain to YCbCr domain color transfer process (rgb2ycbcr) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module demosaic process: Rgb2ycbcrParam
*                      2.class definition of module RawDns process: Rgb2ycbcrModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/2
*/

#pragma once
#ifndef _ISP_RGB2YCBCR_H
#define _ISP_RGB2YCBCR_H

#include "common.h"

/* This is the local param of the isp module -- RGB Domain to Ycbcr Domain Process, main contains the value of parameter:enable,mode
		parameter description:
				1. enable: controls whether enables the rgb2ycbcr process
*/
typedef struct
{
	bool enable;                 /* Determine whether enables the rgb2yuv process */
}Rgb2ycbcrParam;


/* This is the class definition of module rgb2yucbcr process, private param only includes the Rgb2ycbcrParam
	 Functions includes the data read in/ data write out ��main Rgb2ycbcr process ��data copy process ��
	 Rgb2ycbcr_param initialize process 
*/

class Rgb2ycbcrModule
{
private:
	Rgb2ycbcrParam local_param;
protected:
	void Rgb2ycbcr_Param_Initial(bool enable, bool manual);
public:
	Rgb2ycbcrModule()
	{

	};
	~Rgb2ycbcrModule()
	{

	};
	void  Rgb2ycbcrModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, TopParam& Top);
	void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};
#endif


