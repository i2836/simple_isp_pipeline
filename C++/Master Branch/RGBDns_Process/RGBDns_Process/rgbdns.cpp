/*  CopyRight��ZhiHanZhang
*   FileName��    rgbdns.cpp
*   Description�� This the C model of module RGB Domain Denoise (RGBDNS) in the simplest camera ISP pipeline
*                 The RGB Domain Denoise (RGBDNS) module is the combination of 4 processes:
*                      1.Local 9*9*3 block fetch process: being finished by the function ���� Get_Rgbdns_Block()
*                      2.Calculating the Eur_distance in each 3*3*3 data region of the fetched 9*9*3 block
*                      3.Calculating the weight of each 3*3*3 pairs according to the eur_dis value in step2
*                      4.Statisticing all the calculated weight in local block 9*9*3 and weighting to get final denoised result
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class RgbdnsModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/2
*/

#include "rgbdns.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void RgbdnsModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}

/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void RgbdnsModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the rgbdns process is not enabled
	parameter description :
		 1.src_in : the allocated input data space and stores the input length size data
		 2.dst_out : the allocated output data space and stores the output length size data
		 3.length : the data size length of src_in / dst_out data stream
*/
void RgbdnsModule::CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}

/* This Function is adopted for the rgbdns module local param initialzation
	 parameter description :
			1.enable : the control signal of the rgbdns process, default value is 1
			2.sigma  : controls the intensity (standard diff) of gaussian noise of the input image 
*/
void RgbdnsModule::Rgbdns_Param_Initial(bool enable, bool manual, uint32_t sigma)
{
	memset(&local_param, 0, sizeof(RgbdnsParam));

	if (manual)
	{
		local_param.enable = enable;
		local_param.sigma = sigma;
	}
	else
	{
		local_param.enable = 1;
		local_param.sigma = 30;
	}
}


/* This Function is adopted for the rgbdns module local img data block fetch process (size is 9*9*3)
	 Enables this function to get the 9*9*3 data block which is centered as the cur scan point
	 parameter description:
			1.block:  the 3D array to store the fetched 9*9*3 local img data region
			2.src_in: the allocated input data space and stores the input length size data
			3.cur_x:  the horizontial position index of the current scan point
			4.cur_y:  the vertical position index of the current scan point
			5.width:  the width of the input image size
 */
void  RgbdnsModule::Get_Rgbdns_Block(uint8_t block[9][9][3], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width)
{
	for (uint32_t i = 0; i < 9; i++)
	{
		for (uint32_t j = 0; j < 9; j++)
		{
			for (uint32_t k = 0; k < 3; k++)
			{
				block[i][j][k] = src_in[(cur_y - 4 + i) * 3 * width + 3 * (cur_x - 4 +  j) + k];
			}
		}
	}
}


/* This Function is adopted for the main process of the Rgbdns module
	 parameter description:
			1.src_in:    the allocated input data space and stores the input length size data
			2.dst_out:   the allocated output data space and stores the output length size data
			3.enable:    the signal controls whether enables the rgbdns process
			4.sigma :    controls the intensity of gaussian noise of the input image
			5.top_param: the top param struct
*/
void RgbdnsModule::RgbdnsModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint32_t sigma, TopParam& top_param)
{
	Rgbdns_Param_Initial(enable, top_param.Manual, sigma);
	if (local_param.enable)
	{
		uint8_t local_block[9][9][3];
		uint8_t pix_out = 0;
		memcpy(dst_out, src_in, sizeof(uint8_t) * 3 * top_param.Img_Height * top_param.Img_Width);
		printf("r = %d,g = %d, b = %d\n", src_in[3 * 772], src_in[3 * 772 + 1], src_in[3 * 772 + 2]);
		for (uint32_t i = 4; i < top_param.Img_Height - 4; i++)
		{
			for (uint32_t j = 4; j < top_param.Img_Width - 4; j++)
			{
				uint32_t accum_rv = 0, accum_gv = 0, accum_bv = 0;
				uint32_t accum_w = 0;
				uint32_t eur_distance;
				uint8_t com_block1[3][3][3], com_block2[3][3][3];
				uint32_t max_weight =0;
				uint32_t weight;
				Get_Rgbdns_Block(local_block, src_in, j, i, top_param.Img_Width);
				Get_Comp_Block(local_block, 4, 4, com_block2);
				for (uint32_t m = 0; m < 7; m++)
				{
					for (uint32_t n = 0; n < 7; n++)
					{
						if ((m != 3) || (n != 3))
						{
							Get_Comp_Block(local_block, 1 + m, 1 + n, com_block1);
							eur_distance = Cal_Eur_Distance(com_block1, com_block2);
							weight = Weight_Calculation_Process(eur_distance, sigma);

							accum_w += weight;
							accum_rv += weight * com_block1[1][1][0];
							accum_gv += weight * com_block1[1][1][1];
							accum_bv += weight * com_block1[1][1][2];

							if (weight > max_weight)
							{
								max_weight = weight;
							}
						}

						/*
						if ((i == 4) && (j == 4))
						{
							printf("m = %d,n = %d,weight = %d\n", m, n, weight);
							printf("accum_r = %d,accum_g = %d��accum_b = %d, accum_w = %d\n", accum_rv, accum_gv, accum_bv, accum_w);
						}
						*/
					}
				}

				accum_w += max_weight;
				accum_rv += max_weight * com_block2[1][1][0];
				accum_gv += max_weight * com_block2[1][1][1];
				accum_bv += max_weight * com_block2[1][1][2];

				/*
				if ((i == 4) && (j == 4))
				{
					printf("accum_r = %d,accum_g = %d��accum_b = %d, accum_w = %d\n", accum_rv, accum_gv, accum_bv, accum_w);
				}
				*/

				if ((accum_w >> 10) == 0)
				{
					dst_out[i * 3 * top_param.Img_Width + 3 * j] = com_block2[1][1][0];
					dst_out[i * 3 * top_param.Img_Width + 3 * j + 1] = com_block2[1][1][1];
					dst_out[i * 3 * top_param.Img_Width + 3 * j + 2] = com_block2[1][1][2];
				}
				else
				{
					dst_out[i * 3 * top_param.Img_Width + 3 * j] = clip((accum_rv >> 10) / (accum_w >> 10),0,255);
					dst_out[i * 3 * top_param.Img_Width + 3 * j + 1] = clip((accum_gv >> 10) / (accum_w >> 10),0,255);
					dst_out[i * 3 * top_param.Img_Width + 3 * j + 2] = clip((accum_bv >> 10) / (accum_w >> 10),0,255);
				}
			}
		}
		printf("ISP rgbdns process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, 3 *top_param.Img_Height * top_param.Img_Width);
		printf("ISP rgbdns process is bypassed!\n");
	}
}

/* This Function is adopted for the Eurdistance Calculation process of two local data array in the Rgbdns module
	 parameter description:
			1.comp_block1:  the block which has the size of 3*3*3 to store the comp block data in the non center 3*3*3 block area
			2.comp_block2:  the block which has the size of 3*3*3 to store the comp block data in the center 3*3*3 block area
*/
uint32_t  RgbdnsModule::Cal_Eur_Distance(uint8_t comp_block1[3][3][3], uint8_t comp_block2[3][3][3])
{
	uint32_t dis = 0;
	for (uint32_t i = 0; i < 3; i++)
	{
		for (uint32_t j = 0; j < 3; j++)
		{
			for (uint32_t k = 0; k < 3; k++)
			{
				dis += static_cast<uint32_t>(abs_diff(comp_block1[i][j][k], comp_block2[i][j][k]))* static_cast<uint32_t>(abs_diff(comp_block1[i][j][k], comp_block2[i][j][k]));
			}
		}
	}
	return (dis * 19) >> 9 ;
}

/* This Function is adopted to get the 3*3*3 size comp block in the fetched 9*9*3 local data region
	 parameter description:
			1.local_block:  the 3D array to store the fetched 9*9*3 local img data region
			2.cur_x/cur_y:  the center of under-fetched 3*3*3 comp block area
			3.comp_block:   the block which has the size of 3*3*3 to store the fetched comp block data 
*/
void  RgbdnsModule::Get_Comp_Block(uint8_t local_block[9][9][3], uint32_t cur_x, uint32_t cur_y, uint8_t comp_block[3][3][3])
{
	for (uint32_t i = 0; i < 3; i++)
	{
		for (uint32_t j = 0; j < 3; j++)
		{
			comp_block[i][j][0] = local_block[cur_y - 1 + i][cur_x - 1 + j][0];
			comp_block[i][j][1] = local_block[cur_y - 1 + i][cur_x - 1 + j][1];
			comp_block[i][j][2] = local_block[cur_y - 1 + i][cur_x - 1 + j][2];
		}
	}
}

/* This Function is adopted to calcultion the denoised weighting value for the input block-based Eur-Distance value
	 parameter description:
			1.local_block:  the 3D array to store the fetched 9*9*3 local img data region
			2.cur_x/cur_y:  the center of under-fetched 3*3*3 comp block area
			3.comp_block:   the block which has the size of 3*3*3 to store the fetched comp block data
*/
uint32_t RgbdnsModule::Weight_Calculation_Process(uint32_t eur_dis, uint32_t sigma)
{
	uint32_t weight;
	switch (sigma)
	{
	case 10:
		if (eur_dis <= 169)
			weight = 1024;
		else if ((eur_dis > 169) && (eur_dis <= 196))
			weight = 445;
		else if ((eur_dis > 196) && (eur_dis <= 225))
			weight = 158;
		else if ((eur_dis > 225) && (eur_dis <= 256))
			weight = 53;
		else if ((eur_dis > 256) && (eur_dis <= 289))
			weight = 16;
		else if ((eur_dis > 289) && (eur_dis <= 324))
			weight = 5;
		else if ((eur_dis > 324) && (eur_dis <= 361))
			weight = 1;
		else
			weight = 0;
		break;
	case 30:
		if (eur_dis <= 1764)
			weight = 1024;
		else if ((eur_dis > 1764) && (eur_dis <= 1849))
			weight = 729;
		else if ((eur_dis > 1849) && (eur_dis <= 1936))
			weight = 398;
		else if ((eur_dis > 1936) && (eur_dis <= 2025))
			weight = 215;
		else if ((eur_dis > 2025) && (eur_dis <= 2116))
			weight = 114;
		else if ((eur_dis > 2116) && (eur_dis <= 2209))
			weight = 60;
		else if ((eur_dis > 2209) && (eur_dis <= 2304))
			weight = 31;
		else if ((eur_dis > 2304) && (eur_dis <= 2401))
			weight = 16;
		else
			weight = 0;
		break;
	case 50:
		if (eur_dis <= 6889)
			weight = 1024;
		else if ((eur_dis > 6889) && (eur_dis <= 7056))
			weight = 968;
		else if ((eur_dis > 7056) && (eur_dis <= 7225))
			weight = 657;
		else if ((eur_dis > 7225) && (eur_dis <= 7396))
			weight = 444;
		else if ((eur_dis > 7396) && (eur_dis <= 7596))
			weight = 298;
		else if ((eur_dis > 7596) && (eur_dis <= 7744))
			weight = 200;
		else if ((eur_dis > 7744) && (eur_dis <= 7921))
			weight = 133;
		else if ((eur_dis > 7921) && (eur_dis <= 8100))
			weight = 88;
		else if ((eur_dis > 8100) && (eur_dis <= 8281))
			weight = 58;
		else if ((eur_dis > 8281) && (eur_dis <= 8464))
			weight = 38;
		else if ((eur_dis > 8464) && (eur_dis <= 8649))
			weight = 25;
		else if ((eur_dis > 8649) && (eur_dis <= 8836))
			weight = 16;
		else
			weight = 0;
		break;
	default:
		if (eur_dis <= 1764)
			weight = 1024;
		else if ((eur_dis > 1764) && (eur_dis <= 1849))
			weight = 729;
		else if ((eur_dis > 1849) && (eur_dis <= 1936))
			weight = 398;
		else if ((eur_dis > 1936) && (eur_dis <= 2025))
			weight = 215;
		else if ((eur_dis > 2025) && (eur_dis <= 2116))
			weight = 114;
		else if ((eur_dis > 2116) && (eur_dis <= 2209))
			weight = 60;
		else if ((eur_dis > 2209) && (eur_dis <= 2304))
			weight = 31;
		else if ((eur_dis > 2304) && (eur_dis <= 2401))
			weight = 16;
		else
			weight = 0;
	}
	return weight;
}
