/*  CopyRight��ZhiHanZhang
*   FileName��  dgc.h
*   Description�� This the header file of module Digital Gain Gain(DGC) in the simplest camera ISP pipeline   
*                 This header file includes the following contents:
*                      1.local paramset of module dgc process: Dgcparam
*                      2.class definition of module dgc process: DgcModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/25
*/

#pragma once
#ifndef ISP_DGC_H
#define ISP_DGC_H

#include "common.h"

/* This is the local param of the isp module -- Digital Gain Correction, main contains the params of BLC��Dgain and LSC */
typedef struct { 
	bool enable;                 /* Determine whether enables the dgc process */
	uint8_t blc_Value;           /* blc value of input picture,default is 0 */
	uint8_t rGain;               /* dGain value of R position in the cfa array, default is 64 ,should be right shifted 6 bit when getting the final result */
	uint8_t gGain;               /* dGain value of G position in the cfa array, default is 64 ,should be right shifted 6 bit when getting the final result */
	uint8_t bGain;               /* dGain value of B position in the cfa array, default is 64 ,should be right shifted 6 bit when getting the final result */
	uint32_t lsc_param[17][13];  /* calibration parameter of camera lens ,default is 256 , the result should be right shifted 8 bit */
}Dgcparam;



/* This is the class definition of module dgc process, private param only includes the dgcparam 
   Functions includes the data read in/ data write out �� main dgc process ��the lsc process and data copy process
*/
class DgcModule
{
 private:
	 Dgcparam local_param;
 protected:
	 void Dgc_Param_Initial(bool enable, bool Manual, uint8_t blc_Value, uint8_t rGain, uint8_t gGain, uint8_t bGain, uint32_t lsc_param[17][13]);
 public:
	 DgcModule()
	 {

	 };

	 ~DgcModule()
	 {

	 };

	 uint32_t DgcLscProcess(uint32_t pos_x, uint32_t pos_y, uint32_t height, uint32_t width, uint32_t lsc_param[17][13], uint32_t cur_x, uint32_t cur_y);
	 void DgcModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t blc_Value, uint8_t rGain, uint8_t gGain, uint8_t bGain, uint32_t lsc_param[17][13],TopParam& top_param);
	 void ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	 void ImgWriteout(FILE* fp2, uint32_t length ,uint8_t* dst_out);
	 void CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};

#endif