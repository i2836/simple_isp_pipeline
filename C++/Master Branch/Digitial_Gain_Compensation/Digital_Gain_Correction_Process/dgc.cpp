/*  CopyRight��ZhiHanZhang
*   FileName��  dgc.cpp
*   Description�� This the C model of module Digital Gain Gain(DGC) in the simplest camera ISP pipeline
*                 The Digital Gain Correction Process (DGC) module is the combination of 3 processes:
*                      1.BLC Process (Black Lens Correction Process)
*                      2.Dgain Process (Sensor Digtial Gain Compensation Process)
*                      3.LSC Process (Lens Shading Correction Process)
*                 This file contains the following content:
*                      1.the public function descriptions of class DgcModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/27
*/

#include "dgc.h"
/* This Function is adopted for the input img data read in 
   parameter description: 
			1.fp1(the FILE pointer),used to open a file input stream
	    2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data 
*/
void DgcModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}

/* This Function is adopted for the input img data write out
	 parameter description:
			1.fp2(the FILE pointer),used to open a file output stream
			2.length: the write size of output data stream from the fp2
			3.dst_out: the allocated output data space and stores the output length size data
*/
void DgcModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the dgc process is not enabled
	 parameter description:
			1.src_in: the allocated input data space and stores the input length size data
			2.dst_out: the allocated output data space and stores the output length size data
			3.length: the data size length of src_in/dst_out data stream
*/
void DgcModule::CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t)*length);
}


/* This Function is adopted for the dgc module local param initialzation, when the manual = 1,adopts the outside input param to initialize the Dgcparam, vice adopts the default param
	 parameter description:
			1.enable: the control signal of Dgc process
			2.Manual: choose the initialize methods of the dgc param value
			3.blc_value : the param of blc process
			4.rGain/gGain/bGain: the param of the dGain process
			5.lsc_param: the param of the lsc process
*/
void DgcModule::Dgc_Param_Initial(bool enable,bool Manual, uint8_t blc_Value, uint8_t rGain, 
	                                uint8_t gGain, uint8_t bGain, uint32_t lsc_param[17][13])
{
	memset(&local_param, 0, sizeof(Dgcparam));

	if (Manual)
	{
		local_param.enable = enable;
		local_param.blc_Value = blc_Value;
		local_param.rGain = rGain;
		local_param.gGain = gGain;
		local_param.bGain = bGain;
		for (uint32_t i = 0; i < 17; i++)
		{
			for (uint32_t j = 0; j < 13; j++)
			{
				local_param.lsc_param[i][j] = lsc_param[i][j];
			}
		}
	}
	else
	{
		local_param.enable = 1;
		local_param.blc_Value = 10;
		local_param.rGain = 62;
		local_param.gGain = 66;
		local_param.bGain = 64;
		for (uint32_t i = 0; i < 17; i++)
		{
			for (uint32_t j = 0; j < 13; j++)
			{
				local_param.lsc_param[i][j] = 256;
			}
		}
	}
}

/* This Function is adopted for the main process of the dgc module local param
	 parameter description:
			1.src_in: the allocated input data space and stores the input length size data
		  2.dst_out: the allocated output data space and stores the output length size data
			3.enable: the signal controls whether enables the dgc process
			4.blc_Value:  the param of blc process
			5.rGain/gGain/bGain: the param of the dGain process
			6.lsc_param: the param of the lsc process
			7.top_param: the top param struct 
*/
void DgcModule::DgcModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t blc_Value,uint8_t rGain, uint8_t gGain, uint8_t bGain, uint32_t lsc_param[17][13],TopParam& top_param)
{
	Dgc_Param_Initial(enable, top_param.Manual, blc_Value, rGain, gGain, bGain, lsc_param);
	if (local_param.enable)
	{
		uint32_t height_interval = (top_param.Img_Height >> 2) / 3;    /* The height interval for LSC param postion preset */
		uint32_t width_interval = top_param.Img_Width >> 4;            /* The width interval for LSC param postion preset */
		uint32_t count_x = 0, count_y = 0;                             /* the counter and pos are used to ensure the position of cur scan point in the lsc param array */
		uint8_t  pos_x = 0, pos_y = 0;                                 /* pos_x and pox_y are used to ensure the top-left pointi adopted in the lsc param array */

		for (uint32_t i = 0; i < top_param.Img_Height; i++)
		{
			count_y++;
			count_x = 0;  pos_x = 0;
			for (uint32_t j = 0; j < top_param.Img_Width; j++)
			{
				uint8_t pos_flag_y = static_cast<uint8_t>(i & 1);
				uint8_t pos_flag_x = static_cast<uint8_t>(j & 1);
				uint8_t pos_flag_r = (pos_flag_y << 1) || (pos_flag_x);
				uint8_t pos_flag_b = (!(pos_flag_x << 1)) || (!(pos_flag_y));

				count_x++;
				if (count_x == width_interval)
				{
					pos_x++;
					count_x = 0;
				}

				/* enables the blc process */
				dst_out[i * top_param.Img_Width + j] = clip(src_in[i * top_param.Img_Width + j], local_param.blc_Value, (1 << (top_param.BitDepth)) - 1) - local_param.blc_Value;

				/* enables the dgain process seperately for the R/G/B pixels in the cfa pattern */
				if (top_param.BayerPattern == pos_flag_r) /*  Current Scan Point is R in the CFA pattern */
				{
					dst_out[i * top_param.Img_Width + j] = clip(static_cast<uint32_t>(dst_out[i * top_param.Img_Width + j]) * local_param.rGain >> 6, 0, (1 << (top_param.BitDepth)) - 1);
				}
				else if (top_param.BayerPattern == pos_flag_b) /*  Current Scan Point is B in the CFA pattern */
				{
					dst_out[i * top_param.Img_Width + j] = clip(static_cast<uint32_t>(dst_out[i * top_param.Img_Width + j]) * local_param.bGain >> 6, 0, (1 << (top_param.BitDepth)) - 1);
				}
				else /*  Current Scan Point is G in the CFA pattern */
				{
					dst_out[i * top_param.Img_Width + j] = clip(static_cast<uint32_t>(dst_out[i * top_param.Img_Width + j]) * local_param.gGain >> 6, 0, (1 << (top_param.BitDepth)) - 1);
				}

				/* enables the lsc process according to the currecnt scan position */
				dst_out[i * top_param.Img_Width + j] = clip(static_cast<uint32_t>(dst_out[i * top_param.Img_Width + j]) * DgcLscProcess(pos_x, pos_y, top_param.Img_Height, top_param.Img_Width, local_param.lsc_param, j, i) >> 8, 0, (1 << (top_param.BitDepth)) - 1);
				if ((i == 0) && (j == 1440))
				{
					printf("in = %d, out = %d\n", src_in[i * top_param.Img_Width + j], dst_out[i * top_param.Img_Width + j]);
				}
			}
			if (count_y == height_interval)
			{
				pos_y++;
				count_y = 0;
			}
		}

		printf("ISP dgc process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, top_param.Img_Height* top_param.Img_Width);
		printf("ISP dgc process is bypassed!\n");
	}
}

/* This Function is adopted for the lsc process in the dgc module, adopts the bilinear intp to calculate the lsc gain value 
	 parameter description:
			1.pos_x/pos_y: the current scan gridding position in the 17*13 divided img 
			2.height/width: the size of input img 
			3.lsc_param: the param of the lsc process
			4.cur_x/cur_y: the current scan  position in the height*width sized img 
*/
uint32_t DgcModule::DgcLscProcess(uint32_t pos_x, uint32_t pos_y, uint32_t height, uint32_t width, uint32_t lsc_param[17][13], uint32_t cur_x, uint32_t cur_y)
{
	uint32_t Top_Left = lsc_param[pos_y][pos_x];
	uint32_t Top_Right = lsc_param[pos_y][pos_x + 1];
	uint32_t Bottom_Left = lsc_param[pos_y + 1][pos_x];
	uint32_t Bottom_Right = lsc_param[pos_y + 1][pos_x + 1];
	
	uint32_t dis_left = cur_x + 1 - pos_x * width / 16;
	uint32_t dis_right = (pos_x + 1) * width / 16 - cur_x - 1;
	uint32_t dis_top = cur_y + 1 - pos_y * height / 12;
	uint32_t dis_bottom = (pos_y + 1) * height / 12 - cur_y - 1;

	/* for the C++ software version */
	//uint32_t pos_x1 = (cur_x *  Top_Right + 1) * 16  / width + Top_Left * (pos_x + 1) - (cur_x *  Top_Left + 1) * 16 / width  - pos_x * Top_Right;
	//uint32_t pos_x2 = (cur_x *  Bottom_Right + 1) * 16 / width + Bottom_Left * (pos_x + 1) - (cur_x * Bottom_Left + 1) * 16 / width - pos_x * Bottom_Right;
	//uint32_t lsc_value = (cur_y  * pos_x2 + 1) * 12 / height + pos_x1 * (pos_y + 1) - (cur_y * pos_x1 + 1) * 12 / height - pos_y * pos_x2;
	
	/* for the HLS hardware implemenmt version */
	uint32_t pos_x1 = clip(((Top_Left * dis_right + Top_Right * dis_left) * 17) >> 11,0,1023);
	uint32_t pos_x2 = clip(((Bottom_Left * dis_right + Bottom_Right * dis_left) * 17) >> 11,0,1023);
	uint32_t lsc_value = clip(((pos_x1 * dis_bottom + pos_x2 * dis_top) * 91) >> 13, 0, 1023);
	return lsc_value;
}