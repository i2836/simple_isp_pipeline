/*  CopyRight��ZhiHanZhang
*   FileName��  dgc_singletest.cpp
*   Description�� This the Test file of module Digital Gain Gain(DGC) in the simplest camera ISP pipeline
*                 This file contains the following content:
*                      1.TopParam initial 
*                      2.The input/output data space allocation and read/write
*                      3.Test the DGC process
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/27
*/

#include "common.h"
#include "dgc.h"

int main(int argc, char** argv)
{
	/* Top param��class and FILE pointer initialization */
	TopParam t1;
	DgcModule dgc;
	FILE* fp1, *fp2;
	errno_t err_1, err_2;

	if (argc != 8)
		printf("Error! Not input required number of paramter!\n");

	//fp1 = fopen(argc[0], "rb");
	//fp2 = fopen(argc[1], "wb");
	err_1 = fopen_s(&fp1, argv[1], "rb");
	err_2 = fopen_s(&fp2, argv[2], "wb");

	memset(&t1,0,sizeof(TopParam));

	t1.BitDepth = 8;
	t1.BayerPattern = 0;
	t1.Enable = 1;
	t1.Img_Height = 1080;
	t1.Img_Width = 1920;
	t1.Manual = 0;

	/* In/Out data space allocation and Initilze the parameter of the lsc process */
	uint32_t lsc_array[17][13];
	uint8_t* src_in = (uint8_t*)malloc(sizeof(uint8_t) * t1.Img_Height * t1.Img_Width);
	uint8_t* dst_out = (uint8_t*)malloc(sizeof(uint8_t) * t1.Img_Height * t1.Img_Width);

	for (uint32_t i = 0; i < 17; i++)
	{
		for (uint32_t j = 0; j < 13; j++)
		{
			lsc_array[i][j] = 256;
		}
	}

	/* Data input ��Main DGC process and Data output */
	dgc.ImgReadin(fp1, t1.Img_Height * t1.Img_Width, src_in);
	dgc.DgcModuleProcess(src_in, dst_out, atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]), lsc_array, t1);
	dgc.ImgWriteout(fp2, t1.Img_Height * t1.Img_Width, dst_out);

	free(src_in);
	free(dst_out);

	printf("ISP Dgc process finish!\n");
	return 0;
}