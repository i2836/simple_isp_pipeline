/*  CopyRight��ZhiHanZhang
*   FileName��    sp.cpp
*   Description�� This the C model of module Salt and Pepper Noise Remove (SP) in the simplest camera ISP pipeline
*                 The Salt and Pepper Noise Remove (SP) module is the combination of 3 processes:
*                      1.Local block fetch process: being finished by the function ���� Get_Sp_Block()
*                      2.The Bilinear process of the input 5*5 size raw block to get the full center color type block: 
*                        being finished by the function ���� Bilinear_Intp()
*                      3.The Array_sort process: being finished by the function ���� Quick_Sort()
*                 This file contains the following content:
*                      1.the public function descriptions of class SpModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/27
*/

#include "sp.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void SpModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}

/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void SpModule::ImgWriteout(FILE * fp2, uint32_t length, uint8_t * dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the sp process is not enabled
	 parameter description:
			1.src_in: the allocated input data space and stores the input length size data
			2.dst_out: the allocated output data space and stores the output length size data
			3.length: the data size length of src_in/dst_out data stream
*/
void SpModule::CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}

/* This Function is adopted for the sp module local param initialzation, when the manual = 1,
   adopts the outside input param to initialize the SpParam, vice adopts the default value
	 parameter description:
			1.enable: the control signal of the sp process, default value is 1
			2.Manual: choose the initialize methods of the sp param value, default value is 1
			3.low_thre : the assumed value threhold of the pepper noise, default value is 5
			4.high_thre: the the assumed value threhold of the salt noise, default value is 250
*/
void SpModule::Sp_Param_Initial(bool enable, bool Manual, uint8_t low_value, uint8_t high_value)
{
	memset(&local_param, 0, sizeof(SpParam));

	if (Manual)
	{
		local_param.enable = enable;
		local_param.low_thre = low_value;
		local_param.high_thre = high_value;
	}
	else
	{
		local_param.enable = 1;
		local_param.low_thre = 5;
		local_param.high_thre = 250;
	}
}


/* This Function is adopted for the sp module local img data block fetch process
*  When the pixel value of current scan point is below the low_thre or beyond the high_thre
*  Enables this function to get the 5*5  data block which is centered as the cur scan point
	 parameter description:
			1.block:  the 2D array to store the fetched 5*5 img data region 
			2.src_in: the allocated input data space and stores the input length size data
			3.cur_x:  the horizontial position index of the current scan point
			4.cur_y:  the vertical position index of the current scan point
			5.width:  the width of the input image size
 */
void  SpModule::Get_Sp_Block(uint8_t block[5][5], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width)
{
	for (uint32_t i = 0; i < 5; i++)
	{
		for (uint32_t j = 0; j < 5; j++)
		{
			block[i][j] = src_in[(cur_y - 2 + i) * width + cur_x - 2 + j];
		}
	}
}


/* This Function is adopted for the sp module after the local img data block fetch process finished,
   At this time, for Raw Img, the 5*5 fetched block only has 3*3 pixels the same CFA type as the center point
	 The Bilinear_Intp() process is adopted to interpolate the 5*5 raw block into 5*5 the same type as center pixel
	 parameter description:
			1.block[5][5]:  the 2D array to store the fetched 5*5 raw img data region,output the 5*5 Bilinear-Intped block
 */
void  SpModule::Bilinear_Intp(uint8_t block[5][5])
{
	/* pre roughly correct the center SP noise point for avoiding the error intp*/
	uint8_t grad_h = abs_diff(block[2][0],block[2][4]);
	uint8_t grad_v = abs_diff(block[0][2],block[4][2]);
	if (grad_h < grad_v)
	{
		block[2][2] = block[2][0] / 2 + block[2][4] / 2;
	}
	else if (grad_h > grad_v)
	{
		block[2][2] = block[0][2] / 2 + block[4][2] / 2;
	}
	else
	{
		block[2][2] = block[2][0] / 4 + block[2][4] / 4 + block[0][2] / 4 + block[4][2] / 4;
	}

	/* Intp the missing value in the row number 0/2/4, col number 1/3 */
	for (uint32_t i = 0; i < 5; i += 2)
	{
		for (uint32_t j = 1; j < 5; j += 2)
		{
			block[i][j] = block[i][j - 1] / 2 + block[i][j + 1] / 2;
		}
	}

	/* Intp the missing value in the col number 0/2/4, row number 1/3 */
	for (uint32_t i = 0; i < 5; i += 2)
	{
		for (uint32_t j = 1; j < 5; j += 2)
		{
			block[j][i] = block[j-1][i] / 2 + block[j+1][i] / 2;
		}
	}

	/* Intp the missing value in the col number 1/3 , row number 1/3 */
	for (uint32_t i = 1; i < 5; i += 2)
	{
		for (uint32_t j = 1; j < 5; j += 2)
		{
			block[i][j] = block[i - 1][j - 1] / 4 + block[i - 1][j + 1] / 4 + block[i + 1][j - 1] / 4 + block[i + 1][j + 1] / 4;
		}
	}
}


/* This Function is adopted for the sp module after the local img data block fetch process and Bilinear-Intp process finished,
   When enables this function, it aims at correcting the center point(which is already determined as the salt or pepper noise) 
	 by the array sort result of this module
	 parameter description:
			1.block[5][5]:  the 2D array to store the fetched and Bilinear-Intped 5*5 raw img data region
			2.sort_out[25]: the sorted out 1D array to store the ascending 25 pixels values
*/
void  SpModule::Quick_Sort(uint8_t block[5][5], uint8_t sort_out[25])
{
	memset(sort_out, 0, sizeof(uint8_t) * 25);
	/* Load the 2D 5*5 array into the 1D 25 size depth data block */
	uint8_t min , index, temp;
	for (uint32_t i = 0; i < 5; i++)
	{
		for (uint32_t j = 0; j < 5; j++)
		{
			sort_out[i * 5 + j] = block[i][j];
		}
	}

	/*The simplest bubble sort, which will be updated by the more effective sort method in the future*/
	for (uint32_t i = 0; i < 25; i++)
	{
		min = sort_out[i], index = i;
		for (uint32_t j = i + 1; j < 25; j++)
		{
			if (sort_out[j] < min)
			{
				index = j;
				min = sort_out[j];
			}
		}
		temp = sort_out[index];
		sort_out[index] = sort_out[i];
		sort_out[i] = temp;
	}
}


/* This Function is the main process of the module SP(Salt and Pepper Noise remove),the whole process is as follows:
*       1.Scan the whole input img data stored in the allocted space src_in, comparing the value of current scan position 
          with the preset threhold: low_thre (the preset pepper noise threhold)  and high_thre (the preset salt noise threhold)
        2.If the value of current scan point is in normal range,by pass it, otherwise,enables the SP noise remove process
				  The SP noise remove prcoess includes four process: 
					      1. 5*5 data block fetch. Finishing by the function ���� Get_Sp_Block()
								2. 5*5 data block bilinear intp process. Finishing by the function ���� Bilinear_Intp()
								3. 5*5 data block Quick_Sort process. Finishing by the function ���� Quick_Sort()
								4. The SP noise remove process. Related to the Algorithm in the Matlab module version
	 parameter description:
			1.src_in:    the allocated input data space and stores the input length size data
			2.dst_out:   the allocated output data space and stores the output length size data
			3.enanle:    the signal which controls whether enable the SP noise remove process
			4.low_thre:  the assumed value threhold of the pepper noise, default value is 5
			5.high_thre: the the assumed value threhold of the salt noise, default value is 250
			6.Top:       the top paramater definition
*/
void  SpModule::SpModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t low_value, uint8_t high_value, TopParam& Top)
{
	Sp_Param_Initial(enable, Top.Manual, low_value, high_value);
	if (local_param.enable)
	{
		uint8_t local_block[5][5] = { {0} };
		uint8_t sort_block[25] = { 0 };
		uint8_t max = 0, med = 0, min = 0;
		uint8_t temp;
		memcpy(dst_out, src_in, sizeof(uint8_t) * Top.Img_Height * Top.Img_Width);
		for(uint32_t i = 2;i < Top.Img_Height - 2 ; i++)
		{
			for (uint32_t j = 2; j < Top.Img_Width - 2; j++)
			{				
				if ((src_in[i * Top.Img_Width + j] > high_value) || (src_in[i * Top.Img_Width + j] < low_value))
				{		
					Get_Sp_Block(local_block, src_in, j, i, Top.Img_Width);
					temp = local_block[2][2];
					Bilinear_Intp(local_block);
					Quick_Sort(local_block, sort_block);
					max = sort_block[24], med = sort_block[12], min = sort_block[0];

					if ((min < temp) && (temp < max))
					{
						dst_out[i * Top.Img_Width + j] = temp;
					}
					else if ((min == temp) || (max == temp))
					{

						dst_out[i * Top.Img_Width + j] = sort_block[10] / 4 + sort_block[11] / 4 + sort_block[13] / 4 + sort_block[14] / 4;
					}
					else
					{
						dst_out[i * Top.Img_Width + j] = med;
					}
					
				}			
			}
		}
		printf("ISP sp process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, Top.Img_Height * Top.Img_Width);
		printf("ISP sp process is bypassed!\n");
	}
}