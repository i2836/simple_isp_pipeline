/*  CopyRight��ZhiHanZhang
*   FileName��  sp.h
*   Description�� This the header file of module Salt and Pepper Noise Remove (SP) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module sp process: SpParam
*                      2.class definition of module SP process: SpModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/27
*/

#pragma once
#ifndef _ISP_SP_H
#define _ISP_SP_H

#include "common.h"

/* This is the local param of the isp module -- Salt and Pepper Noise Remove, main contains the value of high/low ,seperately refers to assumed Salt/Pepper Noise */
typedef  struct
{
	bool enable;          /* Determine whether enables the dgc process */
	uint8_t low_thre;     /* Assumed Pepper Noise in the image, default is 5 for the 8 bit data depth */
	uint8_t high_thre;    /* Assumed Salt Noise in the image, default is 250 for the 8 bit data depth */
}SpParam;


/* This is the class definition of module sp process, private param only includes the SpParam
	 Functions includes the data read in/ data write out �� main sp process ��data copy process ��
	                    the Biliniear process ��the calculation block get process and array_sort process
*/
class SpModule
{
 private:
	 SpParam local_param;
 protected:
	 void Sp_Param_Initial( bool enable, bool Manual, uint8_t low_value, uint8_t high_value);
 public:
	 SpModule()
	 {

	 };
	 ~SpModule()
	 {

	 };
	 void  Get_Sp_Block(uint8_t block[5][5], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width);
	 void  Quick_Sort(uint8_t block[5][5], uint8_t sort_out[25]);
	 void  Bilinear_Intp(uint8_t block[5][5]);
	 void  SpModuleProcess(uint8_t* src_in, uint8_t* dst_out,bool enable,uint8_t low_value, uint8_t high_value, TopParam& Top);
	 void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	 void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	 void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};

#endif
