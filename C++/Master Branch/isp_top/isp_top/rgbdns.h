/*  CopyRight��ZhiHanZhang
*   FileName��  rgbdns.h
*   Description�� This the header file of module RGB Domain Denoise process (rgbdns) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module demosaic process: RgbdnsParam
*                      2.class definition of module RawDns process: RgbdnsModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/2
*/

#pragma once
#ifndef _ISP_RGBDNS_H
#define _ISP_RGBDNS_H

#include "common.h"

/* This is the local param of the isp module -- RGB Domain Denoise Process, main contains the value of parameter:enable,sigma2
		parameter description:
				1. enable: controls whether enables the rgbdns process
				2. sigma:  controls the intensity of gaussian noise of the input image
*/
typedef  struct
{
	bool enable;                     /* Determine whether enables the rgbdns process */
	uint32_t sigma;							     /* controls the intensity (standard diff) of gaussian noise of the input image */
}RgbdnsParam;


/* This is the class definition of module rgbdns process, private param only includes the RgbdnsParam
	 Functions includes the data read in/ data write out �� main Rgbdns process ��data copy process ��
	 Rgbdns_param initialize process ��local data block fetch process��Eurdis_Calculation_Process 
	 and weight calculation process
*/

class RgbdnsModule
{
private:
	RgbdnsParam local_param;
protected:
	void Rgbdns_Param_Initial(bool enable, bool manual, uint32_t sigma);
public:
	RgbdnsModule()
	{
		memset(&local_param, 0, sizeof(RgbdnsParam));
	};
	~RgbdnsModule()
	{

	};
	void  RgbdnsModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint32_t sigma, TopParam& Top);
	void  Get_Rgbdns_Block(uint8_t local_block[9][9][3],uint8_t* src_in, uint32_t cur_x ,uint32_t cur_y,uint32_t Width);
	void  Get_Comp_Block(uint8_t local_block[9][9][3], uint32_t cur_x, uint32_t cur_y, uint8_t comp_block[3][3][3]);
	uint32_t  Cal_Eur_Distance(uint8_t comp_block1[3][3][3], uint8_t comp_block2[3][3][3]);
	uint32_t  Weight_Calculation_Process(uint32_t eur_dis, uint32_t sigma);
	void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};
#endif


