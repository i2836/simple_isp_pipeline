/*  CopyRight��ZhiHanZhang
*   FileName��  gb.h
*   Description�� This the header file of module GreenBalance (GB) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module gb process: GbParam
*                      2.class definition of module GB process: GbModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/03/28
*/

#pragma once
#ifndef _ISP_GB_H
#define _ISP_GB_H

#include "common.h"

/* This is the local param of the isp module -- GB, main contains the value of parameter: gb_value,
	 which is adopted to determine whether the statistic average difference of abs(Gb-Gr) should be 
	 measured as the Technological differences and should be corrected
*/
typedef  struct
{
	bool enable;          /* Determine whether enables the gb process */
	uint8_t gb_value;     /* Preset gb_value to judge whether the current scan point should be green balance corrected */
	uint8_t thre_value;   /* Preset thre_value to avoid errorly correct the edge gradient */
}GbParam;


/* This is the class definition of module gb process, private param only includes the GbParam
	 Functions includes the data read in/ data write out �� main gb process ��data copy process ��
	 gb_param initialize process ��the block get process�� GreenBalance Diff calculation and correction process
*/
class GbModule
{
private:
	GbParam local_param;
protected:
	void Gb_Param_Initial(bool enable, bool Manual, uint8_t gb_value,uint8_t thre_value);
public:
	GbModule()
	{
		memset(&local_param, 0, sizeof(GbParam));
	};
	~GbModule()
	{

	};
	void  Get_Gb_Block(uint8_t block[5][5], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width);
	void  GbModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t gb_value, uint8_t thre_value, TopParam& Top);
	int32_t  GbLocalProcess(uint8_t block[5][5], uint8_t gb_value, uint8_t thre_value, uint8_t type);
	void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};

#endif


