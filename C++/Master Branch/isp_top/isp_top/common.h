/*
* CopyRight��ZhiHanZhang
* FileName��  common.h
* Description�� This is the common file which includes the common used functions adopted in each ISP process module
* Author��ZhiHanZhang
* Version�� 0.1
* Date�� 2022/03/25
*/

#pragma once
#ifndef ISP_COMMON_H
#define ISP_COMMON_H

#include "stdio.h"
#include "stdlib.h"
#include "memory.h"
#include "stdint.h"
#include "math.h"

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;


/* This is the top param of the whole isp, main contains the attribution of input images */
typedef struct
{
	bool Enable;             /* This signal controls whether enables the whole ISP pipeline */
	bool Manual;             /* This signal controls whether enables the manual parameter set ,when manual = 1, all the modules in the isp pipeline accepts the outside input param as the local param initialization */
	uint8_t BitDepth;        /* This signal presents the bitdepth which the input image data depth */
	uint8_t BayerPattern;    /* This flag is 2 bit width,resembles the four bayer patterns: 00/01/10/11 seperately related to RGGB/GRBG/GBRG/BGGR */
	uint32_t Img_Height;
	uint32_t Img_Width;
}TopParam;


/* the max comparion function for the uint8_t datatype */
inline uint8_t max(uint8_t a, uint8_t b)
{
	if (a >= b)
		return a;
	else
		return b;
}

/* the max comparion function for the int8_t datatype */
inline int8_t max(int8_t a, int8_t b)
{
	if (a >= b)
		return a;
	else
		return b;
}

/* the min comparion function for the uint8_t datatype */
inline uint8_t min(uint8_t a, uint8_t b)
{
	if (a >= b)
		return b;
	else
		return a;
}

/* the min comparion function for the int8_t datatype */
inline int8_t min(int8_t a, int8_t b)
{
	if (a >= b)
		return b;
	else
		return a;
}

/* the min comparion function for the uint32_t datatype */
inline uint32_t min(uint32_t a, uint32_t b)
{
	if (a >= b)
		return b;
	else
		return a;
}


/* the abs calculation function for the uint8_t datatype */
inline uint8_t abs_diff(uint8_t a, uint8_t b)
{
	if (a >= b)
		return a - b;
	else
		return b - a;
}

/* the abs diff calculation function for the int8_t datatype */
inline int8_t abs_diff(int8_t a, int8_t b)
{
	if (a >= b)
		return a - b;
	else
		return b - a;
}

/* the abs diff calculation function for the uint32_t datatype */
inline uint32_t abs_diff(uint32_t a, uint32_t b)
{
	if (a >= b)
		return a - b;
	else
		return b - a;
}

/* the clip calculation function for the uint8_t datatype */
inline uint8_t clip(uint8_t a, uint8_t Lbound, uint8_t Hbound)
{
	if (a > Hbound)
		return Hbound;
	else if (a < Lbound)
		return Lbound;
	else
		return a;
}

/* the clip calculation function for the uint16_t datatype */
inline uint8_t clip(uint16_t a, uint8_t Lbound, uint8_t Hbound)
{
	if (a > Hbound)
		return Hbound;
	else if (a < Lbound)
		return Lbound;
	else
		return static_cast<uint8_t>(a);
}


/* the clip calculation function for the uint32_t datatype */
inline uint8_t clip(uint32_t a, uint8_t Lbound, uint8_t Hbound)
{
	if (a > Hbound)
		return Hbound;
	else if (a < Lbound)
		return Lbound;
	else
		return static_cast<uint8_t> (a);
}

/* the clip calculation function for the int8_t datatype */
inline uint8_t clip(int8_t a, uint8_t Lbound, uint8_t Hbound)
{
	if (a > Hbound)
		return Hbound;
	else if (a < Lbound)
		return Lbound;
	else
		return static_cast<uint8_t> (a);
}

/* the clip calculation function for the int16_t datatype */
inline uint8_t clip(int16_t a, uint8_t Lbound, uint8_t Hbound)
{
	if (a > Hbound)
		return Hbound;
	else if (a < Lbound)
		return Lbound;
	else
		return static_cast<uint8_t> (a);
}

/* the clip calculation function for the int32_t datatype */
inline uint8_t clip(int32_t a, uint8_t Lbound, uint8_t Hbound)
{
	if (a > Hbound)
		return Hbound;
	else if (a < Lbound)
		return Lbound;
	else
		return static_cast<uint8_t> (a);
}

/* the clip calculation function for the double datatype */
inline uint8_t clip(double a, uint8_t Lbound, uint8_t Hbound)
{
	if (a > Hbound)
		return Hbound;
	else if (a < Lbound)
		return Lbound;
	else
		return static_cast<uint8_t> (a);
}
#endif




