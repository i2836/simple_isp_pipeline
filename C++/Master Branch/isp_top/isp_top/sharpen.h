/*  CopyRight��ZhiHanZhang
*   FileName��  sharpen.h
*   Description�� This the header file of module sharpen process in the ycbcr domain (y_sharpen) in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.local paramset of module demosaic process: SharpenParam
*                      2.class definition of module Sharpen process: SharpenModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/2
*/

#pragma once
#ifndef _ISP_SHARPEN_H
#define _ISP_SHARPEN_H

#include "common.h"

/* This is the local param of the isp module --  Ycbcr Domain Sharpen Process, main contains the value of parameter:enable��coeff
		parameter description:
				1. enable: controls whether enables the sharpen process
				2. coeff: controls the sharpen intensity of Y component
*/
typedef struct
{
	bool enable;                 /* Determine whether enables the Y sharpen process */
	uint8_t coeff;               /* Controls the sharpen intensity of Y component */
}SharpenParam;


/* This is the class definition of module Sharpen process, private param only includes the SharpenParam
	 Functions includes the data read in/ data write out ��main Sharpen process ��data copy process ��
	 Sharpen_param initialize process��local sharpen block fetch process and local sharpen\fcr process 
*/

class SharpenModule
{
private:
	SharpenParam local_param;
protected:
	void Sharpen_Param_Initial(bool enable, bool manual,uint8_t coeff);
public:
	SharpenModule()
	{
		memset(&local_param, 0, sizeof(SharpenParam));
	};
	~SharpenModule()
	{

	};
	void  SharpenModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t coeff, TopParam& Top);
	void  Get_Sharpen_Block(uint8_t local_block[5][5][3], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width);
	void  Local_Sharpen_Fcr_Process(uint8_t local_block[5][5][3], uint8_t coeff, uint8_t ycbcr_out[3]);
	void  ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in);
	void  ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out);
	void  CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length);
};
#endif

