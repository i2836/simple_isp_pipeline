/*  CopyRight��ZhiHanZhang
*   FileName��  isp_top.h
*   Description�� This the header file of the top module in the simplest camera ISP pipeline
*                 This header file includes the following contents:
*                      1.paramset of the top module in the isp pipeline: TopParam��EnableParam��SetParam
*                      2.class definition of module isp_top: TopModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/3
*/

#pragma once
#ifndef _ISP_TOP_H
#define _ISP_TOP_H

#include "common.h"
#include "dgc.h"
#include "sp.h"
#include "dpc.h"
#include "gb.h"
#include "rawdns.h"
#include "demosaic.h"
#include "ccp.h"
#include "rgbdns.h"
#include "rgb2ycbcr.h"
#include "sharpen.h"

/*  This is the param of the isp_top module, main contains three struct: TopParam��EnableParam��SetParam
		struct description:
				1. TopParam: contains the parameters of the input raw image(Enable��CFA pattern��Format��Size��Data Depth and So on)
				2. EnableParam: contains each enable signal in the seperate isp sub module
				3. SetParam: contains the paramters in the each sub isp module
		The TopParam struct is defined in the header file ���� "common.h", here gives the other two
*/

typedef struct
{
	bool dgc_enable;          /* Determine whether enables the dgc process */
	bool sp_enable;           /* Determine whether enables the sp process */
	bool dpc_enable;		      /* Determine whether enables the dpc process */
	bool gb_enable;		        /* Determine whether enables the gb process */
	bool rawdns_enable;		    /* Determine whether enables the rawdns process */
	bool demosaic_enable;     /* Determine whether enables the demosaic process */
	bool ccp_enable;					/* Determine whether enables the ccp process */
	bool rgbdns_enable;			  /* Determine whether enables the rgbdns process */
	bool rgb2ycbcr_enable;    /* Determine whether enables the rgb2ycbcr process */
	bool sharpen_enable;      /* Determine whether enables the sharpen process */
}EnableParam;



typedef struct
{
	/* Param in the dgc process */
	uint8_t dgc_blc_Value;           /* blc value of input picture,default is 0 */
	uint8_t dgc_rGain;               /* dGain value of R position in the cfa array, default is 64 ,should be right shifted 6 bit when getting the final result */
	uint8_t dgc_gGain;               /* dGain value of G position in the cfa array, default is 64 ,should be right shifted 6 bit when getting the final result */
	uint8_t dgc_bGain;               /* dGain value of B position in the cfa array, default is 64 ,should be right shifted 6 bit when getting the final result */
	uint32_t dgc_lsc_param[17][13];  /* calibration parameter of camera lens ,default is 256 , the result should be right shifted 8 bit */

	/* Param in the sp process */
	uint8_t sp_low_thre;             /* Assumed Pepper Noise in the image, default is 5 for the 8 bit data depth */
	uint8_t sp_high_thre;            /* Assumed Salt Noise in the image, default is 250 for the 8 bit data depth */

	/* Param in the dpc process */
	uint8_t dpc_grad_thre1;          /* Preset grad_thre1 to judge whether the current scan point is a defected point,default value is 10 */
	uint8_t dpc_grad_thre2;          /* Preset grad_thre2 to judge whether the current scan point is a defected point,default value is 60 */

	/* Param in the gb process */
	uint8_t gb_correct_value;        /* Preset gb_value to judge whether the current scan point should be green balance corrected */
	uint8_t gb_thre_value;           /* Preset thre_value to avoid errorly correct the edge gradient */

	/* Param in the demosaic process */
	uint8_t demosaic_ratio;          /* Determine the judge the type of the local image block according too the  ratio value */

	/* Param in the ccp process */
	bool ccp_histeq_enable;					/* Determine whether enables the histeq process in the ccp module */
	uint8_t ccp_ccm_matrix[3][3];   /* Determine the 3*3 color correction matrix adopted to transfer from rgb to srgb domain */

	/* Param in the rgbdns process */
	uint32_t rgbdns_sigma;					/* controls the intensity (standard diff) of gaussian noise of the input image */

	/* Param in the sharpen process */
	uint8_t sharpen_coeff;               /* Controls the sharpen intensity of Y component */
}SetParam;



/* This is the class definition of module isp_top function, private param includes the TopParam and classes related to each sub isp module
*  Public param includes the EnableParam and SetParam those can be set outside the class
	 Functions includes the data read in/ data write out ��isp_top process ��data copy process ��
	 Top_param initialize process
*/

class TopModule
{
private:
	TopParam Top_param;
	DgcModule dgc;
	SpModule sp;
	DpcModule dpc;
	GbModule gb;
	RawdnsModule rawdns;
	DemosaicModule demosaic;
	CcpModule ccp;
	RgbdnsModule rgbdns;
	Rgb2ycbcrModule rgb2ycbcr;
	SharpenModule sharpen;
protected:
	void Top_Param_Initial(bool enable, bool manual, uint8_t bitdepth, uint8_t bayer, uint32_t height, uint32_t width);
public:
	TopModule()
	{
		memset(&Top_param, 0, sizeof(TopParam));
	};
	~TopModule()
	{

	};

	void  TopModuleProcess(FILE* fp_in,FILE* fp_out, bool enable, bool manual, uint8_t bitdepth, uint8_t bayer, uint32_t height, uint32_t width, SetParam& Set, EnableParam& Enable);
	void  CopyData(FILE* fp_in, FILE* fp_out, uint32_t length);
};
#endif


