/*  CopyRight��ZhiHanZhang
*   FileName��  main.cpp
*   Description�� This the Test file of top module in the simplest camera ISP pipeline
*                 This file contains the following contents:
*                      1.EnableParam initial��SetParam initial
*                      2.Test the whole isp process
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/3
*/

#include "common.h"
#include "isp_top.h"

int main(int argc, char** argv)
{
	/* Enable param��Set Param��class and FILE pointer initialization */
	EnableParam t1;
	SetParam t2;
	TopModule top;
	FILE* fp_in, * fp_out;
	errno_t err_in, err_out;

	if (argc != 9)
		printf("Error! Not input required number of paramter!\n");

	//fp_in = fopen(argc[0], "rb");
	//fp_out = fopen(argc[1], "wb");
	err_in = fopen_s(&fp_in, argv[1], "rb");
	err_out = fopen_s(&fp_out, argv[2], "wb");

	memset(&t1, 0, sizeof(EnableParam));
	memset(&t2, 0, sizeof(SetParam));

	t1.dgc_enable = 1;          /* enables the dgc process */
	t1.sp_enable = 1;           /* enables the sp process */
	t1.dpc_enable = 1;		      /* enables the dpc process */
	t1.gb_enable = 1;	          /* enables the gb process */
	t1.rawdns_enable = 1;		    /* enables the rawdns process */
	t1.demosaic_enable = 1;     /* enables the demosaic process */
	t1.ccp_enable = 1;					/* enables the ccp process */
	t1.rgbdns_enable = 1;			  /* enables the rgbdns process */
	t1.rgb2ycbcr_enable = 1;    /* enables the rgb2ycbcr process */
	t1.sharpen_enable = 1;      /* enables the sharpen process */

	t2.dgc_blc_Value = 5;          
	t2.dgc_rGain = 64;           
	t2.dgc_gGain = 64;
	t2.dgc_bGain = 64; 
	t2.sp_low_thre = 5;
	t2.sp_high_thre = 250;
	t2.dpc_grad_thre1 = 10;
	t2.dpc_grad_thre2 = 60;
	t2.gb_correct_value = 5;
	t2.gb_thre_value = 20;
	t2.demosaic_ratio = 2;
	t2.ccp_histeq_enable = 0; 
	t2.rgbdns_sigma = 30;
	t2.sharpen_coeff = 3;

	for (uint32_t i = 0; i < 17; i++)
	{
		for (uint32_t j = 0; j < 13; j++)
		{
			t2.dgc_lsc_param[i][j] = 256;
		}
	}

	for (uint32_t i = 0; i < 3; i++)
	{
		for (uint32_t j = 0; j < 3; j++)
		{
			if (i == j)
			{
				t2.ccp_ccm_matrix[i][j] = 1;
			}
			else
			{
				t2.ccp_ccm_matrix[i][j] = 0;
			}
		}
	}

	/* Data input ��Main isp_top process and Data output */
	top.TopModuleProcess(fp_in,fp_out, atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]), atoi(argv[8]), t2 , t1);

	printf("The whole ISP process is finished!\n");
	return 0;
}