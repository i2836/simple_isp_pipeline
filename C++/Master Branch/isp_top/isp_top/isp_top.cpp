/*  CopyRight��ZhiHanZhang
*   FileName��    isp_top.cpp
*   Description�� This the C model of the top function modelin the simplest camera ISP pipeline
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class TopModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/3
*/


#include "isp_top.h"

/* This Function is adopted for the input img data directly write out when the whole isp process is not enabled
parameter description :
1.fp_in :  the file pointer which point to the input image path
2.fp_out : the file pointer which point to the output image path
3.length : the data size length of input/output data stream
*/
void TopModule::CopyData(FILE* fp_in, FILE* fp_out, uint32_t length)
{
	uint8_t* src_in = (uint8_t*)malloc(sizeof(uint8_t) * length);
	uint8_t* dst_out = (uint8_t*)malloc(sizeof(uint8_t) * length);

	fread(src_in, sizeof(uint8_t), length, fp_in);
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
	fwrite(dst_out, sizeof(uint8_t), length, fp_out);
}

/* This Function is adopted for the isp_top module local param initialzation
	 parameter description:
			1.enable:   the control signal of the whole isp process
			2.manual:   controls whether enables the parameter auto-set in each sub isp module
			3.bitdepth: controls the data depth of the input image
			4.bayer:    controls the pattern of the cfa pattern
			5.height:   rows of the input image
			6.width:    cols of the input image
*/
void TopModule::Top_Param_Initial(bool enable, bool manual, uint8_t bitdepth, uint8_t bayer, uint32_t height, uint32_t width)
{
	Top_param.Enable = enable;
	Top_param.Manual = manual;
	Top_param.BitDepth = bitdepth;
	Top_param.BayerPattern = bayer;
	Top_param.Img_Height = height;
	Top_param.Img_Width = width;
}


/* This Function is adopted for the main process of the top module in the whole isp pipeline
*  The whole process main contains five process:
			1.input data read in
			2.sub module data space allocation
			3.execute the whole isp process in pipeline turn
			4.write the process data out
			5.release the allocated sub module data space
	 parameter description:
			1.fp_in :   the file pointer which point to the input image path
			2.fp_out:   the file pointer which point to the output image path
			3.enable:   the signal controls whether enables the whole isp process
			4.manual:   the signal controls whether enables the parameter auto-set in each sub isp module
			5.bitdepth: the parameter presents the bitdepth which the input image data depth
			6.bayer:    the parameter resembles the bayer pattern of input image
			7.height:   the parameter gives the rows of input image
			8.width:    the parameter gives the cols of input image
			9.Set:      the struct includes the parameters in seperate sub isp modules
			10.Enable:  the struct includes the enable signals in seperate sub isp modules
*/
void TopModule::TopModuleProcess(FILE* fp_in, FILE* fp_out, bool enable, bool manual, uint8_t bitdepth, uint8_t bayer, uint32_t height, uint32_t width, SetParam& Set, EnableParam& Enable)
{
	Top_Param_Initial(enable, manual, bitdepth, bayer, height, width);
	if (Top_param.Enable)
	{
		uint8_t* image_in = (uint8_t*)malloc(sizeof(uint8_t) * height * width);
		uint8_t* sp_in = (uint8_t*)malloc(sizeof(uint8_t) * height * width);
		uint8_t* dpc_in = (uint8_t*)malloc(sizeof(uint8_t) * height * width);
		uint8_t* gb_in = (uint8_t*)malloc(sizeof(uint8_t) * height * width);
		uint8_t* rawdns_in = (uint8_t*)malloc(sizeof(uint8_t) * height * width);
		uint8_t* demosaic_in = (uint8_t*)malloc(sizeof(uint8_t) * height * width);
		uint8_t* ccp_in = (uint8_t*)malloc(sizeof(uint8_t) * 3 * height * width);
		uint8_t* rgbdns_in = (uint8_t*)malloc(sizeof(uint8_t) * 3 * height * width);
		uint8_t* rgb2ycbcr_in = (uint8_t*)malloc(sizeof(uint8_t) * 3 * height * width);
		uint8_t* sharpen_in = (uint8_t*)malloc(sizeof(uint8_t) * 3 * height * width);
		uint8_t* image_out = (uint8_t*)malloc(sizeof(uint8_t) * 3 * height * width);

		fread(image_in, sizeof(uint8_t), height * width, fp_in);
		fclose(fp_in);

		dgc.DgcModuleProcess(image_in, sp_in, Enable.dgc_enable, Set.dgc_blc_Value, Set.dgc_rGain, Set.dgc_gGain, Set.dgc_bGain, Set.dgc_lsc_param, Top_param);
		sp.SpModuleProcess(sp_in, dpc_in, Enable.sp_enable, Set.sp_low_thre, Set.sp_high_thre, Top_param);
		dpc.DpcModuleProcess(dpc_in, gb_in, Enable.dpc_enable, Set.dpc_grad_thre1, Set.dpc_grad_thre2, Top_param);
		gb.GbModuleProcess(gb_in, rawdns_in, Enable.gb_enable, Set.gb_correct_value, Set.gb_thre_value, Top_param);
		rawdns.RawDnsModuleProcess(rawdns_in, demosaic_in, Enable.rawdns_enable, Top_param);
		demosaic.DemosaicModuleProcess(demosaic_in, ccp_in, Enable.demosaic_enable, Set.demosaic_ratio, Top_param);
		ccp.CcpModuleProcess(ccp_in, rgbdns_in, Enable.ccp_enable, Set.ccp_histeq_enable, Set.ccp_ccm_matrix, Top_param);
		rgbdns.RgbdnsModuleProcess(rgbdns_in, rgb2ycbcr_in, Enable.rgbdns_enable, Set.rgbdns_sigma, Top_param);
		rgb2ycbcr.Rgb2ycbcrModuleProcess(rgb2ycbcr_in, sharpen_in, Enable.rgb2ycbcr_enable, Top_param);
		sharpen.SharpenModuleProcess(sharpen_in, image_out, Enable.sharpen_enable, Set.sharpen_coeff, Top_param);

		fwrite(image_out, sizeof(uint8_t), 3 * height * width, fp_out);
		fclose(fp_out);

		free(image_in);
		free(sp_in);
		free(dpc_in);
		free(gb_in);
		free(rawdns_in);
		free(demosaic_in);
		free(ccp_in);
		free(rgbdns_in);
		free(rgb2ycbcr_in);
		free(sharpen_in);
		free(image_out);

		printf("ISP whole pipeline process is finished!\n");
	}
	else
	{
		CopyData(fp_in, fp_out, height * width);
		printf("Sorry, the isp process is not enabled!\n");
	}
}