/*  CopyRight��ZhiHanZhang
*   FileName��    sharpen.cpp
*   Description�� This the C model of module Ycbcr Domain sharpen and False Color Remove (Sharpen) in the simplest camera ISP pipeline
*                 The Ycbcr Domain Sharpen and FCR (Sharpen) module is the combination of 2 processes:
*                      1.Local 5*5*3 block fetch process: being finished by the function ���� Get_Sharpen_Block()
*                      2.Y sharpen and UV fcr by the judgement result between the Gradient of Y and preset threhold
*                 This file contains the following content:
*                      1.the detailed public function descriptions of class SharpenModule
*   Author��ZhiHanZhang
*   Version�� 0.1
*   Date�� 2022/4/2
*/

#include "sharpen.h"
/* This Function is adopted for the input img data read in
	 parameter description:
			1.fp1(the FILE pointer),used to open a file input stream
			2.length: the read size of input data stream from the fp1
			3.src_in: the allocated input data space and stores the input length size data
*/
void SharpenModule::ImgReadin(FILE* fp1, uint32_t length, uint8_t* src_in)
{
	if (fp1 == nullptr)
	{
		printf("The Input Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (src_in == nullptr)
	{
		printf("The Input Image data space allocation failed!\n");
		exit(1);
	}
	fread(src_in, sizeof(uint8_t), length, fp1);
	fclose(fp1);
}

/* This Function is adopted for the input img data write out
	 parameter description :
			1.fp2(the FILE pointer), used to open a file output stream
			2.length : the write size of output data stream from the fp2
			3.dst_out : the allocated output data space and stores the output length size data
*/
void SharpenModule::ImgWriteout(FILE* fp2, uint32_t length, uint8_t* dst_out)
{
	if (fp2 == nullptr)
	{
		printf("The Output Image file is not exist, please check the file path!\n");
		exit(1);
	}

	if (dst_out == nullptr)
	{
		printf("The Output Image data space allocation failed!\n");
		exit(1);
	}

	fwrite(dst_out, sizeof(uint8_t), length, fp2);
	fclose(fp2);
}

/* This Function is adopted for the input img data directly write out when the sharpen process is not enabled
	parameter description :
		 1.src_in : the allocated input data space and stores the input length size data
		 2.dst_out : the allocated output data space and stores the output length size data
		 3.length : the data size length of src_in / dst_out data stream
*/
void SharpenModule::CopyData(uint8_t* src_in, uint8_t* dst_out, uint32_t length)
{
	memcpy(dst_out, src_in, sizeof(uint8_t) * length);
}

/* This Function is adopted for the sharpen module local param initialzation
	 parameter description :
			1.enable : the control signal of the rgbdns process, default value is 1
			2.grad_thre  : the judge threhold of gradient of y component, to determine whether the cbcr component should be false color removed
*/
void SharpenModule::Sharpen_Param_Initial(bool enable, bool manual, uint8_t coeff)
{
	memset(&local_param, 0, sizeof(SharpenParam));

	if (manual)
	{
		local_param.enable = enable;
		local_param.coeff = coeff;
	}
	else
	{
		local_param.enable = 1;
		local_param.coeff = 3;
	}
}

/* This Function is adopted for the sharpen module local img data block fetch process (size is 5*5*3)
	 Enables this function to get the 5*5*3 data block which is centered as the cur scan point
	 parameter description:
			1.block:  the 3D array to store the fetched 5*5*3 local img data region
			2.src_in: the allocated input data space and stores the input length size data
			3.cur_x:  the horizontial position index of the current scan point
			4.cur_y:  the vertical position index of the current scan point
			5.width:  the width of the input image size
*/
void SharpenModule::Get_Sharpen_Block(uint8_t local_block[5][5][3], uint8_t* src_in, uint32_t cur_x, uint32_t cur_y, uint32_t width)
{
	for (uint32_t i = 0; i < 5; i++)
	{
		for (uint32_t j = 0; j < 5; j++)
		{
			for (uint32_t k = 0; k < 3; k++)
			{
				local_block[i][j][k] = src_in[(cur_y - 2 + i) * 3 * width + 3 * (cur_x - 2 + j) + k];
			}
		}
	}
}


/* This Function is adopted for the main process of the Sharpen module
	 parameter description:
			1.src_in:    the allocated input data space and stores the input length size data
			2.dst_out:   the allocated output data space and stores the output length size data
			3.enable:    the signal controls whether enables the sharpen and fcr process
			4.grad_thre :  Determine the judge threhold of gradient of y component, to determine whether the cbcr component should be false color removed 
			5.top_param: the top param struct
*/
void SharpenModule::SharpenModuleProcess(uint8_t* src_in, uint8_t* dst_out, bool enable, uint8_t coeff, TopParam& Top)
{
	Sharpen_Param_Initial(enable, Top.Manual, coeff);
	if (local_param.enable)
	{
		uint8_t local_block[5][5][3];
		uint8_t ycbcr_out[3];
		
		memcpy(dst_out, src_in, sizeof(uint8_t) * 3 * Top.Img_Height * Top.Img_Width);
		for (uint32_t i = 2; i < Top.Img_Height - 2; i++)
		{
			for (uint32_t j = 2; j < Top.Img_Width - 2; j++)
			{
				Get_Sharpen_Block(local_block, src_in, j, i, Top.Img_Width);
				Local_Sharpen_Fcr_Process(local_block, coeff, ycbcr_out,i,j);

				if ((i == 6) && (j == 19))
				{
					printf("y_out = %d, u_out = %d, v_out = %d\n", ycbcr_out[0], ycbcr_out[1], ycbcr_out[2]);
				}
				dst_out[i * 3 * Top.Img_Width + 3 * j] = ycbcr_out[0];
				dst_out[i * 3 * Top.Img_Width + 3 * j + 1] = ycbcr_out[1];
				dst_out[i * 3 * Top.Img_Width + 3 * j + 2] = ycbcr_out[2];
			}
		}
		printf("ISP sharpen and fcr process is finished!\n");
	}
	else
	{
		CopyData(src_in, dst_out, 3 * Top.Img_Height * Top.Img_Width);
		printf("ISP sharpen and fcr process is bypassed!\n");
	}
}

/* This Function is adopted for the local sharpen and fcr process of the Sharpen module
	 parameter description:
			1.local_block:  the 3D array to store the fetched 5*5*3 local img data region
			2.grad_thre:    Determine the judge threhold of gradient of y component, to determine whether the cbcr component should be false color removed
			3.ycbcr_out:    the output y and uv processed data
*/
void SharpenModule::Local_Sharpen_Fcr_Process(uint8_t local_block[5][5][3], uint8_t coeff, uint8_t ycbcr_out[3], uint32_t row, uint32_t col)
{
	uint8_t y_block[5][5];

	int32_t filter_55[5][5] = { 1,2,4,2,1,2,4,8,4,2,4,8,16,8,4,2,4,8,4,2,1,2,4,2,1 };
	int32_t temp_result = 0;
	int32_t high_v[4], low_v[4], result_v[5];
	int32_t high_h[4], low_h[4];
	int32_t lowfeq = 0;

	for (uint32_t i = 0; i < 5; i++)
	{
		for (uint32_t j = 0; j < 5; j++)
		{
			y_block[i][j] = local_block[i][j][0];
		}
	}

	for (uint32_t i = 0; i < 5; i++)
	{
		for (uint32_t j = 0; j < 5; j++)
		{
			temp_result += filter_55[i][j] * y_block[i][j];
		}
	}
	temp_result = temp_result >> 10;

	for (uint32_t i = 0; i < 5; i++)
	{
		for (uint32_t j = 0; j < 4; j++)
		{
			high_v[j] = (y_block[j][i] - y_block[j + 1][i])/2;
			low_v[j] =  (y_block[j][i] + y_block[j + 1][i])/2;

			if (high_v[j] > temp_result)
			{
				high_v[j] -= temp_result;
			}
			else if (high_v[j] < -temp_result)
			{
				high_v[j] += temp_result;
			}
			else
			{
				high_v[j] = 0;
			}
		}
		result_v[i] = clip(high_v[1] / 2 + low_v[1] / 2 + high_v[2] / 2 + low_v[2] / 2, 0, 255);
	}

	if ((row == 6) && (col == 19))
	{
		printf("%d %d %d %d %d\n", y_block[0][0], y_block[0][1], y_block[0][2], y_block[0][3], y_block[0][4]);
		printf("%d %d %d %d %d\n", y_block[1][0], y_block[1][1], y_block[1][2], y_block[1][3], y_block[1][4]);
		printf("%d %d %d %d %d\n", y_block[2][0], y_block[2][1], y_block[2][2], y_block[2][3], y_block[2][4]);
		printf("%d %d %d %d %d\n", y_block[3][0], y_block[3][1], y_block[3][2], y_block[3][3], y_block[3][4]);
		printf("%d %d %d %d %d\n", y_block[4][0], y_block[4][1], y_block[4][2], y_block[4][3], y_block[4][4]);

		printf("thre = %d\n", temp_result);
		printf("%d %d %d %d %d\n", result_v[0], result_v[1], result_v[2],
			result_v[3], result_v[4]);
	}
	for (uint32_t i = 0; i < 4; i++)
	{
		high_h[i] = result_v[i] / 2 - result_v[i + 1] / 2;
		low_h[i] = result_v[i] / 2 + result_v[i + 1] / 2;

		if (high_h[i] > temp_result)
		{
			high_h[i] -= temp_result;
		}
		else if (high_h[i] < -temp_result)
		{
			high_h[i] += temp_result;
		}
		else
		{
			high_h[i] = 0;
		}
	}

	lowfeq = clip(high_h[1] / 2 + low_h[1] / 2 + high_h[2] / 2 + low_h[2] / 2, 0, 255);
	ycbcr_out[0] = clip(lowfeq + coeff * (static_cast<int32_t>(y_block[2][2] - lowfeq)), 0, 255);
	ycbcr_out[1] = local_block[2][2][1];
	ycbcr_out[2] = local_block[2][2][2];
}